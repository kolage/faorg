#!/bin/bash

# Create families data

cat adresy.csv | cut -d";" -f3 > family-name
cat adresy.csv | cut -d";" -f5 > family-street
cat adresy.csv | cut -d";" -f4 > family-city
cat adresy.csv | cut -d";" -f6 > family-zipcode

id=0
while read line
do
  surname=$(echo "${family-name}" | cut -d" " -f1 | cut -d"," -f1)
  if echo $surname | grep "á$"
  then
    title="Vážená paní"
  elif echo $surname | grep "ovi$"
  then
    title="Vážení manželé"
  else
    title="Vážený pan"
  fi
  
  letter_title="Vážený pane ${surname}"
  if echo $surname | grep -q "á$"
  then
    letter_title="Vážená paní ${surname}"
  elif echo $surname | grep -q "ovi$"
  then
    letter_title="Vážení ${surname}"
  elif echo $surname | grep -q "ek$"
  then
    surnameFirst=$(echo $surname | rev | cut -c 3- | rev)
    letter_title="Vážený pane ${surnameFirst}ku"
  elif echo $surname | grep -q "al$"
  then
    letter_title="Vážený pane ${surname}e"
  elif echo $surname | grep -q "er$"
  then
    letter_title="Vážený pane ${surname}e"
  elif echo $surname | grep -q "n$"
  then
    letter_title="Vážený pane ${surname}e"
  elif echo $surname | grep -q "a$"
  then
    surnameFirst=$(echo $surname | rev | cut -c 2- | rev)
    letter_title="Vážený pane ${surnameFirst}o"
  elif echo $surname | grep -q "[^oý]$"
  then
    letter_title="Vážený pane ${surname}u"
  fi
  
  echo $letter_title >> family-letter-title
  echo $title >> family-title
  echo $id >> family-id
  echo "" >> family-semicolon
  id=$(($id + 1))
done < family-name

paste -d";" family-id family-name family-street family-city family-zipcode family-semicolon family-semicolon family-semicolon family-semicolon family-title family-letter-title > families

rm family-*

# Create clients data

cat klienti.csv | cut -d";" -f1 > client-name
cat klienti.csv | cut -d";" -f2 > client-familyname
cat klienti.csv | cut -d";" -f1,3,5 > client-name_birthdate_in

id=0
while read family_name
do
  grep "$family_name" families | head -1 | cut -d";" -f1 >> client-familyID
  echo $id >> client-id
  id=$(($id + 1))
done < client-familyname

while read line
do
  name=$(echo $line | cut -d";" -f1)
  birthdate=$(echo $line | cut -d";" -f2)
  client_in=$(echo $line | cut -d";" -f3)

  if echo $name | cut -d" " -f1 | grep -q "[a,e]$";
  then
    gender="WOMAN"
  else
    gender="MAN"
  fi

  if [ -z $client_in ]
  then

    if [ -n $birthdate ]
    then
      day=$(echo $birthdate | cut -d"." -f1)
      month=$(echo $birthdate | cut -d"." -f2)
      year=$(echo $birthdate | cut -d"." -f3 | tail -c 3)

      if [ $gender == "WOMAN" ]
      then
        month=$(echo $month | sed 's/^0\(.*\)/\1/')
        month=$(($month+50))
      fi

      client_in="${year}${month}${day}0000"
    fi

  fi
  echo "${client_in}" >> client-ident
  echo "${gender}" >> client-gender
  echo "" >> client-semicolon
done < client-name_birthdate_in

paste -d";" client-id client-familyID client-ident client-semicolon client-name client-semicolon client-semicolon client-gender client-semicolon client-semicolon client-semicolon > clients

rm client-*

# Create client telephones data

if [ -f telephones ]
then
  rm telephones
fi

while read line
do
  family_name=$(echo $line | cut -d";" -f3)
  telephone=$(echo $line | cut -d";" -f10)
  family_id=$(grep "$family_name" families | head -1 | cut -d";" -f1)
  client_id=$(cat clients | cut -d";" -f1,2 | grep "${family_id}$" | head -1 | cut -d";" -f1)
  if [ ${#telephone} -gt 5 ]
  then
    echo "${client_id};${telephone}" >> telephones
  fi
done < adresy.csv

# Create client contracts data

cat smlouvy.csv | cut -d";" -f1,2 > contract-client-name
cat smlouvy.csv | cut -d";" -f3 > contract-type
cat smlouvy.csv | cut -d";" -f4 > contract-institution
cat smlouvy.csv | cut -d";" -f6 > contract-startdate
cat smlouvy.csv | cut -d";" -f7 > contract-enddate
cat smlouvy.csv | cut -d";" -f9 > contract-frequency
cat smlouvy.csv | cut -d";" -f10 > contract-payment
cat smlouvy.csv | cut -d";" -f11 > contract-target-amount
cat smlouvy.csv | cut -d";" -f13 > contract-id-value

while read date
do
  day=$(echo $date | cut -d"." -f1)
  month=$(echo $date | cut -d"." -f2)
  year=$(echo $date | cut -d"." -f3)
  shiftedtime=$(date -d"${year}-${month}-${day} 100 years" +%s)
  shiftedtimeMS=$(($shiftedtime*1000))
  echo "${shiftedtimeMS}" >> contract-startdate-new
  echo "" >> contract-semicolon
done < contract-startdate

while read date
do
  day=$(echo $date | cut -d"." -f1)
  month=$(echo $date | cut -d"." -f2)
  year=$(echo $date | cut -d"." -f3)
  shiftedtime=$(date -d"${year}-${month}-${day} 100 years" +%s)
  shiftedtimeMS=$(($shiftedtime*1000))
  echo "${shiftedtimeMS}" >> contract-enddate-new
done < contract-enddate

id=0
while read line
do
  echo $id >> contract-id
  id=$(($id + 1))
done < contract-type

while read line
do
  client_name=$(echo $line | cut -d";" -f1)
  family_name=$(echo $line | cut -d";" -f2)
  family_id=$(grep "$family_name" families | head -1 | cut -d";" -f1)
  client_id=$(cat clients | cut -d";" -f1,2,5 | grep "${family_id};${client_name}$" | head -1 | cut -d";" -f1)
  echo $client_id >> contract-client-id
done < contract-client-name

sed -e 's/ČSST/ČS/g' -e 's/GENERALI/Generali poj./g' -e 's/ČP$/Česká poj./g' -e 's/čp$/Česká poj./g' -e 's/ČS$/ČS poj./g' -e 's/čs$/ČS poj./g' -e 's/VSS KB/KB/g' -e 's/UNIQA/Uniqa poj./g' -e 's/KOOP/Kooperativa poj./g' -e 's/ING\./ING/g' -e 's/ING/ING poj./g' -e 's/ČS PF/ČS penz./g' -e 's/ČP PF/ČP penz./g' -e 's/Allianz/Allianz poj./g' -e 's/ALLIANZ/Allianz poj./g' -e 's/allianz/Allianz poj./g' -e 's/Amcico/Metlife poj./g' -e 's/wr/WR/g' -e 's/wR/WR/g' -e 's/WS/WSS/g' -e 's/ČMPF/ČSOB/g' -e 's/DR$/Deutscher Ring/g' -e 's/KB PF/KB/g' contract-institution > contract-institution-new

sed -e 's/hypotéka/MORTGAGE/g' -e 's/investice/INVESTMENT_INSURANCE/g' -e 's/investiční poj./INVESTMENT_INSURANCE/g' -e 's/penzijní/PENSION_FUND/g' -e 's/poj.domácnosti/HOME_INSURANCE/g' -e 's/poj.nemovitosti/BUILDING_INSURANCE/g' -e 's/stavební/BUILDING_SAVINGS/g' -e 's/úrazové/ACCIDENT_INSURANCE/g' -e 's/úvěr/CREDIT/g' -e 's/životní/LIFE_INSURANCE/g' contract-type > contract-type-new

sed -e 's/čtvrtletně/QUARTERLY/g' -e 's/měsíčně/MONTHLY/g' -e 's/ročně/ANNUAL/g' -e 's/pololetně/BIANNUAL/g' -e 's/jednorázově/ONE_OFF/g' contract-frequency > contract-frequency-new

paste -d";" contract-id contract-client-id contract-type-new contract-id-value contract-startdate-new contract-enddate-new contract-frequency-new contract-payment contract-target-amount contract-institution-new contract-semicolon > contracts

rm contract-*
