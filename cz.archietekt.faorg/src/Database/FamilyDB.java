package Database;

import Entity.Client;
import java.util.List;

/**
 * Interface describing database operations related to specific family.
 * 
 * @author kolage
 */
public interface FamilyDB extends EntityDB {
    
    /**
     * Fetch client records related to this family from DB.
     * @return list of clients
     */
    public List<? extends Client> getClients();
}
