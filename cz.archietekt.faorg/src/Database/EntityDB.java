package Database;

import Entity.Entity;
import java.util.List;

/**
 * Base interface describing database operations related to specific entity.
 * 
 * @author kolage
 */
public interface EntityDB {
    
    /**
     * Inserts entity in DB.
     */
    public void insert();
    
    /**
     * Update entity record in DB.
     */
    public void update();
    
    /**
     * Deletes entity record from DB.
     */
    public void delete();
    
    /**
     * Get all entity records.
     */
    public List<? extends Entity> get();
    
    /**
     * Get entity record from DB filtered by specific search string.
     * 
     * Search string is whole SQL string after "where" clause.
     */
    public List<? extends Entity> get(String searchString);
    
    /**
     * Get entity record from DB filtered by its id.
     */
    public Entity getById(Integer id);
}