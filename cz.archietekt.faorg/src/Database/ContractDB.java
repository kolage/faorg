package Database;

import Entity.Client;

/**
 * Interface describing database operations related to specific client contract.
 * 
 * @author kolage
 */
public interface ContractDB extends EntityDB {
    
    /**
     * Fetch parent client record from DB.
     * @return parent client
     */
    public Client getClient();
}
