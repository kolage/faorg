package Database.Impl;

import Entity.Adviser;
import Entity.Entity;
import Entity.Impl.AdviserImpl;
import Entity.Impl.EntityImpl;
import Entity.Reminder;
import Faorg.Common;
import Faorg.ProcessThread;
import Graphical.EntityListGUI;
import Graphical.LicenseKeyGUI;
import Graphical.MainGUI;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.swing.JOptionPane;
import org.ini4j.Wini;

/**
 * Class for manipulation between database and application. 
 * 
 * It contains all methods for fetching database objects except methods that are 
 * specific only for their parent entity objects.
 * 
 * @author kolage
 */
public final class DatabaseOperator {
    
    /**
     * Flag indicating if program is connected to remote database, otherwise it can only read last locally saved data.
     */
    private boolean readOnlyMode = false;
    
    /**
     * Connection to local application database.
     */
    private Connection localDatabaseConnection;
    
    /**
     * Connection to remote application database.
     */
    private Connection remoteDatabaseConnection;
    
    /**
     * Connection to local validation database.
     */
    private Connection applicationDatabaseConnection;
    
    /**
     * Database connector prefix.
     */
    private final String localConnectorPrefix = "jdbc:sqlite:";
    
    /**
     * Database connector prefix.
     */
    private final String remoteConnectorPrefix = "jdbc:mysql:";
    
    /**
     * Path to directory with settings files.
     */
    private static final String SETTINGS_DIR = "data/settings";
    
    /**
     * File name of database settings file.
     */
    private static final String DB_SETTINGS_FILENAME = "db.ini";
    
    /**
     * List of current reminders according to application start.
     */
    private final List<Entity> currentReminders = new ArrayList<>();
    
    /**
     * Connected adviser login.
     */
    private String login;
    
    /**
     * Application mark.
     * 
     * Marks are used to determine who (adviser) done last change of some specific object in remote DB.
     * It is used together with timestamp.
     * 
     */
    private Long mark;
    
    /**
     * Application start timestamp.
     */
    private Long applicationStartTimestamp;
    
    /**
     * Location of local database.
     */
    public final static String localDatabaseLocation = "data/db/faorg.db";
    
    /**
     * Location of local database.
     */
    public final static String applicationDatabaseLocation = "data/db/app.db";
    
    /**
     * Creates new database operator object.
     */
    public DatabaseOperator() {
        
        // register SQL drivers
        try {
            Class.forName("org.sqlite.JDBC");
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Can't initialize SQL driver.");
            System.err.println(ex);
            System.exit(1);
        }
        
        // initialization procedure
        init();
        
    }
    
    /**
     * Get last app start timestamp.
     * 
     * @return timestamp
     */
    public Long getLastAppStartTimestamp() {
        try {
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select timestamp from mark where login='" + getLogin() + "'");

            if (rs.next()) {
                return rs.getLong("timestamp");
            }
        } catch (SQLException ex) {
            System.out.println("Error while fetching last app start timestamp from DB.");
            localDBErrorMessage(ex);
        }
        
        return null;
    }
    
    /**
     * Get current timestamp taken from remote DB.
     * 
     * @return timestamp
     */
    public Long getCurrentTimestamp() {
        try {
            Statement statement = remoteDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select unix_timestamp() as timestamp");

            if (rs.next()) {
                return rs.getLong("timestamp");
            }
        } catch (SQLException ex) {
            System.out.println("Error while fetching timestamp from remote DB.");
            remoteDBErrorMessage(ex);
        }
        return null;
    }
    
    /**
     * Get if system is in read only mode.
     * @return True if system is in read only mode, otherwise false
     */
    public boolean isReadOnly() {
        return readOnlyMode;
    }
    
    /**
     * Set system to read only mode.
     */
    public void setReadOnly() {
        if(!isReadOnly()) {
            this.readOnlyMode = true;
            MainGUI mg = MainGUI.getInstance();
            if(mg != null) {
                mg.setTitle(mg.getTitle() + " - " + Common.bundle.getString("MAIN_READ_ONLY"));
            }
        }
    }
    
    /**
     * Get login of connected adviser.
     * @return adviser login
     */
    public String getLogin() {
        if(login != null) {
            return login;
        } else {
            return "";
        }
    }
    
    /**
     * Set login of connected adviser.
     * @param login login
     */
    public void setLogin(String login) {
        this.login = login;
    }
    
    /**
     * Get connected adviser.
     * @return adviser
     */
    public AdviserImpl getAdviser() {
        return new AdviserDBImpl(this).get("login='" + getLogin() + "'").get(0);
    }
    
    /**
     * Get connection to local database with clients data.
     * @return connection
     */
    public Connection getLocalDatabaseConnection() {
        return localDatabaseConnection;
    }
    
    /**
     * Get connection to remote database with clients data.
     * @return connection
     */
    public Connection getRemoteDatabaseConnection() {
        return remoteDatabaseConnection;
    }
    
    /**
     * Process method relevant to updateLocalDatabase method. Used for calling in
     * ProcessThread instance.
     * 
     * @param pt Process thread instance
     */
    public void updateLocalDatabaseProcess(ProcessThread pt) {
        
        if(!isReadOnly()) {
            boolean exists;

            try {
                String searchString = "mark <> " + getLastMark() + " and timestamp > " + getLastAppStartTimestamp();

                EntityDBImpl[] dbEntities = new EntityDBImpl[] {
                    new FamilyDBImpl(this),
                    new ClientDBImpl(this),
                    new ContractDBImpl(this),
                    new ReminderDBImpl(this)
                };

                pt.setMaxState(dbEntities.length);

                int i = 0;
                
                for(EntityDBImpl entityDB : dbEntities) {
                    pt.setText(Common.bundle.getString("UPDATE_ENTITY") + " " + entityDB.getTableName());
                    
                    for (EntityImpl itEntity : entityDB.getWorker(getRemoteDatabaseConnection(), searchString)) {
                        exists = entityDB.getWorker(getLocalDatabaseConnection(),
                                "id='" + itEntity.getId() + "'").size() > 0;
                        if (!exists) {
                            itEntity.getDBOperator().insertWorker(getLocalDatabaseConnection());
                        } else {
                            itEntity.getDBOperator().updateWorker(getLocalDatabaseConnection());
                        }
                        
                        if(itEntity instanceof Reminder) {
                            currentReminders.add(itEntity);
                        }
                    }
                    
                    List<Integer> missingIds = entityDB.getMissingIds();

                    for (Integer id : missingIds) {
                        EntityImpl entityImpl = entityDB.getById(id);
                        entityImpl.getDBOperator().deleteWorker(getLocalDatabaseConnection());
                    }
                    
                    pt.nextProgress();
                }
                
                pt.nextProgress();

                setNewTimestamp();
            } catch(SQLException ex) {
                System.out.println("Error while updating local DB.");
                remoteDBErrorMessage(ex);
                pt.destroyGUI();
            } catch (IOException ex) {
                // Should not happen
            }
        }
    }
    
    private void updateAdvisers() {
        if(!isReadOnly()) {
            AdviserDBImpl adviserDB = new AdviserDBImpl(this);

            try {
                for (AdviserImpl itAdviser : adviserDB.getWorker(getLocalDatabaseConnection(), "")) {
                    itAdviser.getDBOperator().deleteWorker(localDatabaseConnection);
                }
            } catch(SQLException ex) {
                System.out.println("Error while updating advisers (first step - removing from local DB).");
                localDBErrorMessage(ex);
            } catch (IOException ex) {
                // Should not happen
            }
            
            try {
                for (AdviserImpl itAdviser : adviserDB.getWorker(getRemoteDatabaseConnection(), "")) {
                    itAdviser.getDBOperator().insertWorkerProc(getLocalDatabaseConnection(),true);
                }
            } catch(SQLException ex) {
                System.out.println("Error while updating advisers (second step - updating from remote DB).");
                remoteDBErrorMessage(ex);
            } catch (IOException ex) {
                // Should not happen
            }
        }
    }
    
    public void addReminder(Reminder reminder) {
        currentReminders.add(reminder);
    }
    
    public List<Entity> getCurrentReminders() {
        return currentReminders;
    }
    
    /**
     * Process method relevant to checkReminders method. Used for calling in
     * ProcessThread instance.
     * 
     * @param pt
     */
    public void checkRemindersProcess(ProcessThread pt) {
        pt.setMaxState(1);
        
        pt.setText(Common.bundle.getString("LOADING_REMINDER"));
        
        currentReminders.addAll(new ReminderDBImpl(this).getCurrentReminders());

        pt.nextProgress();

        if(!currentReminders.isEmpty()) {
            EntityListGUI.getInstance(this, Reminder.class, currentReminders, true).setVisible(true);
        }
    }
    
    /**
     * Generates Faorg application code.
     * @return application code
     */
    private String generateAppCode() {
        return new BigInteger(130, new SecureRandom()).toString(32).
                substring(0, 4);
    }
    
    /**
     * Add new adviser to DB.
     * @param login adviser login
     * @param password adviser password
     * @return true if successfully added, otherwise false
     */
    private boolean addNewAdviser(String login, String password) {
        if (!login.isEmpty()) {
            Adviser adviser = new AdviserImpl(this,login);
            adviser.setPassword(password);

            adviser.getDBOperator().insert();
            
            // check if it was really added into DB
            return adviser.getDBOperator().get("login='" + login + "'").size() > 0;
        } else {
            JOptionPane.showMessageDialog(null,
                    Common.bundle.getString("NEW_ADVISER_LOGIN-EMPTY"),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            
            return false;
        }
    }
    
    /**
     * Checks if there is stored valid application hash code.
     * @return true if there is valid app hash code, false otherwise
     */
    private boolean validAppHash(String checkHash) {
        String realHash = DatabaseOperator.getHashedString(DatabaseOperator.localDatabaseFileModifiedDate());
        return realHash.equals(checkHash);
    }
    
    /**
     * Check if program license is expired.
     * @param isThereAlreadyAdviser if there is already adviser in DB
     * @return true if license is expired, otherwise false
     */
    private boolean isLicenseExpired(boolean isThereAnyAdviser) {
        String checkHash  = Common.getPreferences().get(
                Common.FAORG_LICENSE_CHECK_PREFERENCE,
                null);
        
        if((checkHash == null && isThereAnyAdviser) || (isThereAnyAdviser && !validAppHash(checkHash))) {
            return true;
        }
        
        Date expiryDate = getExpiryDate();
        if(expiryDate != null) {
            return expiryDate.compareTo(new Date()) < 0;
        } else {
            return true;
        }
    }
    
    public boolean login(String login, String password) {
        setLogin(login);
        AdviserDBImpl adviserDB = new AdviserDBImpl(this);
        
        boolean isThereAnyAdviser = adviserDB.get().size() > 0;
        boolean loginExists = adviserDB.get("login='" + login + "'").size() > 0;
        
        if(!isValidLogin(login, password)) {
            if(isReadOnly() || loginExists) {
                JOptionPane.showMessageDialog(null,
                        Common.bundle.getString("LOGIN_BAD-LOGIN"),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                int answer = JOptionPane.showOptionDialog(
                        null,
                        Common.bundle.getString("LOGIN_NEW-TEXT"),
                        Common.bundle.getString("LOGIN_NEW-TITLE"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        new String[] { 
                            Common.bundle.getString("MAIN_CONFIRM-OK"),
                            Common.bundle.getString("MAIN_CONFIRM-NO")
                        },
                        null);
                if (answer == JOptionPane.NO_OPTION) {
                    return false;
                } else {
                    boolean isSuccessfullyAdded = addNewAdviser(login, password);
                    if (isSuccessfullyAdded) {
                        JOptionPane.showMessageDialog(null,
                                Common.bundle.getString("LOGIN_CREATED-MESSAGE"),
                                Common.bundle.getString("LOGIN_CREATED-TITLE"),
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                Common.bundle.getString("LOGIN_BAD-NOREMOTE"),
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            }
        }
        
        if(getApplicationCode() == null) {
            insertApplicationCode(generateAppCode());
        }
        
        if (!isLicenseExpired(isThereAnyAdviser)) {
            return true;
        } else {
            LicenseKeyGUI.getInstance(this).setVisible(true);
        }
        
        return false;
    }
    
    /**
     * Get application code.
     * @return application code
     */
    public String getApplicationCode() {
        try {
            Statement statement = applicationDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select code from faorg");

            if (rs.next()) {
                return Common.decryptString(rs.getString("code"));
            }
        } catch (SQLException ex) {
            System.out.println("Error while fetching application code from DB.");
            localDBErrorMessage(ex);
        }
        
        return null;
    }
    
    /**
     * Get program expiry date.
     * @return expiry date
     */
    public Date getExpiryDate() {
        try {
            Statement statement = applicationDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select expiry_date from faorg");

            if (rs.next()) {
                Date expiryDate = Common.unshiftTime(Long.parseLong(
                        Common.decryptString(rs.getString("expiry_date"))));
                return expiryDate;
            }
        } catch (SQLException | NumberFormatException ex) {
            System.out.println("Error while fetching program expiry date from DB.");
            localDBErrorMessage(ex);
        }
        
        return null;
    }
    
    /**
     * Insert new application code to DB.
     * @param applicationCode application code
     */
    public void insertApplicationCode(String applicationCode) {
        try {
            Statement statement = applicationDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
            
            String encryptedAppCode = Common.encryptString(applicationCode);
            
            String encryptedExpiryDate = Common.encryptString(String.valueOf(
                    Common.getShiftedTime(new Date())));
            
            statement.executeUpdate("insert into faorg (code, expiry_date) "
                    + "values ("
                    + "'" + encryptedAppCode + "',"
                    + "'" + encryptedExpiryDate + "')");
        } catch (SQLException ex) {
            System.out.println("Error while inserting license data to DB.");
            localDBErrorMessage(ex);
        }
        
        Common.getPreferences().put(Common.FAORG_LICENSE_CHECK_PREFERENCE, 
                    DatabaseOperator.
                    getHashedString(localDatabaseFileModifiedDate()));
    }
    
    /**
     * Update program expiry date.
     * @param expiryDate application expiry date
     */
    public void updateExpiryDate(Date expiryDate) {
        try {
            Statement statement = applicationDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
            
            String encryptedExpiryDate = Common.encryptString(String.valueOf(
                    Common.getShiftedTime(expiryDate)));
            
            statement.executeUpdate("update faorg set "
                    + "expiry_date='" + encryptedExpiryDate + "'");
        } catch (SQLException ex) {
            System.out.println("Error while updating application expiry date in DB.");
            localDBErrorMessage(ex);
        }
        
        Common.getPreferences().put(Common.FAORG_LICENSE_CHECK_PREFERENCE, 
                    DatabaseOperator.
                    getHashedString(localDatabaseFileModifiedDate()));
    }
    
    /**
     * Checks if input pair is valid login.
     * @param login tested login
     * @param password tested password
     * @return true if input pair is valid login, false otherwise
     */
    public boolean isValidLogin(String login, String password) {
        
        String hashedPassword = getHashedString(password);
        
        try {
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select login from adviser "
                    + "where login='" + login + "' and password='" + 
                    hashedPassword + "'");
            
            // is there any record with this login and password?
            if(rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error while checking login in DB.");
            localDBErrorMessage(ex);
        }
        
        return false;
    }
    
    public void insertInitialMark() {
        try {
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            statement.executeUpdate("insert into mark (value,timestamp,login) values ('" + getMark() + "',0,'" + 
                    getLogin() + "')");
        } catch (SQLException | NumberFormatException ex) {
            System.out.println("Error while inserting initial program mark and timestamp to DB.");
            localDBErrorMessage(ex);
        }
    }
    
    /**
     * Get actual mark value.
     * 
     * @return mark
     */
    public Long getMark() {
        return mark;
    }
    
    /**
     * Set new application start timestamp.
     */
    public void setNewTimestamp() {
        applicationStartTimestamp = getCurrentTimestamp();
        
        try {
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            statement.executeUpdate("update mark set timestamp=" + applicationStartTimestamp + " " +
                    "where login='" + getLogin() + "'");
        } catch (SQLException ex) {
            System.out.println("Error while setting new mark in DB.");
            localDBErrorMessage(ex);
        }
    }
    
    /**
     * Get mark that was used last time by this application.
     * 
     * @return mark
     */
    public Long getLastMark() {
        try {
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);

            ResultSet rs = statement.executeQuery("select value from mark where login='" + getLogin() + "'");

            if (rs.next()) {
                return rs.getLong("value");
            }
        } catch (SQLException ex) {
            System.out.println("Error while fetching last used mark from DB.");
            localDBErrorMessage(ex);
        }
        
        return null;
    }
    
    /**
     * Get mark if exists and set mark variable.
     */
    public void setMark() {
        Long tempMark = getLastMark();
        applicationStartTimestamp = 0L;
        
        if(tempMark != null) {
            mark = tempMark;
        } else {
            mark = new Random().nextLong();
            insertInitialMark();
        }
    }
    
    /**
     * Should be called before application shutdown for correct DB 
     * disconnection.
     */
    public void closeConnections() {
        try {
            if (remoteDatabaseConnection != null) {
                remoteDatabaseConnection.close();
            }
            if (localDatabaseConnection != null) {
                localDatabaseConnection.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error while closing SQL connection.");
            System.err.println(ex);
            System.exit(1);
        }
    }
    
    /**
     * Generates new identificator that is not included in DB yet.
     * @param tableName name of table where the new id will be searched
     * @return new ID
     */
    public Integer generateNewId(String tableName) {
        try {
            Statement statement = remoteDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
            
            ResultSet rs = statement.executeQuery("select id from " + tableName
                    + " order by abs(id) desc");
            
            if(rs.next()) {
                Integer largestId = rs.getInt("id");
                return ++largestId;
            } 
        } catch (SQLException ex) {
            System.out.println("Error while generating new ID from DB.");
            remoteDBErrorMessage(ex);
        }
        
        return 1;
    }
    
    public void localDBErrorMessage(Throwable ex) {
        System.out.println("Error while working with local DB.");
        System.err.println(ex);

        JOptionPane.showMessageDialog(null,
                Common.bundle.getString("LOGIN_LOCAL_FAIL-TEXT"),
                Common.bundle.getString("LOGIN_LOCAL_FAIL-TITLE"),
                JOptionPane.ERROR_MESSAGE);

        System.exit(1);
    }
    
    public void remoteDBErrorMessage(Throwable ex) {
        System.out.println("Error while working with remote DB.");
        System.err.println(ex);

        int answer = JOptionPane.showOptionDialog(
                null,
                Common.bundle.getString("LOGIN_REMOTE_FAIL-TEXT"),
                Common.bundle.getString("LOGIN_REMOTE_FAIL-TITLE"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[]{Common.bundle.getString("MAIN_CONFIRM-OK"),
            Common.bundle.getString("MAIN_CONFIRM-NO")},
                null);
        if (answer == JOptionPane.NO_OPTION) {
            System.exit(1);
        } else {
            setReadOnly();
        }
    }
    
    private void createTablesProc(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);

        // creates adviser table
        statement.executeUpdate("create table if not exists adviser ("
                + "login varchar(255) NOT NULL,"
                + "password varchar(255),"
                + "firstName varchar(255),"
                + "lastName varchar(255),"
                + "telephone varchar(255),"
                + "email varchar(255),"
                + "website varchar(255),"
                + "title varchar(255),"
                + "street varchar(255),"
                + "city varchar(255),"
                + "zip_code varchar(255),"
                + "position varchar(255),"
                + "ico varchar(255),"
                + "office_street varchar(255),"
                + "office_city varchar(255),"
                + "office_zip_code varchar(255),"
                + "PRIMARY KEY (login)"
                + ")");
        
        // create family table
        statement.executeUpdate("create table if not exists family ("
                + "id INTEGER NOT NULL,"
                + "name varchar(255) NOT NULL,"
                + "street varchar(255),"
                + "city varchar(255),"
                + "zip_code varchar(255),"
                + "postal_street varchar(255),"
                + "postal_city varchar(255),"
                + "postal_zip_code varchar(255),"
                + "family_level varchar(255),"
                + "family_property varchar(255),"
                + "house_type varchar(255),"
                + "last_visit BIGINT,"
                + "envelope_title varchar(255),"
                + "letter_title varchar(255),"
                + "login varchar(255) NOT NULL,"
                + "mark BIGINT DEFAULT -1 NOT NULL,"
                + "timestamp BIGINT NOT NULL,"
                + "PRIMARY KEY (id)"
                + ")");

        // create client table
        statement.executeUpdate("create table if not exists client ("
                + "id INTEGER NOT NULL,"
                + "family_id INTEGER NOT NULL,"
                + "identification_number varchar(255) NOT NULL,"
                + "age INTEGER,"
                + "birth BIGINT DEFAULT 0 NOT NULL,"
                + "first_name varchar(255) NOT NULL,"
                + "doctor varchar(255),"
                + "identity_card_number varchar(255),"
                + "identity_card_valid_date BIGINT,"
                + "identity_card_issuer varchar(255),"
                + "identity_card_birth_place varchar(255),"
                + "income MEDIUMINT,"
                + "working_condition varchar(255),"
                + "profession varchar(255),"
                + "sport_level varchar(255),"
                + "sport_type varchar(255),"
                + "gender varchar(255),"
                + "bank_account_number varchar(255),"
                + "bank_code varchar(255),"
                + "note varchar(255),"
                + "login varchar(255) NOT NULL,"
                + "mark BIGINT DEFAULT -1 NOT NULL,"
                + "timestamp BIGINT NOT NULL,"
                + "PRIMARY KEY (id),"
                + "FOREIGN KEY (family_id) REFERENCES family(id)"
                + ")");

        // create client telephones table
        statement.executeUpdate("create table if not exists telephone ("
                + "client_id INTEGER NOT NULL,"
                + "number varchar(255) NOT NULL,"
                + "FOREIGN KEY (client_id) REFERENCES client(id)"
                + ")");

        // create client emails table
        statement.executeUpdate("create table if not exists email ("
                + "client_id INTEGER NOT NULL,"
                + "address varchar(255) NOT NULL,"
                + "FOREIGN KEY (client_id) REFERENCES client(id)"
                + ")");

        // create contract table
        statement.executeUpdate("create table if not exists contract ("
                + "id INTEGER NOT NULL,"
                + "client_id INTEGER NOT NULL,"
                + "type varchar(255) NOT NULL,"
                + "contract_id_value varchar(255),"
                + "start_date BIGINT,"
                + "start_date_wo_year BIGINT,"
                + "end_date BIGINT,"
                + "frequency varchar(255),"
                + "payment MEDIUMINT,"
                + "target_amount INTEGER,"
                + "institution varchar(255),"
                + "note varchar(255),"
                + "login varchar(255) NOT NULL,"
                + "mark BIGINT DEFAULT -1 NOT NULL,"
                + "timestamp BIGINT NOT NULL,"
                + "PRIMARY KEY (id),"
                + "FOREIGN KEY (client_id) REFERENCES client(id)"
                + ")");
        
        // create reminder table
        statement.executeUpdate("create table if not exists reminder ("
                + "id INTEGER NOT NULL,"
                + "object_id INTEGER,"
                + "type varchar(255) NOT NULL,"
                + "text varchar(255),"
                + "date BIGINT,"
                + "target_date BIGINT,"
                + "login varchar(255) NOT NULL,"
                + "mark BIGINT DEFAULT -1 NOT NULL,"
                + "timestamp BIGINT NOT NULL,"
                + "PRIMARY KEY (id)"
                + ")");
    }
    
    private void createTables() {
        // create local tables
        try {
            createTablesProc(localDatabaseConnection);
        } catch(SQLException ex) {
            localDBErrorMessage(ex);
        }
        
        // create remote tables
        if(!isReadOnly()) {
            try {
                createTablesProc(remoteDatabaseConnection);
            } catch(SQLException ex) {
                remoteDBErrorMessage(ex);
            }
        }
    }

    /**
     * Database operators initialization.
     */
    private void init() {
        // first try to log into remote database
        try {
            Wini ini = new Wini(new File(SETTINGS_DIR + "/" + DB_SETTINGS_FILENAME));
            String remoteDatabaseLocation = ini.get("DbConnection", "Location");
            String password = ini.get("DbConnection", "Password");
            
            remoteDatabaseConnection = DriverManager.getConnection(remoteConnectorPrefix + remoteDatabaseLocation +
                    "?characterEncoding=cp1250", "faorg", getHashedString(password));
            
            Statement statement = remoteDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
        } catch(SQLException | IOException ex) {
            remoteDBErrorMessage(ex);
        }
        
        // log into local database and create local-only tables
        try {
            localDatabaseConnection = DriverManager.getConnection(localConnectorPrefix + localDatabaseLocation);
            
            Statement statement = localDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
            
            // create mark table if not exists
            statement.executeUpdate("create table if not exists mark ("
                    + "value BIGINT NOT NULL,"
                    + "timestamp BIGINT NOT NULL,"
                    + "login varchar(255) NOT NULL,"
                    + "PRIMARY KEY (login)"
                    + ")");
        } catch(SQLException ex) {
            localDBErrorMessage(ex);
        }
        
        // log into local validation database and create local-only tables
        try {
            applicationDatabaseConnection = 
                    DriverManager.getConnection(localConnectorPrefix + applicationDatabaseLocation);
            
            Statement statement = applicationDatabaseConnection.createStatement();
            statement.setQueryTimeout(10);
            
            // create faorg application table if not exists
            statement.executeUpdate("create table if not exists faorg ("
                    + "code varchar(255) NOT NULL,"
                    + "expiry_date INTEGER NOT NULL,"
                    + "PRIMARY KEY (code)"
                    + ")");
        } catch(SQLException ex) {
            localDBErrorMessage(ex);
        }
        
        createTables();
        
        updateAdvisers();
    }

    /**
     * Get local database file creation date.
     * @return app DB file creation date
     */
    public static String localDatabaseFileModifiedDate() {
        try {
            BasicFileAttributes attrs = Files.readAttributes(Paths.get(applicationDatabaseLocation),
                    BasicFileAttributes.class);
            return attrs.lastModifiedTime().toString();
        } catch (IOException ex) {
            System.err.println(ex);
            System.exit(1);
        }
        return null;
    }
    
    /**
     * Creates MD5 hashed string.
     * @param input input string
     * @return hashed string
     */
    public static String getHashedString(String input) {
        String hashText = null;
        
        try {
            byte[] passwordBytes = input.getBytes("UTF-8");
            
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] theDigest = md.digest(passwordBytes);

            BigInteger bigInt = new BigInteger(1, theDigest);
            hashText = bigInt.toString(16);
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            System.err.println(ex);
            System.exit(1);
        }

        return hashText;
    }
}