package Database.Impl;

import Database.ReminderDB;
import Entity.Impl.EntityImpl;
import Entity.Impl.ReminderImpl;
import Faorg.Common;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of ReminderDB interface.
 * 
 * @author kolage
 */
public final class ReminderDBImpl extends EntityDBImpl implements ReminderDB {

    /**
     * Reference to reminder object.
     */
    private ReminderImpl reminder;
    
    /**
     * Creates new instance of ReminderDB interface implementation.
     * @param operator database operator
     */
    public ReminderDBImpl(DatabaseOperator operator) {
        super(operator);
    }
    
    /**
     * Creates new instance of ReminderDB interface implementation.
     * @param reminder related reminder object
     * @param operator database operator
     */
    public ReminderDBImpl(ReminderImpl reminder, DatabaseOperator operator) {
        super(operator);
        this.reminder = reminder;
    }
    
    @Override
    public String getTableName() {
        return "reminder";
    }
    
    @Override
    public void insertWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("insert into " + getTableName()
                + " (id, object_id, type, text, date, target_date, login, mark, timestamp) values ("
                + reminder.getId() + ","
                + reminder.getRemindedObjectId() + ","
                + "'" + reminder.getType().toString() + "',"
                + "'" + reminder.getText() + "',"
                + Common.getShiftedTime(reminder.getDate()) + ","
                + Common.getShiftedTime(reminder.getTargetDate()) + ","
                + "'" + operator.getLogin() + "',"
                + operator.getMark() + ","
                + operator.getCurrentTimestamp() + ")");
    }
    
    @Override
    public void updateWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("update " + getTableName() + " set "
                + "mark=" + operator.getMark() + ","
                + "timestamp="+ operator.getCurrentTimestamp() + ","
                + "object_id=" + reminder.getRemindedObjectId() + ","
                + "type='" + reminder.getType().toString() + "',"
                + "text='" + reminder.getText() + "',"
                + "date=" + Common.getShiftedTime(reminder.getDate()) + ","
                + "target_date=" + Common.getShiftedTime(reminder.getTargetDate()) + " "
                + "where id=" + reminder.getId());
    }
    
    @Override
    public void deleteWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("delete from " + getTableName() + " where "
                + "id=" + reminder.getId());
    }
    
    /**
     * Auxiliary method for fetching reminder record from result set.
     * @param rs result set
     * @param connection related database connection
     * @return reminder
     */
    private ReminderImpl fetchReminderFromResultSet(ResultSet rs, Connection connection) throws SQLException {
        ReminderImpl reminderImpl;
        
        Integer id = rs.getInt("id");

        reminderImpl = new ReminderImpl(operator, id);
        reminderImpl.setType(rs.getString("type"));
        reminderImpl.setRemindedObjectId(rs.getInt("object_id"));
        reminderImpl.setText(rs.getString("text"));
        reminderImpl.setDate(Common.unshiftTime(rs.getLong("date")));
        reminderImpl.setTargetDate(Common.unshiftTime(rs.getLong("target_date")));
        
        if(reminderImpl.isExpired()) {
            reminderImpl.getDBOperator().deleteWorker(connection);
        }
        
        return reminderImpl;
    }
    
    @Override
    protected List<ReminderImpl> getWorker(Connection connection, String searchString) 
            throws SQLException, IOException {
        List<ReminderImpl> resultList = new ArrayList<>();

        String query = "select * from " + getTableName() + setWhereString(searchString);
        
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setQueryTimeout(10);
        
        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            resultList.add(fetchReminderFromResultSet(rs,connection));
        }
        
        return resultList;
    }
    
    @Override
    public List<ReminderImpl> get() {
        return (List<ReminderImpl>) super.get();
    }
    
    @Override
    public List<ReminderImpl> get(String searchString) {
        return (List<ReminderImpl>) super.get(searchString);
    }
    
    @Override
    public ReminderImpl getById(Integer id) {
        List<ReminderImpl> reminders = get("id=" + id);
        
        if(reminders.size() > 0) {
            return reminders.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public EntityImpl getParentObject() {
        switch(reminder.getType()) {
            case BIRTHDAY:
            case MEETING:
            case CLIENT:
                return new ClientDBImpl(operator).getById(reminder.getRemindedObjectId());
            case LAST_VISIT:
            case FAMILY:
                return new FamilyDBImpl(operator).getById(reminder.getRemindedObjectId());
            case CONTRACT:
            case CONTRACT_END:
            case CONTRACT_ANNIVERSARY:
                return new ContractDBImpl(operator).getById(reminder.getRemindedObjectId());
            default:
                return null;
        }
    }
    
    protected List<ReminderImpl> getCurrentAutoReminders() {
        List<ReminderImpl> currentReminders = new ArrayList<>();

        currentReminders.addAll(new ClientDBImpl(operator).getCurrentReminders());
        currentReminders.addAll(new ContractDBImpl(operator).getCurrentReminders());
        currentReminders.addAll(new FamilyDBImpl(operator).getCurrentReminders());
        
        return currentReminders;
    }
    
    protected List<ReminderImpl> getCurrentManualReminders() {
        List<ReminderImpl> reminders = new ArrayList<>();
        
        for(ReminderImpl reminderIt : get("date <= " + Common.getShiftedTime(Common.getTodayDateAtEnd()))) {
            reminders.add(reminderIt);
        }
        
        return reminders;
    }
    
    @Override
    public List<ReminderImpl> getCurrentReminders() {
        List<ReminderImpl> reminders = new ArrayList<>();

        reminders.addAll(getCurrentAutoReminders());
        reminders.addAll(getCurrentManualReminders());
        
        return reminders;
    }

    @Override
    protected List<ReminderImpl> getReminders() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
