package Database.Impl;

import Database.EntityDB;
import Entity.Impl.EntityImpl;
import Entity.Impl.ReminderImpl;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Base abstract class for entity database operations.
 * 
 * @author kolage
 */
public abstract class EntityDBImpl implements EntityDB {
    
    /**
     * Path to directory with CSV export files.
     */
    public static final String EXPORT_DIR = "data/export";
    
    /**
     * Reference to database operator.
     */
    protected DatabaseOperator operator;
    
    /**
     * Insert worker method.
     * 
     * @param connection Connection for executing query.
     * @throws java.sql.SQLException
     */
    public abstract void insertWorker(Connection connection) throws SQLException;
    
    /**
     * Update worker method.
     * @param connection Connection for executing query.
     * @throws java.sql.SQLException
     */
    public abstract void updateWorker(Connection connection) throws SQLException;
    
    /**
     * Delete worker method.
     * @param connection Connection for executing query.
     * @throws java.sql.SQLException
     */
    public abstract void deleteWorker(Connection connection) throws SQLException;
    
    /**
     * Get worker method.
     * 
     * @param connection Connection for executing query.
     * @param searchString Whole string after where clause.
     * 
     * @return List of found results extending EntityImpl class.
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    protected abstract List<? extends EntityImpl> getWorker(Connection connection, String searchString) throws SQLException, IOException;
    
    /**
     * Creates new instance of entity database operations implementation.
     * @param operator reference to database operator
     */
    public EntityDBImpl(DatabaseOperator operator) {
        this.operator = operator;
    }
    
    protected abstract String getTableName();
    
    private Method findMethodByName(String methodName) {
        for(Method itMethod : this.getClass().getDeclaredMethods()) {
            if(itMethod.getName().equals(methodName)) {
                return itMethod;
            }
        }
        return null;
    }
    
    protected final void writeActionWrapper(String methodName) {
        if(!operator.isReadOnly()) {
            Method method = null;
            
            try {
                method = findMethodByName(methodName);
                method.invoke(this, operator.getRemoteDatabaseConnection());
            } catch (InvocationTargetException ex) {
                if (ex.getTargetException() instanceof SQLException) {
                    operator.remoteDBErrorMessage(ex.getTargetException());
                    return;
                }
            } catch (IllegalAccessException | IllegalArgumentException ex) {
                System.out.println("Write action wrapper exception.");
                System.err.println(ex);
                System.exit(1);
            }
            
            try {
                method.invoke(this, operator.getLocalDatabaseConnection());
            } catch (InvocationTargetException ex) {
                if (ex.getTargetException() instanceof SQLException) {
                    operator.localDBErrorMessage(ex.getTargetException());
                }
            } catch (IllegalAccessException | IllegalArgumentException ex) {
                System.out.println("Write action wrapper exception.");
                System.err.println(ex);
                System.exit(1);
            }
        }
    }
    
    protected final List<EntityImpl> readActionWrapper(String methodName, String searchString) {
        Method method = findMethodByName(methodName);
        
        try {
            return (List<EntityImpl>) method.invoke(this, operator.getLocalDatabaseConnection(), searchString);
        } catch(InvocationTargetException ex) {
            if(ex.getTargetException() instanceof SQLException) {
                operator.localDBErrorMessage(ex.getTargetException());
            }
        } catch(IllegalAccessException | IllegalArgumentException ex) {
            System.out.println("Read action wrapper exception.");
            System.err.println(ex);
            System.exit(1);
        }
        
        return null;
    }
    
    protected String setWhereString(String searchString) {
        String whereString;
        
        if(searchString.isEmpty()) {
            whereString = " where login='" + operator.getLogin() + "'";
        } else {
            whereString = " where " + searchString + " and login='" + operator.getLogin() + "'";
        }
        
        return whereString;
    }
    
    private List<Integer> getIds(Connection connection) throws SQLException {
        List<Integer> ids = new ArrayList<>();
        
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        ResultSet rs = statement.executeQuery("select id from " + getTableName() + " where login='" + 
                operator.getLogin() + "'");

        while(rs.next()) {
            ids.add(rs.getInt("id"));
        }
        
        return ids;
    }
    
    protected final List<Integer> getMissingIds() {
        List<Integer> localIds, remoteIds;
        
        try {
            remoteIds = getIds(operator.getRemoteDatabaseConnection());
        } catch(SQLException ex) {
            operator.remoteDBErrorMessage(ex);
            return null;
        }
        
        try {
            localIds = getIds(operator.getLocalDatabaseConnection());
        } catch(SQLException ex) {
            operator.localDBErrorMessage(ex);
            return null;
        }
        
        localIds.removeAll(remoteIds);
        
        return localIds;
    }
    
    @Override
    public void insert() {
        writeActionWrapper("insertWorker");
    }
    
    @Override
    public void update() {
        writeActionWrapper("updateWorker");
    }
    
    @Override
    public void delete() {
        writeActionWrapper("deleteWorker");
    }
    
    @Override
    public List<? extends EntityImpl> get() {
        return readActionWrapper("getWorker", "");
    }
    
    @Override
    public List<? extends EntityImpl> get(String searchString) {
        return readActionWrapper("getWorker", searchString);
    }
    
    @Override
    public EntityImpl getById(Integer id) {
        return null;
    }
    
    protected abstract List<ReminderImpl> getCurrentReminders();
    
    protected abstract List<ReminderImpl> getReminders();
}