package Database.Impl;

import Database.ClientDB;
import Entity.Client.Gender;
import Entity.Client.WorkingCondition;
import Entity.Impl.ClientImpl;
import Entity.Impl.ContractImpl;
import Entity.Impl.FamilyImpl;
import Entity.Impl.ReminderImpl;
import Entity.Reminder.ReminderType;
import Faorg.Common;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Implementation of ClientDB interface.
 * 
 * @author kolage
 */
public final class ClientDBImpl extends EntityDBImpl implements ClientDB {

    /**
     * Reference to client object.
     */
    private ClientImpl client;
    
    /**
     * Creates new instance of ClientDB interface implementation.
     * @param operator database operator
     */
    public ClientDBImpl(DatabaseOperator operator) {
        super(operator);
    }
    
    /**
     * Creates new instance of ClientDB interface implementation.
     * @param client related client object
     * @param operator database operator
     */
    public ClientDBImpl(ClientImpl client, DatabaseOperator operator) {
        super(operator);
        this.client = client;
    }
    
    @Override
    public String getTableName() {
        return "client";
    }
    
    @Override
    public void insertWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("insert into " + getTableName()
                + " (id, family_id, identification_number, age, birth, doctor, "
                + "first_name, identity_card_number, identity_card_valid_date, identity_card_issuer, "
                + "identity_card_birth_place, income, working_condition, profession, sport_level, sport_type, gender, "
                + "bank_account_number, bank_code, note, login, mark, timestamp) values ("
                + client.getId() + ","
                + client.getFamilyId() + ","
                + "'" + client.getIdentificationNumber() + "',"
                + client.getAge() + ","
                + Common.getShiftedTime(Common.getDateWithoutYear(client.getDateOfBirth())) + ","
                + "'" + client.getDoctor() + "',"
                + "'" + client.getFirstName() + "',"
                + "'" + client.getIdentificationNumber() + "',"
                + Common.getShiftedTime(client.getIdentityCardValidity()) + ","
                + "'" + client.getIdentityCardIssuer() + "',"
                + "'" + client.getBirthPlace() + "',"
                + client.getIncome() + ","
                + "'" + client.getWorkingCondition().toString() + "',"
                + "'" + client.getProfession() + "',"
                + "'" + client.getSportLevel().toString() + "',"
                + "'" + client.getSportType() + "',"
                + "'" + client.getGender().toString() + "',"
                + "'" + client.getBankAccountNumber() + "',"
                + "'" + client.getBankCode() + "',"
                + "'" + client.getNote() + "',"
                + "'" + operator.getLogin() + "',"
                + operator.getMark() + ","
                + operator.getCurrentTimestamp() + ")");

        addEmailsAndTelephones(connection);
    }
    
    @Override
    public void updateWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("update " + getTableName() + " set "
                + "mark=" + operator.getMark() + ","
                + "timestamp=" + operator.getCurrentTimestamp() + ","
                + "family_id=" + client.getFamilyId() + ","
                + "first_name='" + client.getFirstName() + "',"
                + "identification_number='" + client.getIdentificationNumber() + "',"
                + "age=" + client.getAge() + ","
                + "birth=" + Common.getShiftedTime(Common.getDateWithoutYear(client.getDateOfBirth())) + ","
                + "doctor='" + client.getDoctor() + "',"
                + "identity_card_number='" + client.getIdentityCardNumber() + "',"
                + "identity_card_valid_date=" + Common.getShiftedTime(client.getIdentityCardValidity()) + ","
                + "identity_card_issuer='" + client.getIdentityCardIssuer() + "',"
                + "identity_card_birth_place='" + client.getBirthPlace() + "',"
                + "income=" + client.getIncome() + ","
                + "working_condition='" + client.getWorkingCondition().toString() + "',"
                + "profession='" + client.getProfession() + "',"
                + "sport_level='" + client.getSportLevel().toString() + "',"
                + "sport_type='" + client.getSportType() + "',"
                + "gender='" + client.getGender() + "',"
                + "bank_account_number='"
                + client.getBankAccountNumber() + "',"
                + "bank_code='" + client.getBankCode() + "',"
                + "note='" + client.getNote() + "' "
                + "where id=" + client.getId());

        // delete present emails and telephones and add new ones
        deleteEmailsAndTelephones(connection);
        addEmailsAndTelephones(connection);
    }

    @Override
    public void deleteWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        deleteEmailsAndTelephones(connection);

        for(ContractImpl contract : getContracts()) {
            contract.getDBOperator().deleteWorker(connection);
        }

        for(ReminderImpl reminder : getReminders()) {
            reminder.getDBOperator().deleteWorker(connection);
        }

        statement.executeUpdate("delete from " + getTableName() + " where id=" + client.getId());
    }
    
    /**
     * Auxiliary method for fetching client record from result set.
     * @param rs result set
     * @return client
     */
    private ClientImpl fetchClientFromResultSet(Connection connection, ResultSet rs) throws SQLException {
        ClientImpl clientImpl;
        
        Integer id = rs.getInt("id");
        Integer familyId = rs.getInt("family_id");

        clientImpl = new ClientImpl(operator, id, familyId);

        clientImpl.setFirstName(rs.getString("first_name"));
        clientImpl.setIdentificationNumberAndGender(
                rs.getString("identification_number"),
                rs.getString("gender"));
        clientImpl.setIncome(rs.getLong("income"));
        clientImpl.setBankAccount(rs.getString("bank_account_number"),
                rs.getString("bank_code"));
        clientImpl.setWorkingCondition(rs.getString("working_condition"));
        clientImpl.setNote(rs.getString("note"));
        clientImpl.setDoctor(rs.getString("doctor"));
        clientImpl.setIdentityCardNumber(rs.getString("identity_card_number"));
        clientImpl.setIdentityCardValidity(Common.unshiftTime(rs.getLong("identity_card_valid_date")));
        clientImpl.setIdentityCardIssuer(rs.getString("identity_card_issuer"));
        clientImpl.setBirthPlace(rs.getString("identity_card_birth_place"));
        clientImpl.setProfession(rs.getString("profession"));
        
        clientImpl.setSportLevel(rs.getString("sport_level"));
        clientImpl.setSportType(rs.getString("sport_type"));

        // add client telephones
        Statement telephoneStatement = connection.createStatement();
        telephoneStatement.setQueryTimeout(10);

        ResultSet rsTelephone = telephoneStatement.
                executeQuery("select * from telephone where "
                + "client_id='" + clientImpl.getId() + "'");

        while (rsTelephone.next()) {
            clientImpl.addTelephoneNumber(rsTelephone.getString("number"));
        }

        // add client emails
        Statement emailStatement = connection.createStatement();
        emailStatement.setQueryTimeout(10);

        ResultSet rsEmail = emailStatement.
                executeQuery("select * from email where "
                + "client_id='" + clientImpl.getId() + "'");

        while (rsEmail.next()) {
            clientImpl.addEmail(rsEmail.getString("address"));
        }
        
        return clientImpl;
    }
    
    @Override
    protected List<ClientImpl> getWorker(Connection connection, String searchString) 
            throws SQLException, IOException {
        List<ClientImpl> resultList = new ArrayList<>();

        String queryString;
        
        if(searchString.contains("select")) {
            queryString = searchString;
        } else {
            String whereString = setWhereString(searchString);
            queryString = "select * from " + getTableName() + whereString;
        }
        
        PreparedStatement statement = connection.prepareStatement(queryString);
        statement.setQueryTimeout(10);
        
        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            resultList.add(fetchClientFromResultSet(connection, rs));
        }
        
        return resultList;
    }
    
    @Override
    public List<ClientImpl> get() {
        return (List<ClientImpl>) super.get();
    }
    
    @Override
    public List<ClientImpl> get(String searchString) {
        return (List<ClientImpl>) super.get(searchString);
    }
    
    @Override
    public ClientImpl getById(Integer id) {
        List<ClientImpl> clients = get("id=" + id);
        
        if(clients.size() > 0) {
            return clients.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Get clients from DB based on defined conditions.
     * @param namePart part of client name that must record contain, not null!
     * @param incomeComparator income comparator value - -1 if searching for 
     * smaller income value than defined, 0 if searching for exact income value 
     * as defined, 1 if searching for greater income value than defined or null
     * if dont care
     * @param income income value that must client satisfy based on comparator
     * before
     * @param ageComparator age comparator value - -1 if searching for 
     * smaller age value than defined, 0 if searching for exact age value 
     * as defined, 1 if searching for greater income age than defined or null
     * if dont care
     * @param age client age value that must client satisfy based on comparator
     * before
     * @param condition working condition that must client satisfy or null if
     * dont care
     * @param gender client gender or null if dont care
     * @return list of clients that satisfy defined conditions
     */
    public List<ClientImpl> getClients(String namePart, Integer incomeComparator, 
            Integer income, Integer ageComparator, Integer age, WorkingCondition
                    condition, Gender gender) {
        String query = "select client.first_name || ' ' || family.name 'full_name', client.* from client"
                + " INNER JOIN family ON client.family_id=family.id where ";

        query += "full_name like '%" + namePart + "%' ";

        if (incomeComparator != null) {
            Character incomeChar;
            switch (incomeComparator) {
                case -1:
                    incomeChar = '<';
                    break;
                case 0:
                    incomeChar = '=';
                    break;
                case 1:
                    incomeChar = '>';
                    break;
                default:
                    incomeChar = null;
            }
            query += "and income " + incomeChar + income + " ";
        }

        if (ageComparator != null) {
            Character ageChar;
            switch (ageComparator) {
                case -1:
                    ageChar = '<';
                    break;
                case 0:
                    ageChar = '=';
                    break;
                case 1:
                    ageChar = '>';
                    break;
                default:
                    ageChar = null;
            }
            query += "and age " + ageChar + age + " ";
        }

        if (condition != null) {
            query += "and working_condition='"
                    + condition.toString() + "' ";
        }

        if (gender != null) {
            query += "and gender='" + gender.toString() + "' ";
        }
        
        return get(query);
    }
    
    @Override
    public List<ContractImpl> getContracts() {
        return new ContractDBImpl(operator).get("client_id=" + client.getId());
    }
    
    @Override
    public List<ReminderImpl> getCurrentReminders() {
        List<ReminderImpl> reminders = new ArrayList<>();
        
        // if creating birthday reminder is set
        boolean automaticBirthdayReminderCreation = Common.getPreferences().getBoolean(
                Common.CREATE_BIRTHDAY_REMINDER_PREFERENCE, false);
        
        if(automaticBirthdayReminderCreation) {
        
            int unitCount = Common.getPreferences().getInt(
                    Common.BIRTHDAY_UNIT_COUNT_PREFERENCE,
                    1);

            int unit = Common.getPreferences().getInt(
                    Common.BIRTHDAY_UNIT_PREFERENCE,
                    Calendar.DATE);

            Date reminderStartDate = Common.computeShiftedDate(Common.getDateWithoutYear(Common.getTodayDateAtEnd()), 
                    unitCount, unit, 1);
            
            Date reminderEndDate = Common.computeShiftedDate(Common.getDateWithoutYear(Common.getTodayDateAtEnd()), 
                    Common.MAX_DAYS_TO_KEEP_REMINDER, Calendar.DATE, -1);
            
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            
            for(ClientImpl clientIt : get("birth < " + Common.getShiftedTime(reminderStartDate) + 
                    " and birth > " + Common.getShiftedTime(reminderEndDate))) {
                Calendar currentYearDate = Calendar.getInstance();
                currentYearDate.setTime(clientIt.getDateOfBirth());
                currentYearDate.set(Calendar.YEAR, currentYear);
                
                ReminderImpl reminder = new ReminderImpl(operator, null);
                reminder.setDate(Common.computeShiftedDate(currentYearDate.getTime(), unitCount, unit, -1));
                reminder.setTargetDate(clientIt.getDateOfBirth());
                reminder.setRemindedObjectId(clientIt.getId());
                reminder.setType(ReminderType.BIRTHDAY);
                reminders.add(reminder);
            }
        
        }
        
        return reminders;
    }
    
    @Override
    protected List<ReminderImpl> getReminders() {
        return new ReminderDBImpl(operator).get("object_id=" + client.getId() + " and " + "type in ('"
                + ReminderType.CLIENT.toString() + "','"
                + ReminderType.MEETING.toString() + "')");
    }
    
    @Override
    public FamilyImpl getFamily() {
        return new FamilyDBImpl(operator).getById(client.getFamilyId());
    }
    
    /**
     * Auxiliary method for adding client emails and telephones to database.
     */
    private void addEmailsAndTelephones(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        // add client telephones to DB
        List<String> telephones = client.getTelephones();

        if (telephones.size() > 0) {
            for (String telephone : telephones) {
                statement.executeUpdate("insert into telephone "
                        + "(client_id,number) "
                        + "values ("
                        + client.getId() + ","
                        + "'" + telephone + "')");
            }
        }

        // add client emails to DB
        List<String> emails = client.getEmails();

        if (emails.size() > 0) {
            for (String email : emails) {
                statement.executeUpdate("insert into email "
                        + "(client_id,address) "
                        + "values ("
                        + client.getId() + ","
                        + "'" + email + "')");
            }
        }
    }
    
    /**
     * Auxiliary method for deleting client emails and telephones from database.
     */
    private void deleteEmailsAndTelephones(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        // delete client telephones from DB
        statement.executeUpdate("delete from telephone"
                + " where client_id=" + client.getId());

        // delete client emails from DB
        statement.executeUpdate("delete from email"
                + " where client_id=" + client.getId());
    }
}
