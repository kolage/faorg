package Database.Impl;

import Database.ContractDB;
import Entity.Contract.ContractType;
import Entity.Impl.ClientImpl;
import Entity.Impl.ContractImpl;
import Entity.Impl.ReminderImpl;
import Entity.Reminder.ReminderType;
import Faorg.Common;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Implementation of ContractDB interface.
 * 
 * @author kolage
 */
public final class ContractDBImpl extends EntityDBImpl implements ContractDB {

    /**
     * Reference to contract object.
     */
    private ContractImpl contract;
    
    /**
     * Creates new instance of ContractDB interface implementation.
     * @param operator database operator
     */
    public ContractDBImpl(DatabaseOperator operator) {
        super(operator);
    }
    
    /**
     * Creates new instance of ContractDB interface implementation.
     * @param contract related contract
     * @param operator database operator
     */
    public ContractDBImpl(ContractImpl contract, DatabaseOperator operator) {
        super(operator);
        this.contract = contract;
    }
    
    @Override
    public String getTableName() {
        return "contract";
    }
    
    @Override
    public void insertWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("insert into " + getTableName()
                + " (id, client_id, type, contract_id_value, start_date, start_date_wo_year,"
                + "end_date, frequency, payment, institution, "
                + "target_amount, note, login, mark, timestamp) values ("
                + contract.getId() + ","
                + contract.getClientId() + ","
                + "'" + contract.getType().toString() + "',"
                + "'" + contract.getIdentificationValue() + "',"
                + Common.getShiftedTime(contract.getStartDate()) + ","
                + Common.getShiftedTime(Common.getDateWithoutYear(contract.getStartDate())) + ","
                + Common.getShiftedTime(contract.getEndDate()) + ","
                + "'" + contract.getFrequency().toString() + "',"
                + contract.getPayment() + ","
                + "'" + contract.getInstitutionName() + "',"
                + contract.getTargetAmount() + ","
                + "'" + contract.getNote() + "',"
                + "'" + operator.getLogin() + "',"
                + operator.getMark() + ","
                + operator.getCurrentTimestamp() + ")");
    }
    
    @Override
    public void updateWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("update " + getTableName() + " set "
                + "mark=" + operator.getMark() + ","
                + "timestamp=" + operator.getCurrentTimestamp() + ","
                + "client_id=" + contract.getClientId() + ","
                + "type='" + contract.getType().toString() + "',"
                + "contract_id_value='"
                + contract.getIdentificationValue() + "',"
                + "start_date=" + Common.getShiftedTime(contract.getStartDate()) + ","
                + "start_date_wo_year=" + Common.getShiftedTime(Common.getDateWithoutYear(contract.getStartDate())) + ","
                + "end_date=" + Common.getShiftedTime(contract.getEndDate()) + ","
                + "frequency='"
                + contract.getFrequency().toString() + "',"
                + "payment=" + contract.getPayment() + ","
                + "target_amount=" + contract.getTargetAmount() + ","
                + "institution='" + contract.getInstitutionName() + "',"
                + "note='" + contract.getNote() + "' "
                + "where id=" + contract.getId());
    }

    @Override
    public void deleteWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);
        
        for(ReminderImpl reminder : getReminders()) {
            reminder.getDBOperator().deleteWorker(connection);
        }

        statement.executeUpdate("delete from " + getTableName() + " where "
                + "id=" + contract.getId());
    }
    
    /**
     * Auxiliary method for fetching contract record from result set.
     * @param rs result set
     * @return contract
     */
    private ContractImpl fetchContractFromResultSet(ResultSet rs) throws SQLException {
        ContractImpl contractImpl;
        
        Integer id = rs.getInt("id");
        Integer clientId = rs.getInt("client_id");
        contractImpl =
                new ContractImpl(operator, id, clientId);
        contractImpl.setType(rs.getString("type"));
        contractImpl.setIdentificationValue(
                rs.getString("contract_id_value"));
        contractImpl.setStartDate(Common.unshiftTime(
                rs.getLong("start_date")));
        contractImpl.setEndDate(Common.unshiftTime(
                rs.getLong("end_date")));
        contractImpl.setFrequency(rs.getString("frequency"));
        contractImpl.setPayment(rs.getInt("payment"));
        contractImpl.setTargetAmount(rs.getLong("target_amount"));
        contractImpl.setInstitutionName(rs.getString("institution"));
        contractImpl.setNote(rs.getString("note"));
        
        return contractImpl;
    }
    
    @Override
    protected List<ContractImpl> getWorker(Connection connection, String searchString)
            throws SQLException, IOException {
        List<ContractImpl> resultList = new ArrayList<>();

        String query = "select * from " + getTableName() + setWhereString(searchString);
        
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setQueryTimeout(10);
        
        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            resultList.add(fetchContractFromResultSet(rs));
        }
        
        return resultList;
    }
    
    @Override
    public List<ContractImpl> get() {
        return (List<ContractImpl>) super.get();
    }
    
    @Override
    public List<ContractImpl> get(String searchString) {
        return (List<ContractImpl>) super.get(searchString);
    }
    
    @Override
    public ContractImpl getById(Integer id) {
        List<ContractImpl> contracts = get("id=" + id);
        
        if(contracts.size() > 0) {
            return contracts.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Get contracts from DB based on defined conditions.
     * @param institutionPart part of contract institution name that must record 
     * contain, not null!
     * @param type contract type that must contract satisfy or null if dont 
     * care
     * @param startDateBegin lower bound of contract start date or null if dont
     * care
     * @param startDateEnd upper bound of contract start date or null if dont
     * care
     * @param endDateBegin lower bound of contract end date or null if dont
     * care
     * @param endDateEnd upper bound of contract end date or null if dont
     * care
     * @return list of contracts that satisfy defined conditions
     */
    public List<ContractImpl> getContracts(String institutionPart, 
            ContractType type, Date startDateBegin, Date startDateEnd, 
            Date endDateBegin, Date endDateEnd) {
        String query = "";

        query += "institution like '%" + institutionPart + "%' ";

        if (type != null) {
            query += "and type='" + type.toString() + "' ";
        }

        if (startDateBegin != null) {
            query += "and start_date >='" + Common.getShiftedTime(
                    startDateBegin) + "' ";
        }

        if (startDateEnd != null) {
            query += "and start_date <='" + Common.getShiftedTime(
                    startDateEnd) + "' ";
        }

        if (endDateBegin != null) {
            query += "and end_date >='" + Common.getShiftedTime(
                    endDateBegin) + "' ";
        }

        if (endDateEnd != null) {
            query += "and end_date <='" + Common.getShiftedTime(
                    endDateEnd) + "' ";
        }
        
        return get(query);
    }
    
    @Override
    public List<ReminderImpl> getReminders() {
        return new ReminderDBImpl(operator).get("object_id=" + contract.getId() + " and " + "type='" +
                ReminderType.CONTRACT.toString() + "'");
    }
    
    @Override
    public List<ReminderImpl> getCurrentReminders() {
        List<ReminderImpl> reminders = new ArrayList<>();
        
        // if automatic creating of contract reminder is set
        boolean automaticContractReminderCreation =
                Common.getPreferences().getBoolean(
                Common.CREATE_CONTRACT_REMINDER_PREFERENCE, false);
        
        if(automaticContractReminderCreation) {
        
            int unitCount = Common.getPreferences().getInt(
                    Common.CONTRACT_UNIT_COUNT_PREFERENCE,
                    1);

            int unit = Common.getPreferences().getInt(
                    Common.CONTRACT_UNIT_PREFERENCE,
                    Calendar.DATE);

            Date reminderStartDate = Common.computeShiftedDate(Common.getTodayDateAtEnd(), unitCount, unit, 1);
            
            Date reminderEndDate = Common.computeShiftedDate(Common.getTodayDateAtEnd(), 
                    Common.MAX_DAYS_TO_KEEP_REMINDER, Calendar.DATE, -1);

            for(ContractImpl contractIt : get("end_date < " + Common.getShiftedTime(reminderStartDate) + 
                    " and end_date > " + Common.getShiftedTime(reminderEndDate))) {
                ReminderImpl reminder = new ReminderImpl(operator, null);
                reminder.setDate(Common.computeShiftedDate(contractIt.getEndDate(), unitCount, unit, -1));
                reminder.setTargetDate(contractIt.getEndDate());
                reminder.setRemindedObjectId(contractIt.getId());
                reminder.setType(ReminderType.CONTRACT_END);
                reminders.add(reminder);
            }
        
        }
        
         // if automatic creating of contract anniversary reminder is set
        boolean automaticContractAnniversaryReminderCreation = Common.getPreferences().getBoolean(
                Common.CREATE_CONTRACT_ANNIVERSARY_REMINDER_PREFERENCE, false);
        
        if(automaticContractAnniversaryReminderCreation) {
        
            int unitCount = Common.getPreferences().getInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_COUNT_PREFERENCE,
                    1);

            int unit = Common.getPreferences().getInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_PREFERENCE,
                    Calendar.DATE);

            Date reminderStartDate = Common.computeShiftedDate(Common.getDateWithoutYear(Common.getTodayDateAtEnd()), 
                    unitCount, unit, 1);
            
            Date reminderEndDate = Common.computeShiftedDate(Common.getDateWithoutYear(Common.getTodayDateAtEnd()), 
                    Common.MAX_DAYS_TO_KEEP_REMINDER, Calendar.DATE, -1);
            
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            
            for(ContractImpl contractIt : get("start_date_wo_year < " + Common.getShiftedTime(reminderStartDate) + 
                    " and start_date_wo_year > " + Common.getShiftedTime(reminderEndDate))) {
                Calendar currentYearDate = Calendar.getInstance();
                currentYearDate.setTime(contractIt.getStartDate());
                currentYearDate.set(Calendar.YEAR, currentYear);
                
                ReminderImpl reminder = new ReminderImpl(operator, null);
                reminder.setDate(Common.computeShiftedDate(currentYearDate.getTime(), unitCount, unit, -1));
                reminder.setTargetDate(contractIt.getStartDate());
                reminder.setRemindedObjectId(contractIt.getId());
                reminder.setType(ReminderType.CONTRACT_ANNIVERSARY);
                reminders.add(reminder);
            }
        
        }
        
        return reminders;
    }
    
    @Override
    public ClientImpl getClient() {
        return new ClientDBImpl(operator).getById(contract.getClientId());
    }
}
