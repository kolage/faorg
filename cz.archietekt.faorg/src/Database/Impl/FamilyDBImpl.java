package Database.Impl;

import Database.FamilyDB;
import Entity.Family.FamilyLevel;
import Entity.Family.FamilyProperty;
import Entity.Family.HouseType;
import Entity.Impl.ClientImpl;
import Entity.Impl.FamilyImpl;
import Entity.Impl.ReminderImpl;
import Entity.Reminder.ReminderType;
import Faorg.Address;
import Faorg.Common;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Implementation of FamilyDB interface.
 * 
 * @author kolage
 */
public final class FamilyDBImpl extends EntityDBImpl implements FamilyDB {

    /**
     * Reference to family object.
     */
    private FamilyImpl family;
    
    /**
     * Creates new instance of FamilyDB interface implementation.
     * @param operator database operator
     */
    public FamilyDBImpl(DatabaseOperator operator) {
        super(operator);
    }
    
    /**
     * Creates new instance of FamilyDB interface implementation.
     * @param family related family object
     * @param operator database operator
     */
    public FamilyDBImpl(FamilyImpl family, DatabaseOperator operator) {
        super(operator);
        this.family = family;
    }
    
    @Override
    public String getTableName() {
        return "family";
    }
    
    @Override
    public void insertWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("insert into " + getTableName() + " (id, "
                + "name, street, city, zip_code, "
                + "postal_street, postal_city, postal_zip_code, "
                + "family_level, family_property, house_type, last_visit, envelope_title, "
                + "letter_title, login, mark, timestamp) values ("
                + family.getId() + ","
                + "'" + family.getName() + "',"
                + "'" + family.getAddress().getStreet() + "',"
                + "'" + family.getAddress().getCity() + "',"
                + "'" + family.getAddress().getZipCode() + "',"
                + "'" + family.getPostalAddress().getStreet() + "',"
                + "'" + family.getPostalAddress().getCity() + "',"
                + "'" + family.getPostalAddress().getZipCode() + "',"
                + "'" + family.getFamilyLevel().toString() + "',"
                + "'" + family.getFamilyProperty().toString() + "',"
                + "'" + family.getHouseType().toString() + "',"
                + Common.getShiftedTime(family.getLastVisit()) + ","
                + "'" + family.getEnvelopeTitle() + "',"
                + "'" + family.getLetterTitle() + "',"
                + "'" + operator.getLogin() + "',"
                + operator.getMark() + ","
                + operator.getCurrentTimestamp() + ")");
        
    }

    @Override
    public void updateWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("update " + getTableName() + " set "
                + "mark=" + operator.getMark() + ","
                + "timestamp=" + operator.getCurrentTimestamp() + ","
                + "name='" + family.getName() + "',"
                + "street='" + family.getAddress().getStreet() + "',"
                + "city='" + family.getAddress().getCity() + "',"
                + "zip_code='" + family.getAddress().getZipCode() + "',"
                + "postal_street='" + family.getPostalAddress().getStreet() + "',"
                + "postal_city='" + family.getPostalAddress().getCity() + "',"
                + "postal_zip_code='" + family.getPostalAddress().getZipCode() + "',"
                + "family_level='"
                + family.getFamilyLevel().toString() + "',"
                + "family_property='"
                + family.getFamilyProperty().toString() + "',"
                + "house_type='" + family.getHouseType().toString() + "',"
                + "last_visit=" + 
                Common.getShiftedTime(family.getLastVisit()) + ","
                + "envelope_title='" + family.getEnvelopeTitle() + "',"
                + "letter_title='" + family.getLetterTitle() + "' "
                + "where id=" + family.getId());
    }

    @Override
    public void deleteWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        for(ClientImpl client : getClients()) {
            client.getDBOperator().deleteWorker(connection);
        }
        
        for(ReminderImpl reminder : getReminders()) {
            reminder.getDBOperator().deleteWorker(connection);
        }

        statement.executeUpdate("delete from " + getTableName() + " where id=" + family.getId());
    }
    
    /**
     * Auxiliary method for fetching family record from result set.
     * @param rs result set
     * @return family
     */
    private FamilyImpl fetchFamilyFromResultSet(ResultSet rs) throws SQLException {
        FamilyImpl familyImpl;
        
        Integer id = rs.getInt("id");

        familyImpl = new FamilyImpl(operator, id);
        familyImpl.setName(rs.getString("name"));
        familyImpl.setAddress(new Address(
                    rs.getString("city"),
                    rs.getString("street"),
                    rs.getString("zip_code")));
        familyImpl.setPostalAddress(new Address(
                    rs.getString("postal_city"),
                    rs.getString("postal_street"),
                    rs.getString("postal_zip_code")));
        familyImpl.setFamilyLevel(rs.getString("family_level"));
        familyImpl.setFamilyProperty(rs.getString("family_property"));
        familyImpl.setHouseType(rs.getString("house_type"));
        familyImpl.setLastVisit(Common.unshiftTime(rs.getLong("last_visit")));
        familyImpl.setEnvelopeTitle(rs.getString("envelope_title"));
        familyImpl.setLetterTitle(rs.getString("letter_title"));
        
        return familyImpl;
    }
    
    @Override
    protected List<FamilyImpl> getWorker(Connection connection, String searchString) 
            throws SQLException, IOException {
        List<FamilyImpl> resultList = new ArrayList<>();

        String query = "select * from " + getTableName() + setWhereString(searchString);
        
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setQueryTimeout(10);
        
        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            resultList.add(fetchFamilyFromResultSet(rs));
        }
        
        return resultList;
    }
    
    @Override
    public List<FamilyImpl> get() {
        return (List<FamilyImpl>) super.get();
    }
    
    @Override
    public List<FamilyImpl> get(String searchString) {
        return (List<FamilyImpl>) super.get(searchString);
    }
    
    @Override
    public FamilyImpl getById(Integer id) {
        List<FamilyImpl> families = get("id=" + id);
        
        if(families.size() > 0) {
            return families.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Get families from DB based on defined conditions.
     * @param namePart part of family name that must record contain, not null!
     * @param cityPart part of city name that must record contain, or null
     * if dont care
     * @param level minimal family level that must family satisfy, or null
     * if dont care
     * @param property minimal family property level that must family satisfy, 
     * or null if dont care
     * @param houseType minimal family house type that must family satisfy, 
     * or null if dont care
     * @return list of families that satisfy defined conditions
     */
    public List<FamilyImpl> getFamilies(String namePart, String cityPart, 
            FamilyLevel level, FamilyProperty property, HouseType houseType) {
        String searchString = "";

        searchString += "name like '%" + namePart + "%' ";

        if (cityPart != null) {
            searchString += "and city like '%" + cityPart + "%' ";
        }

        if (level != null) {
            List<FamilyLevel> levels = new ArrayList<>();
            switch (level) {
                case PERFECT:
                    levels.add(FamilyLevel.PERFECT);
                    break;
                case GOOD:
                    levels.add(FamilyLevel.GOOD);
                    levels.add(FamilyLevel.PERFECT);
                    break;
                case STANDARD:
                    levels.add(FamilyLevel.STANDARD);
                    levels.add(FamilyLevel.GOOD);
                    levels.add(FamilyLevel.PERFECT);
                    break;
                case BAD:
                    levels.add(FamilyLevel.BAD);
                    levels.add(FamilyLevel.STANDARD);
                    levels.add(FamilyLevel.GOOD);
                    levels.add(FamilyLevel.PERFECT);
                    break;
            }
            searchString += "and family_level in (";
            for (FamilyLevel levelIt : levels) {
                searchString += "'" + levelIt.toString() + "',";
            }
            searchString = searchString.substring(0, searchString.length() - 1);
            searchString += ")";
        }

        if (property != null) {
            List<FamilyProperty> properties = new ArrayList<>();
            switch (property) {
                case MORE_CARS:
                    properties.add(FamilyProperty.MORE_CARS);
                    break;
                case TWO_CARS:
                    properties.add(FamilyProperty.TWO_CARS);
                    properties.add(FamilyProperty.MORE_CARS);
                    break;
                case ONE_CAR:
                    properties.add(FamilyProperty.ONE_CAR);
                    properties.add(FamilyProperty.TWO_CARS);
                    properties.add(FamilyProperty.MORE_CARS);
                    break;
                case NO_CAR:
                    properties.add(FamilyProperty.NO_CAR);
                    properties.add(FamilyProperty.ONE_CAR);
                    properties.add(FamilyProperty.TWO_CARS);
                    properties.add(FamilyProperty.MORE_CARS);
                    break;
            }
            searchString += "and family_property in (";
            for (FamilyProperty propertyIt : properties) {
                searchString += "'" + propertyIt.toString() + "',";
            }
            searchString = searchString.substring(0, searchString.length() - 1);
            searchString += ")";
        }

        if (houseType != null) {
            List<HouseType> types = new ArrayList<>();
            switch (houseType) {
                case HOUSE:
                    types.add(HouseType.HOUSE);
                    break;
                case SEMI_DETACHED:
                    types.add(HouseType.SEMI_DETACHED);
                    types.add(HouseType.HOUSE);
                    break;
                case FLAT_OWN:
                    types.add(HouseType.FLAT_OWN);
                    types.add(HouseType.SEMI_DETACHED);
                    types.add(HouseType.HOUSE);
                    break;
                case FLAT_HIRE:
                    types.add(HouseType.FLAT_HIRE);
                    types.add(HouseType.FLAT_OWN);
                    types.add(HouseType.SEMI_DETACHED);
                    types.add(HouseType.HOUSE);
                    break;
            }
            searchString += "and house_type in (";
            for (HouseType typeIt : types) {
                searchString += "'" + typeIt.toString() + "',";
            }
            searchString = searchString.substring(0, searchString.length() - 1);
            searchString += ")";
        }
        
        return get(searchString);
    }
    
    @Override
    public List<ClientImpl> getClients() {
        return new ClientDBImpl(operator).get("family_id=" + family.getId());
    }
    
    @Override
    public List<ReminderImpl> getReminders() {
        return new ReminderDBImpl(operator).get("object_id=" + family.getId() + " and " + "type='" +
                ReminderType.FAMILY.toString() + "'");
    }
    
    @Override
    public List<ReminderImpl> getCurrentReminders() {
        List<ReminderImpl> reminders = new ArrayList<>();
        
        // if creating last visit reminder is set
        boolean automaticLastVisitReminderCreation =
                Common.getPreferences().getBoolean(
                Common.CREATE_LASTVISIT_REMINDER_PREFERENCE, false);
        
        if(automaticLastVisitReminderCreation) {
        
            int unitCount = Common.getPreferences().getInt(
                    Common.LASTVISIT_UNIT_COUNT_PREFERENCE,
                    1);

            int unit = Common.getPreferences().getInt(
                    Common.LASTVISIT_UNIT_PREFERENCE,
                    Calendar.DATE);

            Date reminderStartDate = Common.computeShiftedDate(Common.getTodayDateAtEnd(), unitCount, unit, -1);
            
            Date reminderEndDate = Common.computeShiftedDate(Common.getTodayDateAtEnd(), 
                    Common.MAX_DAYS_TO_KEEP_REMINDER, Calendar.DATE, -1);
            
            reminderEndDate = Common.computeShiftedDate(reminderEndDate, unitCount, unit, -1);

            for(FamilyImpl familyIt : get("last_visit < " + Common.getShiftedTime(reminderStartDate) + 
                    " and last_visit > " + Common.getShiftedTime(reminderEndDate))) {
                ReminderImpl reminder = new ReminderImpl(operator, null);
                reminder.setDate(Common.computeShiftedDate(familyIt.getLastVisit(), unitCount, unit, 1));
                reminder.setTargetDate(familyIt.getLastVisit());
                reminder.setRemindedObjectId(familyIt.getId());
                reminder.setType(ReminderType.LAST_VISIT);
                reminders.add(reminder);
            }
        
        }
        
        return reminders;
    }
}
