package Database.Impl;

import Database.AdviserDB;
import Entity.Impl.AdviserImpl;
import Entity.Impl.ReminderImpl;
import Faorg.Address;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of AdviserDB interface.
 * 
 * @author kolage
 */
public final class AdviserDBImpl extends EntityDBImpl implements AdviserDB {

    /**
     * Reference to adviser object.
     */
    private AdviserImpl adviser;
    
    /**
     * Creates new instance of AdviserDB interface implementation.
     * @param operator database operator
     */
    public AdviserDBImpl(DatabaseOperator operator) {
        super(operator);
    }
    
    /**
     * Creates new instance of AdviserDB interface implementation.
     * @param adviser related adviser object
     * @param operator database operator
     */
    public AdviserDBImpl(AdviserImpl adviser, DatabaseOperator operator) {
        super(operator);
        this.adviser = adviser;
    }
    
    @Override
    public String getTableName() {
        return "adviser";
    }
    
    protected void insertWorkerProc(Connection connection, boolean keepPassword) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);
        
        String passwordString;
        
        if(keepPassword) {
            passwordString = adviser.getHashedPassword();
        } else {
            passwordString = DatabaseOperator.getHashedString(adviser.getPassword());
        }

        statement.executeUpdate("insert into " + getTableName() + " (login, password, "
                + "firstName, lastName, telephone, email, website, title, "
                + "street, city, zip_code, position, ico, office_street,"
                + "office_city, office_zip_code) values ("
                + "'" + adviser.getLogin() + "',"
                + "'" + passwordString + "',"
                + "'" + adviser.getFirstName() + "',"
                + "'" + adviser.getLastName() + "',"
                + "'" + adviser.getTelephone() + "',"
                + "'" + adviser.getEmail() + "',"
                + "'" + adviser.getWebsite() + "',"
                + "'" + adviser.getTitle() + "',"
                + "'" + adviser.getAddress().getStreet() + "',"
                + "'" + adviser.getAddress().getCity() + "',"
                + "'" + adviser.getAddress().getZipCode() + "',"
                + "'" + adviser.getPosition() + "',"
                + "'" + adviser.getICO() + "',"
                + "'" + adviser.getOfficeAddress().getStreet() + "',"
                + "'" + adviser.getOfficeAddress().getCity() + "',"
                + "'" + adviser.getOfficeAddress().getZipCode() + "')");
    }
    
    @Override
    public void insertWorker(Connection connection) throws SQLException {
        insertWorkerProc(connection, false);
    }
    
    @Override
    public void updateWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        // dont update password if it wasn't set
        String passwordString = adviser.getPassword().isEmpty() ? "" :
                "password='" + DatabaseOperator.getHashedString(adviser.getPassword()) + 
                "',";

        statement.executeUpdate("update " + getTableName() + " set "
                + passwordString
                + "firstName='" + adviser.getFirstName() + "',"
                + "lastName='" + adviser.getLastName() + "',"
                + "telephone='" + adviser.getTelephone() + "',"
                + "email='" + adviser.getEmail() + "',"
                + "website='" + adviser.getWebsite() + "',"
                + "title='" + adviser.getTitle() + "',"
                + "street='" + adviser.getAddress().getStreet() + "',"
                + "city='" + adviser.getAddress().getCity() + "',"
                + "zip_code='" + adviser.getAddress().getZipCode() + "',"
                + "position='" + adviser.getPosition() + "',"
                + "ico='" + adviser.getICO() + "',"
                + "office_street='" + adviser.getOfficeAddress().getStreet() + "',"
                + "office_city='" + adviser.getOfficeAddress().getCity() + "',"
                + "office_zip_code='" + adviser.getOfficeAddress().getZipCode() + "' "
                + "where login='" + adviser.getLogin() + "'");
    }
    
    @Override
    public void deleteWorker(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);

        statement.executeUpdate("delete from " + getTableName() + " where login='" + adviser.getLogin() + "'");
    }
    
    /**
     * Auxiliary method for fetching adviser record from result set.
     * @param rs result set
     * @return adviser
     */
    private AdviserImpl fetchAdviserFromResultSet(ResultSet rs) throws SQLException {
        AdviserImpl adviserImpl;
        
        String login = rs.getString("login");

        adviserImpl = new AdviserImpl(operator,login);
        adviserImpl.setHashedPassword(rs.getString("password"));
        adviserImpl.setFirstName(rs.getString("firstName"));
        adviserImpl.setLastName(rs.getString("lastName"));
        adviserImpl.setTitle(rs.getString("title"));
        adviserImpl.setAddress(new Address(
                rs.getString("city"),
                rs.getString("street"),
                rs.getString("zip_code")));
        adviserImpl.setTelephone(rs.getString("telephone"));
        adviserImpl.setEmail(rs.getString("email"));
        adviserImpl.setWebsite(rs.getString("website"));
        adviserImpl.setPosition(rs.getString("position"));
        adviserImpl.setICO(rs.getString("ico"));
        adviserImpl.setOfficeAddress(new Address(
                rs.getString("office_city"),
                rs.getString("office_street"),
                rs.getString("office_zip_code")));
        
        return adviserImpl;
    }
    
    
    @Override
    protected List<AdviserImpl> getWorker(Connection connection, String searchString) 
            throws SQLException, IOException {
        List<AdviserImpl> resultList = new ArrayList<>();

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(10);
        
        String whereString = searchString.isEmpty() ? "" : " where " + searchString;
        
        ResultSet rs = statement.executeQuery("select * from " + getTableName() + whereString);

        while (rs.next()) {
            resultList.add(fetchAdviserFromResultSet(rs));
        }

        return resultList;
    }
    
    @Override
    public List<AdviserImpl> get() {
        return (List<AdviserImpl>) super.get();
    }
    
    @Override
    public List<AdviserImpl> get(String searchString) {
        return (List<AdviserImpl>) super.get(searchString);
    }
    
    @Override
    public AdviserImpl getById(Integer id) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    protected List<ReminderImpl> getCurrentReminders() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    protected List<ReminderImpl> getReminders() {
        throw new UnsupportedOperationException("Not supported.");
    }
}
