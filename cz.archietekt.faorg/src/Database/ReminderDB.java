package Database;

import Entity.Entity;

/**
 * Interface describing database operations related to specific reminder object.
 * 
 * @author kolage
 */
public interface ReminderDB extends EntityDB {
    
    /**
     * Fetch entity object related to this reminder.
     * @return parent entity object
     */
    public Entity getParentObject();
}
