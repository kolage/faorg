package Database;

import Entity.Contract;
import Entity.Family;
import java.util.List;

/**
 * Interface describing database operations related to specific client.
 * 
 * @author kolage
 */
public interface ClientDB extends EntityDB {
    
    /**
     * Fetch contract records related to this client from DB.
     * @return list of contracts
     */
    public List<? extends Contract> getContracts();
    
    /**
     * Fetch parent family record from DB.
     * @return parent family
     */
    public Family getFamily();
}
