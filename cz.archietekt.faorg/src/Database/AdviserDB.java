package Database;

/**
 * Interface describing database operations related to specific adviser.
 * 
 * @author kolage
 */
public interface AdviserDB extends EntityDB {

}
