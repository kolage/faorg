package Graphical;

import Database.Impl.ClientDBImpl;
import Database.Impl.ContractDBImpl;
import Database.Impl.DatabaseOperator;
import Database.Impl.FamilyDBImpl;
import Entity.Client;
import Entity.Contract;
import Entity.Entity;
import Entity.Family;
import Entity.Impl.ReminderImpl;
import Entity.Reminder.ReminderType;
import Faorg.Common;
import Faorg.StringPair;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;

/**
 * Class representing window for creating new reminder.
 * 
 * @author kolage
 */
public class NewReminderGUI extends javax.swing.JFrame {

    /**
     * Position of item "direction before target date" in combo box.
     */
    private static final int DIRECTION_COMBO_BEFORE_POSITION = 0;
    
     /**
     * Using singleton GUI.
     */
    private static NewReminderGUI myInstance;
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;

    /**
     * Reminder object.
     */
    private final ReminderImpl reminder;
    
    /**
     * List of objects relevant to reminder type fetched from DB.
     */
    private ArrayList<Entity> relevantObjects;
    
    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        NewReminderGUI.myInstance = null;
    }
    
    /**
     * Get instance of NewReminderGUI. If it not exists, return null.
     * @return instance of NewReminderGUI
     */
    public static NewReminderGUI getInstance() {
        return myInstance;
    }
    
    /**
     * Method for creating singleton NewReminderGUI object.
     * 
     * @param dbOperator reference to database operator
     * @param reminderType reminder type
     * @param objectId id of parent object
     * 
     * @return instance of reminder GUI
     */
    public static NewReminderGUI getInstance(DatabaseOperator dbOperator, ReminderType reminderType, Integer objectId) {
        if (myInstance == null)
            myInstance = new NewReminderGUI(dbOperator,reminderType,objectId);

        return myInstance;
    }
    
    /**
     * Creates new form NewReminderGUI.
     */
    private NewReminderGUI(DatabaseOperator dbOperator, 
            ReminderType reminderType, Integer objectId) {
        this.dbOperator = dbOperator;
        
        this.reminder = new ReminderImpl(dbOperator,dbOperator.generateNewId("reminder"));
        this.reminder.setType(reminderType);
        this.reminder.setRemindedObjectId(objectId);
   
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Fetches objects from DB into hash map corresponding to reminder type.
     */
    // TODO: inheritance instead of switch !
    private void fetchRelevantObjects() {
        objectCombo.removeAllItems();
        
        relevantObjects = new ArrayList<>();
        
        switch(reminder.getType()) {
            case FAMILY:
                for(Family family : new FamilyDBImpl(dbOperator).get()) {
                    relevantObjects.add(family);
                }
                break;
            case CONTRACT:
                for (Contract contract : new ContractDBImpl(dbOperator).get()) {
                    relevantObjects.add(contract);
                }
                break;
            case MEETING:
            case CLIENT:
                for (Client client : new ClientDBImpl(dbOperator).get()) {
                    relevantObjects.add(client);
                }
                break;
        }
        
        for(Entity entity : relevantObjects) {
            String text = Common.bundle.getString("REMINDER-"
                    + entity.toString()) + " " + entity.getId();
            
            if(entity instanceof Family) {
                text = ((Family) entity).getName();
            } else if(entity instanceof Client) {
                Client client = (Client) entity;
                text = client.getFirstName() + " " 
                        + client.getDBOperator().getFamily().
                        getName();
            } else if(entity instanceof Contract) {
                Contract contract = (Contract) entity;
                Client client = contract.getDBOperator().getClient();
                Family family = client.getDBOperator().getFamily();
                text = family.getName() + "; ";
                if(!contract.getIdentificationValue().isEmpty()) {
                    text += contract.getIdentificationValue();
                } else {
                    text += contract.getId();
                }
            }
            
            objectCombo.addItem(new StringPair(entity.getId().toString(),text));
        }
        
        if(objectCombo.getItemCount() == 0 && reminder.getType() != ReminderType.OTHER) {
            applyButton.setEnabled(false);
        } else {
            applyButton.setEnabled(true);
        }
        
        for(int i = 0; i < objectCombo.getItemCount(); i++) {
            StringPair pair = (StringPair) objectCombo.getItemAt(i);
            if(reminder.getRemindedObjectId() != null && 
                    Integer.parseInt(pair.getKey()) == reminder.getRemindedObjectId()) {
                objectCombo.setSelectedIndex(i);
                break;
            }
        }
    }
    
    /**
     * Method for initializing objects before graphical elements are loaded.
     */
    private void preInit() {
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        reminderTypeCombo.addItem(new StringPair(ReminderType.CLIENT.toString(),
                    Common.bundle.getString("REMINDER-" + ReminderType.CLIENT.toString())));
        
        reminderTypeCombo.addItem(new StringPair(ReminderType.FAMILY.toString(),
                    Common.bundle.getString("REMINDER-" + ReminderType.FAMILY.toString())));
        
        reminderTypeCombo.addItem(new StringPair(ReminderType.CONTRACT.toString(),
                    Common.bundle.getString("REMINDER-" + ReminderType.CONTRACT.toString())));
        
        reminderTypeCombo.addItem(new StringPair(ReminderType.MEETING.toString(),
                    Common.bundle.getString("REMINDER-" + ReminderType.MEETING.toString())));
        
        reminderTypeCombo.addItem(new StringPair(ReminderType.OTHER.toString(),
                    Common.bundle.getString("REMINDER-" + ReminderType.OTHER.toString())));
        
        dateUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.DATE), 
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-DAY")));
        dateUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.WEEK_OF_MONTH), 
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-WEEK")));
        dateUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-MONTH")));
        
        dateDirectionCombo.addItem(new StringPair(
                "-1", 
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-BEFORE")));
        dateDirectionCombo.addItem(new StringPair(
                "1",
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-AFTER")));
        
        dateUnitSpinner.setModel(new SpinnerNumberModel(1, 0, 30, 1));
        
        dateChooser.setLocale(new Locale("cs"));
        dateChooser.setDateFormatString(Common.getDateFormatString());
        
        targetDateChooser.setLocale(new Locale("cs"));
        targetDateChooser.setDateFormatString(Common.getDateFormatString());
        
        for(int i = 0; i < reminderTypeCombo.getItemCount(); i++) {
            if(((StringPair) reminderTypeCombo.getItemAt(i)).getKey().equals(reminder.getType().toString())) {
                reminderTypeCombo.setSelectedIndex(i);
                break;
            }
        }
        
        fetchRelevantObjects();
        
        objectCombo.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                computeTargetDateOfSelectedObject();
            }
        });
        
        reminderTypeCombo.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                reminder.setType(((StringPair) reminderTypeCombo.getSelectedItem()).getKey());
                fetchRelevantObjects();
            }
        });
        
        computeTargetDateOfSelectedObject();
    }
    
    /**
     * Method will fetch written and selected reminder attributes and stores it
     * in reminder object.
     */
    private void loadReminderAttributesFromForm() {
        if(objectCombo.getSelectedIndex() >= 0) {
            reminder.setRemindedObjectId(Integer.parseInt(((StringPair) objectCombo.getSelectedItem()).getKey()));
        }
        
        reminder.setText(noteMemo.getText());
        
        reminder.setTargetDate(targetDateChooser.getDate());
        
        if(specificDateRadio.isSelected()) {
            reminder.setDate(dateChooser.getDate());
        } else {
            reminder.setDate(computeRelativeDateFromForm());
        }
    }
    
    private Date computeTargetDateOfSelectedObject() {
        if(objectCombo.getSelectedIndex() >= 0) {
            Calendar targetCalendar = Calendar.getInstance();

            dateDirectionCombo.setSelectedIndex(DIRECTION_COMBO_BEFORE_POSITION);

            if(dateChooser.getDate() != null && dateChooser.getDate().compareTo(targetCalendar.getTime()) > 0) {
                targetCalendar.setTime(dateChooser.getDate());
            }

            // set target date label text to new target date
            targetDateChooser.setDate(targetCalendar.getTime());

            return targetCalendar.getTime();
        } else {
            return new Date();
        }
    }

    /**
     * Computes reminder date from form data.
     * @return reminder date
     */
    private Date computeRelativeDateFromForm() {
        int count = (Integer) dateUnitSpinner.getValue();
        
        String unitString = ((StringPair) dateUnitCombo.getSelectedItem()).getKey();
        
        int unit = Integer.parseInt(unitString);
        
        Integer direction = Integer.parseInt((((StringPair) dateDirectionCombo.getSelectedItem()).getKey()));
        
        return Common.computeShiftedDate(targetDateChooser.getDate(), count, unit, direction);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        reminderDateGroup = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        reminderTypeCombo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        specificDateRadio = new javax.swing.JRadioButton();
        relativeDateRadio = new javax.swing.JRadioButton();
        dateChooser = new com.toedter.calendar.JDateChooser();
        dateUnitSpinner = new javax.swing.JSpinner();
        dateUnitCombo = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        noteMemo = new javax.swing.JTextArea();
        applyButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        objectCombo = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        targetDateChooser = new com.toedter.calendar.JDateChooser();
        dateDirectionCombo = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("NEW_REMINDER_TITLE")); // NOI18N
        setResizable(false);
        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        layout.rowHeights = new int[] {0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0, 12, 0};
        getContentPane().setLayout(layout);

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()+7));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminder.png"))); // NOI18N
        jLabel1.setText(bundle.getString("NEW_REMINDER_CAPTION")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel1, gridBagConstraints);

        jLabel2.setText(bundle.getString("NEW_REMINDER_TYPE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel2, gridBagConstraints);

        reminderTypeCombo.setMaximumSize(new java.awt.Dimension(216, 26));
        reminderTypeCombo.setMinimumSize(new java.awt.Dimension(216, 26));
        reminderTypeCombo.setPreferredSize(new java.awt.Dimension(216, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(reminderTypeCombo, gridBagConstraints);

        jLabel3.setText(bundle.getString("NEW_REMINDER_WHEN")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 11, 0);
        getContentPane().add(jSeparator1, gridBagConstraints);

        reminderDateGroup.add(specificDateRadio);
        specificDateRadio.setFont(specificDateRadio.getFont());
        specificDateRadio.setText(bundle.getString("NEW_REMINDER_SPECIFIC_DATE")); // NOI18N
        specificDateRadio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                specificDateRadioItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(specificDateRadio, gridBagConstraints);

        reminderDateGroup.add(relativeDateRadio);
        relativeDateRadio.setFont(relativeDateRadio.getFont());
        relativeDateRadio.setSelected(true);
        relativeDateRadio.setText(bundle.getString("NEW_REMINDER_RELATIVE_DATE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(relativeDateRadio, gridBagConstraints);

        dateChooser.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(dateChooser, gridBagConstraints);

        dateUnitSpinner.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(dateUnitSpinner, gridBagConstraints);

        dateUnitCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(dateUnitCombo, gridBagConstraints);

        jLabel4.setText(bundle.getString("NEW_REMINDER_NOTE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel4, gridBagConstraints);

        noteMemo.setColumns(20);
        noteMemo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        noteMemo.setRows(5);
        jScrollPane1.setViewportView(noteMemo);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jScrollPane1, gridBagConstraints);

        applyButton.setFont(applyButton.getFont().deriveFont(applyButton.getFont().getStyle() | java.awt.Font.BOLD));
        applyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        applyButton.setText(bundle.getString("NEW_REMINDER_APPLY")); // NOI18N
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(applyButton, gridBagConstraints);

        jLabel5.setText(bundle.getString("NEW_REMINDER_OBJECT")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel5, gridBagConstraints);

        objectCombo.setMaximumSize(new java.awt.Dimension(216, 26));
        objectCombo.setMinimumSize(new java.awt.Dimension(216, 26));
        objectCombo.setPreferredSize(new java.awt.Dimension(216, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(objectCombo, gridBagConstraints);

        jLabel6.setText(bundle.getString("NEW_REMINDER_TARGET-DATE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(targetDateChooser, gridBagConstraints);

        dateDirectionCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(dateDirectionCombo, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void specificDateRadioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_specificDateRadioItemStateChanged
        if(specificDateRadio.isSelected()) {
            dateChooser.setEnabled(true);
            dateUnitCombo.setEnabled(false);
            dateUnitSpinner.setEnabled(false);
        } else {
            dateChooser.setEnabled(false);
            dateUnitCombo.setEnabled(true);
            dateUnitSpinner.setEnabled(true);
        }
    }//GEN-LAST:event_specificDateRadioItemStateChanged

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        loadReminderAttributesFromForm();
        if(reminder.getDate().compareTo(Common.getTodayDate()) < 0) {
            JOptionPane.showMessageDialog(null,
                    Common.bundle.getString("NEW_REMINDER_BADDATE-TEXT"),
                    Common.bundle.getString("NEW_REMINDER_BADDATE-TITLE"),
                    JOptionPane.ERROR_MESSAGE);
        } else {
            reminder.getDBOperator().insert();
            dbOperator.addReminder(reminder);
            this.dispose();
        }
    }//GEN-LAST:event_applyButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JComboBox dateDirectionCombo;
    private javax.swing.JComboBox dateUnitCombo;
    private javax.swing.JSpinner dateUnitSpinner;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea noteMemo;
    private javax.swing.JComboBox objectCombo;
    private javax.swing.JRadioButton relativeDateRadio;
    private javax.swing.ButtonGroup reminderDateGroup;
    private javax.swing.JComboBox reminderTypeCombo;
    private javax.swing.JRadioButton specificDateRadio;
    private com.toedter.calendar.JDateChooser targetDateChooser;
    // End of variables declaration//GEN-END:variables
}
