package Graphical;

import Database.Impl.ClientDBImpl;
import Database.Impl.ContractDBImpl;
import Database.Impl.DatabaseOperator;
import Database.Impl.FamilyDBImpl;
import Entity.Client;
import Entity.Client.Gender;
import Entity.Client.WorkingCondition;
import Entity.Contract;
import Entity.Contract.ContractType;
import Entity.Entity;
import Entity.Family;
import Entity.Family.FamilyLevel;
import Entity.Family.FamilyProperty;
import Entity.Family.HouseType;
import Entity.Impl.ContractImpl;
import Faorg.Common;
import Faorg.StringPair;
import Main.Java2sAutoTextField;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.swing.SpinnerNumberModel;

/**
 * Class representing window for searching up entities in DB.
 * 
 * @author kolage
 */
public class SearchGUI extends javax.swing.JFrame {

    /**
     * Using singleton GUI.
     */
    private static SearchGUI myInstance;
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;

    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        SearchGUI.myInstance = null;
    }
    
    /**
     * Method for creating singleton SearchGUI object.
     * @param dbOperator reference to database operator
     * @return instance of search GUI
     */
    public static SearchGUI getInstance(DatabaseOperator dbOperator) {
        if (myInstance == null)
            myInstance = new SearchGUI(dbOperator);
        return myInstance;
    }
    
    /**
     * Creates new form SearchGUI.
     */
    private SearchGUI(DatabaseOperator dbOperator) {
        this.dbOperator = dbOperator;
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Method for initializing objects before graphical elements are loaded.
     */
    private void preInit() {
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        StringPair dontCareItem = new StringPair("DONT-CARE", 
                Common.bundle.getString("SEARCH_CLIENT-GENDER-DOESNTMATTER"));
        
        // family tab
        
        familyLevelCombo.addItem(dontCareItem);
        for(FamilyLevel level : FamilyLevel.values()) {
            familyLevelCombo.addItem(new StringPair(level.toString(),
                    Common.bundle.getString("FAMILY-" 
                    + level.toString())));
        }
        
        familyPropertyCombo.addItem(dontCareItem);
        for(FamilyProperty property : FamilyProperty.values()) {
            familyPropertyCombo.addItem(new StringPair(property.toString(),
                    Common.bundle.getString("FAMILY-" 
                    + property.toString())));
        }
        
        familyHouseCombo.addItem(dontCareItem);
        for(HouseType houseType : HouseType.values()) {
            familyHouseCombo.addItem(new StringPair(houseType.toString(),
                    Common.bundle.getString("FAMILY-" 
                    + houseType.toString())));
        }
        
        // client tab
        
        clientIncomeCombo.addItem(dontCareItem);
        clientIncomeCombo.addItem(new StringPair("LESS", 
                Common.bundle.getString("SEARCH_CLIENT-LESS")));
        clientIncomeCombo.addItem(new StringPair("MORE", 
                Common.bundle.getString("SEARCH_CLIENT-MORE")));
        clientIncomeCombo.addItem(new StringPair("EXACT", 
                Common.bundle.getString("SEARCH_CLIENT-EXACT")));
        
        clientIncomeSpinner.setModel(new SpinnerNumberModel(
                1000, 0, 500000, 1000));
        
        clientAgeCombo.addItem(dontCareItem);
        clientAgeCombo.addItem(new StringPair("LESS", 
                Common.bundle.getString("SEARCH_CLIENT-LESS")));
        clientAgeCombo.addItem(new StringPair("MORE", 
                Common.bundle.getString("SEARCH_CLIENT-MORE")));
        clientAgeCombo.addItem(new StringPair("EXACT", 
                Common.bundle.getString("SEARCH_CLIENT-EXACT")));
        
        clientAgeSpinner.setModel(new SpinnerNumberModel(
                20, 0, 120, 1));
        
        clientConditionCombo.addItem(dontCareItem);
        for(WorkingCondition condition : WorkingCondition.values()) {
            clientConditionCombo.addItem(new StringPair(condition.toString(),
                    Common.bundle.getString("CLIENT-" 
                    + condition.toString())));
        }
        
        // contract tab
        
        contractTypeCombo.addItem(dontCareItem);
        for(ContractType type : ContractType.values()) {
            contractTypeCombo.addItem(new StringPair(type.toString(),
                    Common.bundle.getString("CONTRACT-" 
                    + type.toString())));
        }
        
        contractStartDateBeginChooser.setLocale(new Locale("cs"));
        contractStartDateBeginChooser.setDateFormatString(
                Common.getDateFormatString());
        
        contractStartDateEndChooser.setLocale(new Locale("cs"));
        contractStartDateEndChooser.setDateFormatString(
                Common.getDateFormatString());
        
        contractEndDateBeginChooser.setLocale(new Locale("cs"));
        contractEndDateBeginChooser.setDateFormatString(
                Common.getDateFormatString());
        
        contractEndDateEndChooser.setLocale(new Locale("cs"));
        contractEndDateEndChooser.setDateFormatString(
                Common.getDateFormatString());
        
    }
    
    /**
     * Method will fetch written and selected search attributes.
     */
    private void loadAttributesFromFormAndSearch(Class searchClass) {
        List<? extends Entity> entities;
        
        // FAMILY
        if(searchClass == Family.class) {
            String namePart = familyNameEdit.getText();
            String cityPart = familyCityEdit.getText();
            
            String levelString = ((StringPair) 
                    familyLevelCombo.getSelectedItem()).getKey();
            
            FamilyLevel level = null;
            
            // skip dont care item
            if(familyLevelCombo.getSelectedIndex() != 0) {
                level = Enum.valueOf(FamilyLevel.class, levelString);
            }
            
            String propertyString = ((StringPair) 
                    familyPropertyCombo.getSelectedItem()).getKey();
            
            FamilyProperty property = null;
            // skip dont care item
            if(familyPropertyCombo.getSelectedIndex() != 0) {
                property = Enum.valueOf(FamilyProperty.class, propertyString);
            }
            
            String houseString = ((StringPair) 
                    familyHouseCombo.getSelectedItem()).getKey();
            
            HouseType houseType = null;
            // skip dont care item
            if(familyHouseCombo.getSelectedIndex() != 0) {
                houseType = Enum.valueOf(HouseType.class, houseString);
            }
            
            entities = new FamilyDBImpl(dbOperator).getFamilies(namePart, cityPart, level, property, houseType);
        // CLIENT
        } else if(searchClass == Client.class) {
            String namePart = clientNameEdit.getText();
            
            Integer incomeComparator;
            
            switch(((StringPair) clientIncomeCombo.getSelectedItem()).
                    getKey()) {
                case "LESS":
                    incomeComparator = -1;
                    break;
                case "EXACT":
                    incomeComparator = 0;
                    break;
                case "MORE":
                    incomeComparator = 1;
                    break;
                default:
                    incomeComparator = null;
                    break;
            }
            
            Integer income = (Integer) clientIncomeSpinner.getValue();
            
            Integer ageComparator;
            
            switch(((StringPair) clientAgeCombo.getSelectedItem()).
                    getKey()) {
                case "LESS":
                    ageComparator = -1;
                    break;
                case "EXACT":
                    ageComparator = 0;
                    break;
                case "MORE":
                    ageComparator = 1;
                    break;
                default:
                    ageComparator = null;
                    break;
            }
            
            Integer age = (Integer) clientAgeSpinner.getValue();
            
            String conditionString = ((StringPair) 
                    clientConditionCombo.getSelectedItem()).getKey();
            
            WorkingCondition condition = null;
            if(clientConditionCombo.getSelectedIndex() != 0) {
                condition = Enum.valueOf(WorkingCondition.class, 
                        conditionString);
            }
            
            Gender gender = null;
            
            if(clientGenderManRadio.isSelected()) {
                gender = Gender.MAN;
            } else if(clientGenderWomanRadio.isSelected()) {
                gender = Gender.WOMAN;
            }
            
            entities = new ClientDBImpl(dbOperator).getClients(
                    namePart, incomeComparator, income, ageComparator, age,
                    condition, gender);
        // CONTRACT
        } else {
            String institutionNamePart = contractInstitutionEdit.getText();
            
            String typeString = ((StringPair) 
                    contractTypeCombo.getSelectedItem()).getKey();
            
            ContractType type = null;
            if(contractTypeCombo.getSelectedIndex() != 0) {
                type = Enum.valueOf(ContractType.class, typeString);
            }
            
            Date startBeginDate = contractStartDateBeginChooser.getDate();
            Date startEndDate = contractStartDateEndChooser.getDate();
            Date endBeginDate = contractEndDateBeginChooser.getDate();
            Date endEndDate = contractEndDateEndChooser.getDate();
            
            entities = new ContractDBImpl(dbOperator).getContracts(
                    institutionNamePart, type, startBeginDate, startEndDate, 
                    endBeginDate, endEndDate);
        }
        
        EntityListGUI.getInstance(dbOperator, searchClass, entities, false).
                setVisible(true);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        genderGroup = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        familyNameEdit = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        familyCityEdit = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        familyLevelCombo = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        familyPropertyCombo = new javax.swing.JComboBox();
        familyHouseCombo = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        familyApplyButton = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        clientNameEdit = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        clientConditionCombo = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        clientApplyButton = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        clientIncomeSpinner = new javax.swing.JSpinner();
        clientIncomeCombo = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        clientAgeCombo = new javax.swing.JComboBox();
        clientAgeSpinner = new javax.swing.JSpinner();
        clientGenderManRadio = new javax.swing.JRadioButton();
        clientGenderWomanRadio = new javax.swing.JRadioButton();
        clientGenderDontCareRadio = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        contractInstitutionEdit = new Java2sAutoTextField(ContractImpl.getInstitutions());
        jLabel9 = new javax.swing.JLabel();
        contractTypeCombo = new javax.swing.JComboBox();
        contractApplyButton = new javax.swing.JButton();
        contractStartDateBeginChooser = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        contractEndDateBeginChooser = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        contractStartDateEndChooser = new com.toedter.calendar.JDateChooser();
        contractEndDateEndChooser = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("SEARCH_TITLE")); // NOI18N
        setResizable(false);

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()+7));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/search.png"))); // NOI18N
        jLabel1.setText(bundle.getString("SEARCH-CAPTION")); // NOI18N

        jLabel2.setText(bundle.getString("SEARCH_FAMILY-NAME")); // NOI18N

        jLabel3.setText(bundle.getString("SEARCH_FAMILY-CITY")); // NOI18N

        jLabel4.setText(bundle.getString("SEARCH_FAMILY-LEVEL")); // NOI18N

        jLabel5.setText(bundle.getString("SEARCH_FAMILY-PROPERTY")); // NOI18N

        jLabel6.setText(bundle.getString("SEARCH_FAMILY-HOUSE")); // NOI18N

        familyApplyButton.setFont(familyApplyButton.getFont().deriveFont(familyApplyButton.getFont().getStyle() | java.awt.Font.BOLD));
        familyApplyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        familyApplyButton.setText(bundle.getString("SEARCH_APPLY")); // NOI18N
        familyApplyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                familyApplyButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel2))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(familyNameEdit)
                    .addComponent(familyLevelCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(familyCityEdit)
                    .addComponent(familyPropertyCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(familyHouseCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(familyApplyButton)
                .addGap(102, 102, 102))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(familyNameEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(familyCityEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(familyLevelCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(familyPropertyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(familyHouseCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 100, Short.MAX_VALUE)
                .addComponent(familyApplyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(bundle.getString("SEARCH_FAMILY-TITLE"), jPanel1); // NOI18N

        jLabel12.setText(bundle.getString("SEARCH_FAMILY-NAME")); // NOI18N

        jLabel14.setText(bundle.getString("SEARCH_CLIENT-CONDITION")); // NOI18N

        jLabel16.setText(bundle.getString("SEARCH_CLIENT-GENDER")); // NOI18N

        clientApplyButton.setFont(clientApplyButton.getFont().deriveFont(clientApplyButton.getFont().getStyle() | java.awt.Font.BOLD));
        clientApplyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        clientApplyButton.setText(bundle.getString("SEARCH_APPLY")); // NOI18N
        clientApplyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientApplyButtonActionPerformed(evt);
            }
        });

        jLabel17.setText(bundle.getString("SEARCH_CLIENT-INCOME")); // NOI18N

        jLabel18.setText(bundle.getString("SEARCH_CLIENT-AGE")); // NOI18N

        genderGroup.add(clientGenderManRadio);
        clientGenderManRadio.setFont(clientGenderManRadio.getFont());
        clientGenderManRadio.setText(bundle.getString("SEARCH_CLIENT-GENDER-MAN")); // NOI18N

        genderGroup.add(clientGenderWomanRadio);
        clientGenderWomanRadio.setFont(clientGenderWomanRadio.getFont());
        clientGenderWomanRadio.setText(bundle.getString("SEARCH_CLIENT-GENDER-WOMAN")); // NOI18N

        genderGroup.add(clientGenderDontCareRadio);
        clientGenderDontCareRadio.setFont(clientGenderDontCareRadio.getFont());
        clientGenderDontCareRadio.setSelected(true);
        clientGenderDontCareRadio.setText(bundle.getString("SEARCH_CLIENT-GENDER-DOESNTMATTER")); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clientNameEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(clientConditionCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(clientIncomeCombo, 0, 93, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(clientIncomeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(clientAgeCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(clientAgeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(clientGenderManRadio)
                                    .addComponent(clientGenderWomanRadio)
                                    .addComponent(clientGenderDontCareRadio))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(clientApplyButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(clientNameEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(clientIncomeSpinner, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(clientIncomeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel17)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(clientAgeSpinner, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(clientAgeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(clientConditionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(clientGenderManRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clientGenderWomanRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clientGenderDontCareRadio)))
                .addGap(40, 40, 40)
                .addComponent(clientApplyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(bundle.getString("SEARCH_CLIENT-TITLE"), jPanel4); // NOI18N

        jLabel8.setText(bundle.getString("SEARCH_CONTRACT-INSTITUTION")); // NOI18N

        jLabel9.setText(bundle.getString("SEARCH_CONTRACT-TYPE")); // NOI18N

        contractApplyButton.setFont(contractApplyButton.getFont().deriveFont(contractApplyButton.getFont().getStyle() | java.awt.Font.BOLD));
        contractApplyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        contractApplyButton.setText(bundle.getString("SEARCH_APPLY")); // NOI18N
        contractApplyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contractApplyButtonActionPerformed(evt);
            }
        });

        jLabel7.setText(bundle.getString("SEARCH_CONTRACT-STARTDATE")); // NOI18N

        jLabel10.setText(bundle.getString("SEARCH_CONTRACT-ENDDATE")); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(jLabel8))
                .addGap(48, 48, 48)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(contractInstitutionEdit)
                    .addComponent(contractTypeCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contractStartDateBeginChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contractEndDateBeginChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contractStartDateEndChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contractEndDateEndChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(contractApplyButton)
                .addGap(102, 102, 102))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(contractInstitutionEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(contractTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(contractStartDateBeginChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contractStartDateEndChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(contractEndDateBeginChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contractEndDateEndChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(contractApplyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(bundle.getString("SEARCH_CONTRACT-TITLE"), jPanel3); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void familyApplyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_familyApplyButtonActionPerformed
        loadAttributesFromFormAndSearch(Family.class);
    }//GEN-LAST:event_familyApplyButtonActionPerformed

    private void clientApplyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientApplyButtonActionPerformed
        loadAttributesFromFormAndSearch(Client.class);
    }//GEN-LAST:event_clientApplyButtonActionPerformed

    private void contractApplyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contractApplyButtonActionPerformed
        loadAttributesFromFormAndSearch(Contract.class);
    }//GEN-LAST:event_contractApplyButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox clientAgeCombo;
    private javax.swing.JSpinner clientAgeSpinner;
    private javax.swing.JButton clientApplyButton;
    private javax.swing.JComboBox clientConditionCombo;
    private javax.swing.JRadioButton clientGenderDontCareRadio;
    private javax.swing.JRadioButton clientGenderManRadio;
    private javax.swing.JRadioButton clientGenderWomanRadio;
    private javax.swing.JComboBox clientIncomeCombo;
    private javax.swing.JSpinner clientIncomeSpinner;
    private javax.swing.JTextField clientNameEdit;
    private javax.swing.JButton contractApplyButton;
    private com.toedter.calendar.JDateChooser contractEndDateBeginChooser;
    private com.toedter.calendar.JDateChooser contractEndDateEndChooser;
    private javax.swing.JTextField contractInstitutionEdit;
    private com.toedter.calendar.JDateChooser contractStartDateBeginChooser;
    private com.toedter.calendar.JDateChooser contractStartDateEndChooser;
    private javax.swing.JComboBox contractTypeCombo;
    private javax.swing.JButton familyApplyButton;
    private javax.swing.JTextField familyCityEdit;
    private javax.swing.JComboBox familyHouseCombo;
    private javax.swing.JComboBox familyLevelCombo;
    private javax.swing.JTextField familyNameEdit;
    private javax.swing.JComboBox familyPropertyCombo;
    private javax.swing.ButtonGroup genderGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
