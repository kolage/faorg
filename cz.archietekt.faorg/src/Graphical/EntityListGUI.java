package Graphical;

import Database.Impl.DatabaseOperator;
import Database.Impl.EntityDBImpl;
import static Database.Impl.EntityDBImpl.EXPORT_DIR;
import Entity.Client;
import Entity.Contract;
import Entity.Entity;
import Entity.Family;
import Entity.Reminder;
import Faorg.Common;
import Print.PrintProcessor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 * Class representing window with entity listing. It is used for displaying 
 * search results or listing some entities.
 * 
 * @author kolage
 */
public class EntityListGUI extends javax.swing.JFrame {

    /**
     * Number of column in entity list table, where is reminder date displayed
     * when listing reminders.
     */
    private static final int REMINDER_DATE_COLUMN_NUMBER = 3;
    
    /**
     * Using singleton GUI.
     */
    private static EntityListGUI myInstance;
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;

    /**
     * Set to true when using window to show startup reminders.
     */
    private final boolean isStartupReminder;
    
    /**
     * Entity list table model.
     */
    private DefaultTableModel entityListModel;
    
    /**
     * List of entities to be shown in table.
     */
    private final List<? extends Entity> entityList;
    
    /**
     * Type of entity records in table.
     */
    private final Class typeOfEntity;
    
    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        EntityListGUI.myInstance = null;
    }
    
    /**
     * Get instance of EntityListGUI. If it not exists, return null.
     * @return instance of EntityListGUI
     */
    public static EntityListGUI getInstance() {
        return myInstance;
    }
    
    /**
     * Method for creating singleton EntityListGUI object.
     * @param dbOperator reference to DatabaseOperator
     * @param typeOfEntity type of entities stored in entity list
     * @param entityList list of entities to be displayed in table
     * @param isStartupReminder flag indicating if this window is used as
     * displaying reminders on startup or not
     * @return instance of entity list GUI
     */
    public static EntityListGUI getInstance(DatabaseOperator dbOperator, 
            Class typeOfEntity, List<? extends Entity> entityList, 
            boolean isStartupReminder) {
        if (myInstance == null)
            myInstance = new EntityListGUI(dbOperator, typeOfEntity, 
                    entityList,isStartupReminder);
        return myInstance;
    }
    
    /**
     * Creates new form EntityListGUI.
     */
    private EntityListGUI(DatabaseOperator dbOperator, 
            Class typeOfEntity, List<? extends Entity> entityList, 
            boolean isStartupReminder) {
        this.dbOperator = dbOperator;
        this.typeOfEntity = typeOfEntity;
        this.entityList = entityList;
        this.isStartupReminder = isStartupReminder;
        
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Method for initializing objects after successful logging in and before 
     * graphical elements are loaded.
     */
    private void preInit() {
        entityListModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if(getRowCount() > 0) {
                    return getValueAt(0, columnIndex).getClass();
                } else {
                    return super.getColumnClass(columnIndex);
                }
            }
        };
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        entityListTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    showEditEntityWindow(getSelectedEntity());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        
        if(typeOfEntity != Family.class) {
            envelopeButton.setEnabled(false);
            templatesButton.setEnabled(false);
        }
        
        addDataToTable();
        
        if(isStartupReminder) {
            setTitle(Common.bundle.getString("ENTITY_LIST-DB-REMINDER-TITLE"));
            entityListTable.setForeground(Color.RED);
        }
        
        entityListTable.setRowSorter(new TableRowSorter<>(entityListModel));
        
        entityListTable.setDefaultRenderer(Date.class, 
                new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                
                value = Common.getDateFormat().format(value);
                
                final Component component = 
                            super.getTableCellRendererComponent(
                            table, value, isSelected, hasFocus, row, column);
                
                if(!isStartupReminder && typeOfEntity == Reminder.class) {
                    Reminder reminder = (Reminder) entityList.
                            get(entityListTable.convertRowIndexToModel(row));
                    if(column == REMINDER_DATE_COLUMN_NUMBER && !isSelected &&
                            reminder.getDate().compareTo(
                            Common.getTodayDateAtEnd()) <= 0) {
                        setBackground(Common.AQUAMARINE_COLOR);
                    } else {
                        if(isSelected) {
                            setBackground(UIManager.getColor("Table.selectionBackground"));
                        } else {
                            setBackground(UIManager.getColor("Table.Background"));
                        }
                    }
                }
                
                return component;
            }
        });
    }
    
    /**
     * Auxiliary method for adding list of entity data in table.
     */
    private void addDataToTable() {
        if (typeOfEntity == Family.class) {
            entityListModel.setColumnIdentifiers(new String[]{
                Common.bundle.getString("ENTITY_LIST-DB-ID"),
                Common.bundle.getString("MAIN_DB-NAME"),
                Common.bundle.getString("MAIN_DB-CITY"),
                Common.bundle.getString("MAIN_DB-STREET"),
                Common.bundle.getString("MAIN_DB-ZIPCODE")
            });
            for (Entity entity : entityList) {
                Family family = (Family) entity;
                entityListModel.addRow(new Object[]{
                    family.getId(),
                    family.getName(),
                    family.getAddress().getCity(),
                    family.getAddress().getStreet(),
                    family.getAddress().getZipCode()
                });
            }
        } else if (typeOfEntity == Client.class) {
            entityListModel.setColumnIdentifiers(new String[]{
                Common.bundle.getString("ENTITY_LIST-DB-ID"),
                Common.bundle.getString("FAMILY_DB-CLIENT-NAME"),
                Common.bundle.getString("MAIN_DB-NAME"),
                Common.bundle.getString("FAMILY_DB-CLIENT-BIRTHDATE")
            });
            for (Entity entity : entityList) {
                Client client = (Client) entity;
                entityListModel.addRow(new Object[]{
                    client.getId(),
                    client.getFirstName(),
                    client.getDBOperator().getFamily().getName(),
                    client.getDateOfBirth()
                });
            }
        } else if (typeOfEntity == Contract.class) {
            entityListModel.setColumnIdentifiers(new String[]{
                Common.bundle.getString("ENTITY_LIST-DB-ID"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-TYPE"),
                Common.bundle.getString("FAMILY_DB-CLIENT-NAME"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-NUMBER"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-STARTDATE"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-ENDDATE")
            });
            for (Entity entity : entityList) {
                Contract contract = (Contract) entity;
                Client client = contract.getDBOperator().
                        getClient();
                Family clientFamily = client.getDBOperator().
                        getFamily();
                entityListModel.addRow(new Object[]{
                    contract.getId(),
                    Common.bundle.getString("CONTRACT-"
                    + contract.getType().toString()),
                    client.getFirstName() + " " + clientFamily.getName(),
                    contract.getIdentificationValue(),
                    contract.getStartDate(),
                    contract.getEndDate()
                });
            }
        } else if (typeOfEntity == Reminder.class) {
            entityListModel.setColumnIdentifiers(new String[]{
                Common.bundle.getString("ENTITY_LIST-DB-ID"),
                Common.bundle.getString("ENTITY_LIST-DB-REMINDER-TYPE"),
                Common.bundle.getString("ENTITY_LIST-DB-REMINDER-OBJECT"),
                Common.bundle.getString("ENTITY_LIST-DB-REMINDER-DATE"),
                Common.bundle.getString("ENTITY_LIST-DB-REMINDER-TARGETDATE"),
                Common.bundle.getString("ENTITY_LIST-DB-REMINDER-NOTE")
            });
            for (Entity entity : entityList) {
                Reminder reminder = (Reminder) entity;

                Entity parentObject = reminder.getDBOperator().
                        getParentObject();

                String parentObjectText = null;

                if (parentObject instanceof Family) {
                    parentObjectText = ((Family) parentObject).getName();
                } else if (parentObject instanceof Contract) {
                    Contract contract = (Contract) parentObject;
                    parentObjectText = contract.getIdentificationValue();
                } else if (parentObject instanceof Client) {
                    Client client = (Client) parentObject;
                    parentObjectText = client.getFirstName() + " "
                            + client.getDBOperator().getFamily().
                            getName();
                }

                entityListModel.addRow(new Object[]{
                    "",
                    Common.bundle.getString("REMINDER-" + reminder.getType().
                    toString()),
                    parentObjectText,
                    reminder.getDate(),
                    reminder.getTargetDate(),
                    reminder.getText()
                });
            }
        }
    }
    
    /**
     * Get family selected in family table.
     * @return selected family
     */
    private Entity getSelectedEntity() {
        int row = entityListTable.convertRowIndexToModel(
                entityListTable.getSelectedRow());
        return entityList.get(row);
    }
    
    /**
     * Shows edit window for relevant entity.
     */
    private void showEditEntityWindow(Entity entity) {
        if(entity != null) {
            if(entity instanceof Family) {
                FamilyGUI.getInstance(dbOperator,(Family) entity).
                        setVisible(true);
            } else if(entity instanceof Client) {
                Client client = (Client) entity;
                Family parentFamily = client.getDBOperator().
                        getFamily();
                ClientGUI.getInstance(dbOperator,client,parentFamily).
                        setVisible(true);
            } else if(entity instanceof Contract) {
                Contract contract = (Contract) entity;
                Client client = contract.getDBOperator().
                        getClient();
                Family parentFamily = client.getDBOperator().
                        getFamily();
                ClientGUI.getInstance(dbOperator,client,parentFamily).
                        setVisible(true);
                ClientGUI.getInstance(dbOperator,client,parentFamily).
                        switchToContractTabAndSelectContract(contract);
            } else if(entity instanceof Reminder) {
                Reminder reminder = (Reminder) entity;
                Entity reminderParent = reminder.getDBOperator().
                        getParentObject();
                showEditEntityWindow(reminderParent);
            }
        }
    }
    
    public void updateEntityListTable() {
        entityListModel.setRowCount(0);
        entityListModel.setColumnCount(0);
        addDataToTable();
    }
    
    private void showEnvelopePrintGUI() {
        if(typeOfEntity == Family.class) {
            Family[] familyArray = new Family[0];
            familyArray = entityList.toArray(familyArray);
            
            PrintProcessor.printEnvelope(
                dbOperator.getAdviser(),
                familyArray);
        }
    }
    
    private void showTemplatePrintGUI() {
        if(typeOfEntity == Family.class) {
            List<Family> familyList = new ArrayList<>();
            for(Entity entity : entityList) {
                familyList.add((Family) entity);
            }
            TemplatePrintGUI.getInstance(dbOperator,familyList).setVisible(true);
        }
    }
    
    private void exportEntity(List<? extends Entity> entities) {
        String newLine = System.getProperty("line.separator");
        FileWriter writer;
        
        try {
            writer = new FileWriter(EXPORT_DIR + "/" + Common.getCurrentTimeString() + "--" + 
                    entities.get(0).toString() + ".csv");
            
            writer.write(entities.get(0).headerCSV() + newLine);
            
            for(Entity entity : entities) {
                writer.write(entity.exportToCSV() + newLine);
            }
            
            writer.close();
        } catch(IOException ex) {
            System.out.println("Error while exporting entities to CSV.");
            System.err.println(ex);
        }
    }
    
    private void exportEntities(List<? extends Entity> entities) {
        if(entities.isEmpty()) {
            return;
        }
        
        exportEntity(entities);
        
        Entity protoEntity = entities.get(0);
        if(!(protoEntity instanceof Family) &&  !(protoEntity instanceof Client)) {
            return;
        }
        
        List<Entity> childEntities = new ArrayList();
        
        for(Entity entity : entities) {
            if(entity instanceof Family) {
                Family family = (Family) entity;
                childEntities.addAll(family.getDBOperator().getClients());
            } else if(entity instanceof Client) {
                Client client = (Client) entity;
                childEntities.addAll(client.getDBOperator().getContracts());
            }
        }
        
        exportEntities(childEntities);
    }
    
    private void exportData() {
        exportEntities(entityList);
        
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.OPEN)) {
                try {
                    desktop.open(new File(EntityDBImpl.EXPORT_DIR));
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            } else {
                PrintProcessor.showSavedDialog(EntityDBImpl.EXPORT_DIR, Common.bundle.getString("CSV_SAVED_IN"));
            }
        } else {
            PrintProcessor.showSavedDialog(EntityDBImpl.EXPORT_DIR, Common.bundle.getString("CSV_SAVED_IN"));
        }
    }
    
    private void deleteSelectedObject() {
        if(entityListTable.getSelectedRow() >= 0) {
            int answer = JOptionPane.showOptionDialog(
                    null,
                    Common.bundle.getString("ENTITY_LIST_CONFIRM-CLIENT-DELETE"),
                    Common.bundle.getString("ENTITY_LIST_CONFIRM-TITLE"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new String[] { Common.bundle.getString("MAIN_CONFIRM-OK"), 
                        Common.bundle.getString("MAIN_CONFIRM-NO")},
                    null);
            if(answer == JOptionPane.YES_OPTION) {
                Entity selectedEntity = getSelectedEntity();
                selectedEntity.getDBOperator().delete();
                entityList.remove(selectedEntity);
                updateEntityListTable();
                MainGUI mainGUI = MainGUI.getInstance();
                if (mainGUI != null) {
                    mainGUI.updateFamilies();
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        entityListTable = new javax.swing.JTable();
        okButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        envelopeButton = new javax.swing.JButton();
        exportButton = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        templatesButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("ENTITY_LIST-TITLE")); // NOI18N
        getContentPane().setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
        jPanel1Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel1.setLayout(jPanel1Layout);

        entityListTable.setModel(entityListModel);
        entityListTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        entityListTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(entityListTable);
        entityListTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPane1, gridBagConstraints);

        okButton.setFont(okButton.getFont().deriveFont(okButton.getFont().getStyle() | java.awt.Font.BOLD));
        okButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        okButton.setText(bundle.getString("CLIENT_BUTTON-OK")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel1.add(okButton, gridBagConstraints);

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/print.png"))); // NOI18N
        printButton.setText(bundle.getString("ENTITY_LIST-PRINT")); // NOI18N
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel1.add(printButton, gridBagConstraints);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/delete.png"))); // NOI18N
        deleteButton.setText(bundle.getString("ENTITY_LIST-REMOVE")); // NOI18N
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel1.add(deleteButton, gridBagConstraints);

        envelopeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/envelope.png"))); // NOI18N
        envelopeButton.setText(bundle.getString("ENTITY_LIST-ENVELOPE")); // NOI18N
        envelopeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                envelopeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel1.add(envelopeButton, gridBagConstraints);

        exportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/export.png"))); // NOI18N
        exportButton.setText(bundle.getString("ENTITY_LIST-EXPORT")); // NOI18N
        exportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        jPanel1.add(exportButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 12);
        jPanel1.add(filler2, gridBagConstraints);

        templatesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/documents.png"))); // NOI18N
        templatesButton.setText(bundle.getString("ENTITY_LIST-TEMPLATES")); // NOI18N
        templatesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                templatesButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel1.add(templatesButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void exportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportButtonActionPerformed
        exportData();
    }//GEN-LAST:event_exportButtonActionPerformed

    private void envelopeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_envelopeButtonActionPerformed
        showEnvelopePrintGUI();
    }//GEN-LAST:event_envelopeButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        deleteSelectedObject();
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            entityListTable.print();
        } catch (PrinterException ex) {
            System.err.println(ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void templatesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_templatesButtonActionPerformed
        showTemplatePrintGUI();
    }//GEN-LAST:event_templatesButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton deleteButton;
    private javax.swing.JTable entityListTable;
    private javax.swing.JButton envelopeButton;
    private javax.swing.JButton exportButton;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JButton printButton;
    private javax.swing.JButton templatesButton;
    // End of variables declaration//GEN-END:variables
}
