package Graphical;

import Faorg.StringPair;
import Entity.Impl.ClientImpl;
import Entity.Impl.ContractImpl;
import Graphical.FamilyGUI.EditMode;
import Database.Impl.DatabaseOperator;
import Entity.Client;
import Entity.Client.Gender;
import Entity.Client.SportLevel;
import Entity.Client.WorkingCondition;
import Entity.Contract;
import Entity.Contract.ContractFrequency;
import Entity.Contract.ContractType;
import Entity.Family;
import Entity.Reminder.ReminderType;
import Faorg.Common;
import Main.Java2sAutoTextField;
import com.toedter.calendar.JDateChooser;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractCellEditor;
import javax.swing.AbstractListModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

/**
 * Class representing client window. It is used for displaying information
 * about client and creating and editing client contracts.
 * 
 * @author kolage
 */
public class ClientGUI extends javax.swing.JFrame {
    
    /**
     * Class implementing number spinner in table.
     */
    class NumberSpinnerCellEditor extends AbstractCellEditor 
        implements TableCellEditor {

        private final JSpinner spinner = new JSpinner();

        public NumberSpinnerCellEditor(Long value, Long minimum, Long maximum, 
                int stepSize) {
            spinner.setModel(new SpinnerNumberModel((Number) value, 
                    minimum, maximum, stepSize));
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, 
                boolean isSelected, int row, int column) {
            spinner.setValue(value);
            return spinner;
        }

        @Override
        public boolean isCellEditable(EventObject evt) {
            if (evt instanceof MouseEvent) {
                return ((MouseEvent) evt).getClickCount() >= 2;
            }
            return true;
        }

        @Override
        public Object getCellEditorValue() {
            return spinner.getValue();
        }
    }
    
    /**
     * Class implementing date chooser in table.
     */
    class CalendarCellEditor extends AbstractCellEditor
        implements TableCellEditor {

        private final JDateChooser calendar = 
                new JDateChooser(
                    new Date(System.currentTimeMillis()),
                    Common.getDateFormatString());

        public CalendarCellEditor() {
            calendar.setLocale(new Locale("cs"));
            calendar.addPropertyChangeListener("date", 
                    new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    stopCellEditing();
                }
            });
        }
        
        
        @Override
        public Object getCellEditorValue() {
            return Common.getDateFormat().format(calendar.getDate());
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, 
                boolean isSelected, int row, int column) {
            try {
                calendar.setDate(Common.getDateFormat().parse((String) 
                        table.getValueAt(row, column)));
            } catch (ParseException ex) {
                System.err.println(ex);
            }
            return calendar;
        }
        
        @Override
        public boolean isCellEditable(EventObject evt) {
            if (evt instanceof MouseEvent) {
                return ((MouseEvent) evt).getClickCount() >= 2;
            }
            return true;
        }
    }
    
    /**
     * Class used for displaying row header in contract table.
     */
    class RowHeaderRenderer extends JLabel implements ListCellRenderer {

        RowHeaderRenderer(JTable table) {
            JTableHeader header = table.getTableHeader();
            setOpaque(true);
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setForeground(header.getForeground());
            setBackground(header.getBackground());
            setFont(header.getFont());
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }
    
    /**
     * Using singleton GUI.
     */
    private static ClientGUI myInstance;
    
    /**
     * Number of special cell editors in contract table.
     */
    private final int cellEditorsCount = 6;
    
    /**
     * Row header fixed width.
     */
    private final int rowHeaderWidth = 120;
    
    /**
     * Contract table cell fixed width.
     */
    private final int contractColumnWidth = 130;
    
    /**
     * Contract table cell fixed height.
     */
    private final int cellHeight = 25;
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;

    /**
     * Table model displaying members of family from DB.
     */
    private DefaultTableModel contractModel;
    
    /**
     * List model for row header list.
     */
    private AbstractListModel rowHeaderListModel;
    
    /**
     * Client that is being operated with.
     */
    private Client client;
    
    /**
     * Parent family object.
     */
    private final Family family;
    
    /**
     * Editing mode.
     */
    private final EditMode mode;
    
    /**
     * Contracts fetched from DB.
     */
    private HashMap<Integer,Contract> contracts;
    
    /**
     * Special cell editors for contract table.
     */
    private final List<TableCellEditor> cellEditors;
    
    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        ClientGUI.myInstance = null;
    }
    
    /**
     * Get instance of ClientGUI. If it not exists, return null.
     * @return instance of ClientGUI
     */
    public static ClientGUI getInstance() {
        return myInstance;
    }
    
    /**
     * Method for creating singleton ClientGUI object.
     * @param dbOperator reference to DatabaseOperator
     * @param client relevant client
     * @param family relevant family
     * @return instance of client GUI
     */
    public static ClientGUI getInstance(DatabaseOperator dbOperator, 
            Client client, Family family) {
        if (myInstance == null)
            myInstance = new ClientGUI(dbOperator, client, family);

        return myInstance;
    }
    
    /**
     * Switches to contracts tab and tries to select specified contract from
     * table.
     * @param contract contract to be selected
     */
    public void switchToContractTabAndSelectContract(Contract contract) {
        tabbedPane.setSelectedIndex(1);
        for(int i = 0; i < contractTable.getColumnCount(); i++) {
            Integer contractId = (Integer) contractTable.getValueAt(0, i);
            if(contract.getId().compareTo(contractId) == 0) {
                contractTable.setColumnSelectionInterval(i, i);
                contractTable.setRowSelectionInterval(0, 0);
                contractTable.requestFocusInWindow();
                contractTable.scrollRectToVisible(new Rectangle(
                        contractTable.getCellRect(0, i, true)));
                return;
            }
        }
    }
    
    /**
     * Creates new form ClientGUI.
     */
    private ClientGUI(DatabaseOperator dbOperator, Client client, 
            Family family) {
        this.cellEditors = new ArrayList<>(cellEditorsCount);
        this.dbOperator = dbOperator;
        this.client = client;
        this.family = family;
        
        if(client == null) {
            mode = EditMode.NEW;
        } else {
            mode = EditMode.EDIT;
        }
        
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Update contracts hash set and contract list model after making changes 
     * in DB.
     */
    public void updateContracts() {
        fetchContracts();
    }
    
    /**
     * Method for initializing objects before graphical elements are loaded.
     */
    private void preInit() {
        if(mode == EditMode.NEW) {
            client = new ClientImpl(dbOperator,
                    dbOperator.generateNewId("client"),
                    family.getId());
        }
        
        // disable editing the first row with contract ID and last row with
        // "has reminder" flag
        contractModel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                if(row != 0) {
                    return super.isCellEditable(row, column);
                } else {
                    return false;
                }
            }
            
        };
        
        setCellEditors();
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        fetchContracts();
        
        if(mode == EditMode.EDIT) {
            insertClientAttributesFromObject();
            setTitle(Common.bundle.getString("CLIENT_TITLE-INFO"));
        } else {
            setTitle(Common.bundle.getString("CLIENT_TITLE-NEW"));
        }
        
        familyLabel.setText(family.getName());
        setTitle(client.getFirstName() + " " + family.getName());
        
        setRowHeaders();
        
        identityCardValidDateChooser.setLocale(new Locale(Common.LOCALE_STRING));
    }
    
    /**
     * Auxiliary method for setting up contract table cell editors.
     */
    private void setCellEditors() {
        StringPair[] typePairArray = 
                new StringPair[ContractType.values().length];
        
        int i = 0;
        for(ContractType type : ContractType.values()) {
            typePairArray[i++] = new StringPair(type.toString(),
                    Common.bundle.getString("CONTRACT-"
                        + type.toString()));
        }
        
        JComboBox contractTypeCombo = new JComboBox(typePairArray);
        contractTypeCombo.setMaximumRowCount(
                contractTypeCombo.getModel().getSize());
        
        cellEditors.add(new DefaultCellEditor(contractTypeCombo) {
            @Override
            public boolean isCellEditable(EventObject evt) {
                if (evt instanceof MouseEvent) {
                    return ((MouseEvent) evt).getClickCount() >= 2;
                }
                return true;
            }
        });
        
        Java2sAutoTextField institutionField = 
                new Java2sAutoTextField(ContractImpl.getInstitutions());
        
        DefaultCellEditor institutionEditor = 
                new DefaultCellEditor(institutionField);
        
        cellEditors.add(institutionEditor);
        
        CalendarCellEditor startDateEditor = new CalendarCellEditor();
        cellEditors.add(startDateEditor);
        
        CalendarCellEditor endDateEditor = new CalendarCellEditor();
        cellEditors.add(endDateEditor);
        
        TableCellEditor paymentEditor = new NumberSpinnerCellEditor(
                0L, 0L, 200000L, 1);
        cellEditors.add(paymentEditor);
        
        TableCellEditor targetAmountEditor = new NumberSpinnerCellEditor(
                0L, 0L, 10000000L, 100);
        cellEditors.add(targetAmountEditor);
        
        StringPair[] frequencyPairArray = 
                new StringPair[ContractFrequency.values().length];
        
        i = 0;
        for(ContractFrequency frequency : ContractFrequency.values()) {
            frequencyPairArray[i++] = new StringPair(frequency.toString(),
                    Common.bundle.getString("CONTRACT-"
                        + frequency.toString()));
        }
        
        JComboBox frequencyComboBox = new JComboBox(frequencyPairArray);
        
        cellEditors.add(new DefaultCellEditor(frequencyComboBox) {
            @Override
            public boolean isCellEditable(EventObject evt) {
                if (evt instanceof MouseEvent) {
                    return ((MouseEvent) evt).getClickCount() >= 2;
                }
                return true;
            }
        });
    }
    
    /**
     * Auxiliary method for setting up contract table row headers.
     */
    private void setRowHeaders() {
        rowHeaderListModel = new AbstractListModel() {
            String headers[] = { 
                Common.bundle.getString("CLIENT_DB-CONTRACT-ID"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-TYPE"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-INSTITUTION"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-NUMBER"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-STARTDATE"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-ENDDATE"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-PAYMENT"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-TARGET-AMOUNT"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-FREQUENCY"),
                Common.bundle.getString("CLIENT_DB-CONTRACT-NOTE")
            };

            @Override
            public int getSize() {
                return headers.length;
            }

            @Override
            public Object getElementAt(int index) {
                return headers[index];
            }
        };
        
        JList rowHeader = new JList(rowHeaderListModel);
        rowHeader.setFixedCellWidth(rowHeaderWidth);

        rowHeader.setFixedCellHeight(cellHeight);
        contractTable.setRowHeight(cellHeight);
        
        rowHeader.setCellRenderer(new RowHeaderRenderer(contractTable));
        
        contractScrollPane.setRowHeaderView(rowHeader);
        contractScrollPane.setColumnHeaderView(null);
        
        contractModel.setRowCount(rowHeaderListModel.getSize());
    }
    
    /**
     * Method will fetch written and selected client attributes and stores it
     * in client object.
     */
    private void loadClientAttributesFromForm() {
        // fetch client gender selection
        Gender gender = Gender.MAN;
        if(womanSexRadio.isSelected()) {
            gender = Gender.WOMAN;
        }
        
        client.setFirstName(nameEdit.getText());
        client.setIdentificationNumberAndGender(
                identificationNumberEdit.getText(),
                gender);
        
        client.setDoctor(doctorEdit.getText());
        
        client.setIdentityCardNumber(identityCardNumberEdit.getText());
        client.setIdentityCardValidity(identityCardValidDateChooser.getDate());
        client.setIdentityCardIssuer(identityCardIssuerEdit.getText());
        client.setBirthPlace(identityCardBirthPlaceEdit.getText());
        
        client.setIncome((Long) incomeSpinner.getValue());
        
        client.setBankAccount(bankAccountNumberEdit.getText(), 
                bankCodeEdit.getText());
        
        // fetch client employment selection
        WorkingCondition condition = WorkingCondition.UNEMPLOYED;
        if(employedRadio.isSelected()) {
            condition = WorkingCondition.EMPLOYED;
        } else if(tradesmanRadio.isSelected()) {
            condition = WorkingCondition.TRADESMAN;
        } else if(entrepreneurRadio.isSelected()) {
            condition = WorkingCondition.ENTREPRENEUR;
        }
        
        client.setWorkingCondition(condition);
        client.setProfession(professionEdit.getText());
        
        // fetch client sport level
        SportLevel level = SportLevel.NO_SPORT;
        if(recreationalSportRadio.isSelected()) {
            level = SportLevel.RECREATIONAL;
        } else if(professionalSportRadio.isSelected()) {
            level = SportLevel.PROFESSIONAL;
        }
        
        client.setSportLevel(level);
        client.setSportType(sportTypeEdit.getText());
        
        client.setNote(clientNote.getText());
        
        // load telephones
        client.clearTelephones();
        
        ListModel telephoneModel = telephoneList.getModel();
        
        for(int i = 0; i < telephoneModel.getSize(); i++) {
            client.addTelephoneNumber((String) telephoneModel.getElementAt(i));
        }
        
        // load emails
        client.clearEmails();
        
        ListModel emailModel = emailList.getModel();
        
        for(int i = 0; i < emailModel.getSize(); i++) {
            client.addEmail((String) emailModel.getElementAt(i));
        }
    }
    
    /**
     * Method will fetch written and selected contracts attributes and stores it
     * in contracts hashmap.
     */
    private void loadContractsAttributesFromForm() {
        for(int i = 0; i < contractModel.getColumnCount(); i++) {
            Integer contractId = (Integer) contractModel.getValueAt(0, i);
            Contract actualContract = contracts.get(contractId);
            
            actualContract.setType(
                    ((StringPair) contractModel.getValueAt(1, i)).getKey());
            actualContract.setInstitutionName(
                    contractModel.getValueAt(2, i).toString());
            actualContract.setIdentificationValue(
                    contractModel.getValueAt(3, i).toString());
            
            try {
                Date startDate = Common.getDateFormat().parse(
                        (String) contractModel.getValueAt(4, i));
                actualContract.setStartDate(startDate);
                
                Date endDate = Common.getDateFormat().parse(
                        (String) contractModel.getValueAt(5, i));
                actualContract.setEndDate(endDate);
            } catch (ParseException ex) {
                System.err.println(ex);
            }
            
            actualContract.setPayment(Integer.parseInt(
                    contractModel.getValueAt(6, i).toString()));
            actualContract.setTargetAmount(
                    Long.parseLong(contractModel.getValueAt(7, i).toString()));
            actualContract.setFrequency(
                    ((StringPair) contractModel.getValueAt(8, i)).getKey());
            actualContract.setNote(
                    contractModel.getValueAt(9, i).toString());
        }
    }
    
    /**
     * Method will load client attributes from selected object in DB and 
     * outputs it to GUI.
     */
    private void insertClientAttributesFromObject() {
        nameEdit.setText(client.getFirstName());
        identificationNumberEdit.setText(client.getIdentificationNumber());
        
        switch(client.getGender()) {
            case MAN:
                manSexRadio.setSelected(true);
                break;
            case WOMAN:
                womanSexRadio.setSelected(true);
                break;
        }
        
        dateOfBirthLabel.setText(
                Common.getDateFormat().format(client.getDateOfBirth()));
        ageLabel.setText(client.getAge().toString());
        
        doctorEdit.setText(client.getDoctor());
        
        identityCardNumberEdit.setText(client.getIdentityCardNumber());
        identityCardValidDateChooser.setDate(client.getIdentityCardValidity());
        identityCardIssuerEdit.setText(client.getIdentityCardIssuer());
        identityCardBirthPlaceEdit.setText(client.getBirthPlace());
        
        incomeSpinner.setValue(client.getIncome());
        
        bankAccountNumberEdit.setText(client.getBankAccountNumber());
        bankCodeEdit.setText(client.getBankCode());
        
        switch(client.getWorkingCondition()) {
            case UNEMPLOYED:
                unemployedRadio.setSelected(true);
                break;
            case EMPLOYED:
                employedRadio.setSelected(true);
                break;
            case TRADESMAN:
                tradesmanRadio.setSelected(true);
                break;
            case ENTREPRENEUR:
                entrepreneurRadio.setSelected(true);
                break;
        }
        
        professionEdit.setText(client.getProfession());
        
        switch(client.getSportLevel()) {
            case NO_SPORT:
                notSportingRadio.setSelected(true);
                break;
            case RECREATIONAL:
                recreationalSportRadio.setSelected(true);
                break;
            case PROFESSIONAL:
                professionalSportRadio.setSelected(true);
                break;
        }
        
        sportTypeEdit.setText(client.getSportType());
        
        clientNote.setText(client.getNote());
        
        DefaultListModel telephoneModel = 
                (DefaultListModel) telephoneList.getModel();
        
        for(String telephone : client.getTelephones()) {
            telephoneModel.addElement(telephone);
        }
        
        DefaultListModel emailModel = (DefaultListModel) emailList.getModel();
        
        for(String email : client.getEmails()) {
            emailModel.addElement(email);
        }
    }
    
    /**
     * Computes age and birth date from client gender and IN.
     */
    private void computeAgeAndBirthDate() {
        Gender gender = Gender.MAN;
        
        if(womanSexRadio.isSelected()) {
            gender = Gender.WOMAN;
        }
        
        client.setIdentificationNumberAndGender(
                identificationNumberEdit.getText(), 
                gender);
        
        ageLabel.setText(client.getAge().toString());
        dateOfBirthLabel.setText(Common.getDateFormat().format(
                client.getDateOfBirth()));
    }
    
    /**
     * Updates table column width after adding new column.
     */
    private void updateColumnsWidth() {
        TableColumnModel columnModel = contractTable.getColumnModel();
        for(int i = 0; i < contractModel.getColumnCount(); i++) {
            columnModel.getColumn(i).setMaxWidth(contractColumnWidth);
            columnModel.getColumn(i).setMinWidth(contractColumnWidth);
        }
    }
    
    /**
     * Get contract selected in contract table.
     * @return selected contract
     */
    private Contract getSelectedContract() {
        int column = contractTable.getSelectedColumn();
        Integer contractId = (Integer) contractTable.getValueAt(0, column);
        return contracts.get(contractId);
    }
    
    /**
     * Fetch contracts from DB and update table model.
     */
    private void fetchContracts() {
        // clear contracts table
        contractModel.setRowCount(0);
        contractModel.setColumnCount(0);
        
        contracts = new HashMap<>();
        List<? extends Contract> contractList = client.getDBOperator().getContracts();
        
        for(Contract contract : contractList) {
            
            contracts.put(contract.getId(), contract);
            contractModel.addColumn("contract", new Object[] 
                {
                    contract.getId(),
                    new StringPair(contract.getType().toString(),
                        Common.bundle.getString("CONTRACT-"
                        + contract.getType().toString())),
                    contract.getInstitutionName(),
                    contract.getIdentificationValue(),
                    Common.getDateFormat().format(contract.getStartDate()),
                    Common.getDateFormat().format(contract.getEndDate()),
                    contract.getPayment(),
                    contract.getTargetAmount(),
                    new StringPair(contract.getFrequency().toString(),
                        Common.bundle.getString("CONTRACT-"
                        + contract.getFrequency().toString())),
                    contract.getNote(),
                });
            
        }
        updateColumnsWidth();
    }
    
    /**
     * Saves actual contracts values from form to DB.
     */
    private void saveContracts() {
        loadContractsAttributesFromForm();
        for(Contract contract : contracts.values()) {
            contract.getDBOperator().update();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sexGroup = new javax.swing.ButtonGroup();
        employGroup = new javax.swing.ButtonGroup();
        sportGroup = new javax.swing.ButtonGroup();
        tabbedPane = new javax.swing.JTabbedPane();
        clientPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        nameEdit = new javax.swing.JTextField();
        familyLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        manSexRadio = new javax.swing.JRadioButton();
        womanSexRadio = new javax.swing.JRadioButton();
        identificationNumberEdit = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ageLabel = new javax.swing.JLabel();
        dateOfBirthLabel = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        doctorEdit = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel15 = new javax.swing.JLabel();
        identityCardNumberEdit = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        identityCardValidDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        identityCardIssuerEdit = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        identityCardBirthPlaceEdit = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        incomeSpinner = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        unemployedRadio = new javax.swing.JRadioButton();
        employedRadio = new javax.swing.JRadioButton();
        tradesmanRadio = new javax.swing.JRadioButton();
        entrepreneurRadio = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        bankAccountNumberEdit = new javax.swing.JTextField();
        bankCodeEdit = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        professionEdit = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        notSportingRadio = new javax.swing.JRadioButton();
        recreationalSportRadio = new javax.swing.JRadioButton();
        professionalSportRadio = new javax.swing.JRadioButton();
        jLabel21 = new javax.swing.JLabel();
        sportTypeEdit = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        telephoneList = new javax.swing.JList();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        emailList = new javax.swing.JList();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        clientNote = new javax.swing.JTextArea();
        clientReminderButton = new javax.swing.JButton();
        telephoneAddButton = new javax.swing.JButton();
        emailAddButton = new javax.swing.JButton();
        emailDeleteButton = new javax.swing.JButton();
        telephoneDeleteButton = new javax.swing.JButton();
        telephoneEdit = new javax.swing.JTextField();
        emailEdit = new javax.swing.JTextField();
        contractPanel = new javax.swing.JPanel();
        contractScrollPane = new javax.swing.JScrollPane();
        contractTable = new JTable() {
            public TableCellEditor getCellEditor(int row, int column) {
                if (row == 1)
                return cellEditors.get(0);
                else if (row == 2) {
                    return cellEditors.get(1);
                }
                else if (row == 4) {
                    return cellEditors.get(2);
                }
                else if (row == 5) {
                    return cellEditors.get(3);
                }
                else if (row == 6) {
                    return cellEditors.get(4);
                }
                else if (row == 7) {
                    return cellEditors.get(5);
                }
                else if (row == 8) {
                    return cellEditors.get(6);
                } else
                return super.getCellEditor(row, column);
            }
        };
        addContractButton = new javax.swing.JButton();
        removeContractButton = new javax.swing.JButton();
        contractReminderButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nameEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        familyLabel.setFont(familyLabel.getFont());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        jLabel1.setText(bundle.getString("CLIENT-FAMILY")); // NOI18N

        jLabel2.setText(bundle.getString("CLIENT-NAME")); // NOI18N

        jLabel7.setText(bundle.getString("CLIENT-SEX")); // NOI18N

        sexGroup.add(manSexRadio);
        manSexRadio.setFont(manSexRadio.getFont());
        manSexRadio.setSelected(true);
        manSexRadio.setText(bundle.getString("CLIENT-MAN")); // NOI18N

        sexGroup.add(womanSexRadio);
        womanSexRadio.setFont(womanSexRadio.getFont());
        womanSexRadio.setText(bundle.getString("CLIENT-WOMAN")); // NOI18N

        identificationNumberEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        identificationNumberEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                identificationNumberEditActionPerformed(evt);
            }
        });

        jLabel4.setText(bundle.getString("CLIENT-IN")); // NOI18N

        jLabel5.setText(bundle.getString("CLIENT-DATEOFBIRTH")); // NOI18N

        jLabel3.setText(bundle.getString("CLIENT-AGE")); // NOI18N

        ageLabel.setFont(ageLabel.getFont());
        ageLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        ageLabel.setText(bundle.getString("CLIENT-AGE-VALUE")); // NOI18N

        dateOfBirthLabel.setFont(dateOfBirthLabel.getFont());
        dateOfBirthLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dateOfBirthLabel.setText(bundle.getString("CLIENT-DATEOFBIRTH-VALUE")); // NOI18N

        jLabel14.setText(bundle.getString("CLIENT-DOCTOR")); // NOI18N

        jLabel15.setText(bundle.getString("CLIENT-IDENTITY-CARD-NUMBER")); // NOI18N

        identityCardNumberEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel16.setText(bundle.getString("CLIENT-IDENTITY-CARD-VALID_DATE")); // NOI18N

        jLabel17.setText(bundle.getString("CLIENT-IDENTITY-CARD-ISSUER")); // NOI18N

        identityCardIssuerEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel18.setText(bundle.getString("CLIENT-IDENTITY-CARD-BIRTH_PLACE")); // NOI18N

        identityCardBirthPlaceEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jSeparator1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(familyLabel)
                            .addComponent(nameEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(manSexRadio)
                            .addComponent(womanSexRadio)
                            .addComponent(identificationNumberEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dateOfBirthLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ageLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(doctorEdit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(identityCardNumberEdit)
                            .addComponent(identityCardValidDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel17)
                        .addGap(78, 78, 78)
                        .addComponent(identityCardIssuerEdit))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18)
                        .addGap(52, 52, 52)
                        .addComponent(identityCardBirthPlaceEdit)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(familyLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nameEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(manSexRadio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(womanSexRadio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(identificationNumberEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dateOfBirthLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(ageLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(doctorEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(identityCardNumberEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel16)
                    .addComponent(identityCardValidDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(identityCardIssuerEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(identityCardBirthPlaceEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setPreferredSize(new java.awt.Dimension(340, 204));

        jLabel6.setText(bundle.getString("CLIENT-INCOME")); // NOI18N

        incomeSpinner.setModel(new javax.swing.SpinnerNumberModel(Long.valueOf(10000L), Long.valueOf(0L), Long.valueOf(1000000L), Long.valueOf(1000L)));

        jLabel8.setText(bundle.getString("CLIENT-JOB")); // NOI18N

        employGroup.add(unemployedRadio);
        unemployedRadio.setFont(unemployedRadio.getFont());
        unemployedRadio.setSelected(true);
        unemployedRadio.setText(bundle.getString("CLIENT-UNEMPLOYED")); // NOI18N

        employGroup.add(employedRadio);
        employedRadio.setFont(employedRadio.getFont());
        employedRadio.setText(bundle.getString("CLIENT-EMPLOYED")); // NOI18N

        employGroup.add(tradesmanRadio);
        tradesmanRadio.setFont(tradesmanRadio.getFont());
        tradesmanRadio.setText(bundle.getString("CLIENT-TRADESMAN")); // NOI18N

        employGroup.add(entrepreneurRadio);
        entrepreneurRadio.setFont(entrepreneurRadio.getFont());
        entrepreneurRadio.setText(bundle.getString("CLIENT-ENTREPRENEUR")); // NOI18N

        jLabel9.setText(bundle.getString("CLIENT-BANKACCOUNT")); // NOI18N

        bankAccountNumberEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        bankCodeEdit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel10.setText("/");

        jLabel19.setText(bundle.getString("CLIENT-PROFESSION")); // NOI18N

        jLabel20.setText(bundle.getString("CLIENT-SPORT-LEVEL")); // NOI18N

        sportGroup.add(notSportingRadio);
        notSportingRadio.setFont(notSportingRadio.getFont());
        notSportingRadio.setSelected(true);
        notSportingRadio.setText(bundle.getString("CLIENT-NO_SPORT")); // NOI18N

        sportGroup.add(recreationalSportRadio);
        recreationalSportRadio.setFont(recreationalSportRadio.getFont());
        recreationalSportRadio.setText(bundle.getString("CLIENT-RECREATIONAL")); // NOI18N

        sportGroup.add(professionalSportRadio);
        professionalSportRadio.setFont(professionalSportRadio.getFont());
        professionalSportRadio.setText(bundle.getString("CLIENT-PROFESSIONAL")); // NOI18N

        jLabel21.setText(bundle.getString("CLIENT-SPORT-TYPE")); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(39, 39, 39)
                        .addComponent(bankAccountNumberEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bankCodeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel6)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel19))
                        .addGap(49, 49, 49)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(professionEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(recreationalSportRadio)
                            .addComponent(notSportingRadio)
                            .addComponent(professionalSportRadio)
                            .addComponent(incomeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(employedRadio)
                            .addComponent(unemployedRadio)
                            .addComponent(tradesmanRadio)
                            .addComponent(entrepreneurRadio)
                            .addComponent(sportTypeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(incomeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(unemployedRadio)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(employedRadio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tradesmanRadio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(entrepreneurRadio)
                .addGap(1, 1, 1)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(professionEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(bankAccountNumberEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(bankCodeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(notSportingRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(recreationalSportRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(professionalSportRadio)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21)
                    .addComponent(sportTypeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel11.setText(bundle.getString("CLIENT-TELEPHONES")); // NOI18N

        telephoneList.setModel(new DefaultListModel<>());
        jScrollPane1.setViewportView(telephoneList);

        jLabel12.setText(bundle.getString("CLIENT-EMAILS")); // NOI18N

        emailList.setModel(new DefaultListModel<>());
        jScrollPane2.setViewportView(emailList);

        jLabel13.setText(bundle.getString("CLIENT-NOTES")); // NOI18N

        clientNote.setColumns(20);
        clientNote.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        clientNote.setRows(5);
        jScrollPane4.setViewportView(clientNote);

        clientReminderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminder.png"))); // NOI18N
        clientReminderButton.setText(bundle.getString("CLIENT_BUTTON-REMINDER")); // NOI18N
        clientReminderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientReminderButtonActionPerformed(evt);
            }
        });

        telephoneAddButton.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        telephoneAddButton.setText("+");
        telephoneAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telephoneAddButtonActionPerformed(evt);
            }
        });

        emailAddButton.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        emailAddButton.setText("+");
        emailAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailAddButtonActionPerformed(evt);
            }
        });

        emailDeleteButton.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        emailDeleteButton.setText("-");
        emailDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailDeleteButtonActionPerformed(evt);
            }
        });

        telephoneDeleteButton.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        telephoneDeleteButton.setText("-");
        telephoneDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telephoneDeleteButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(telephoneEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(emailAddButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(telephoneDeleteButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(telephoneAddButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(emailDeleteButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(clientReminderButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(telephoneAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telephoneEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(telephoneDeleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(clientReminderButton)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(emailEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailDeleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout clientPanelLayout = new javax.swing.GroupLayout(clientPanel);
        clientPanel.setLayout(clientPanelLayout);
        clientPanelLayout.setHorizontalGroup(
            clientPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clientPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(clientPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(clientPanelLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        clientPanelLayout.setVerticalGroup(
            clientPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clientPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(clientPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabbedPane.addTab(bundle.getString("CLIENT_TAB-CLIENT"), new javax.swing.ImageIcon(getClass().getResource("/pictures/person.png")), clientPanel); // NOI18N

        contractScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        contractTable.setModel(contractModel);
        contractTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        contractTable.setCellSelectionEnabled(true);
        contractTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        contractTable.getTableHeader().setResizingAllowed(false);
        contractTable.getTableHeader().setReorderingAllowed(false);
        contractScrollPane.setViewportView(contractTable);
        contractTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        addContractButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/add.png"))); // NOI18N
        addContractButton.setText(bundle.getString("CLIENT_BUTTON-ADDCONTRACT")); // NOI18N
        addContractButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addContractButtonActionPerformed(evt);
            }
        });

        removeContractButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/delete.png"))); // NOI18N
        removeContractButton.setText(bundle.getString("CLIENT_BUTTON-DELETECONTRACT")); // NOI18N
        removeContractButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeContractButtonActionPerformed(evt);
            }
        });

        contractReminderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminder.png"))); // NOI18N
        contractReminderButton.setText(bundle.getString("CLIENT_BUTTON-REMINDER")); // NOI18N
        contractReminderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contractReminderButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout contractPanelLayout = new javax.swing.GroupLayout(contractPanel);
        contractPanel.setLayout(contractPanelLayout);
        contractPanelLayout.setHorizontalGroup(
            contractPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contractPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contractPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(contractScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 689, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(contractPanelLayout.createSequentialGroup()
                        .addComponent(addContractButton, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(removeContractButton, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(contractReminderButton, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        contractPanelLayout.setVerticalGroup(
            contractPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contractPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(contractScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(contractPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addContractButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(removeContractButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contractReminderButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tabbedPane.addTab(bundle.getString("CLIENT_TAB-CONTRACTS"), new javax.swing.ImageIcon(getClass().getResource("/pictures/contracts.png")), contractPanel); // NOI18N

        applyButton.setFont(applyButton.getFont().deriveFont(applyButton.getFont().getStyle() | java.awt.Font.BOLD));
        applyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        applyButton.setText(bundle.getString("CLIENT_BUTTON-OK")); // NOI18N
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 718, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(applyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(applyButton)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        loadClientAttributesFromForm();
        
        if(mode == EditMode.NEW) {
            client.getDBOperator().insert();
        } else {
            client.getDBOperator().update();
        }
        
        saveContracts();
        
        FamilyGUI familyGUI = FamilyGUI.getInstance();
        if(familyGUI != null) {
            familyGUI.updateClients();
        }
        this.dispose();
    }//GEN-LAST:event_applyButtonActionPerformed

    private void identificationNumberEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_identificationNumberEditActionPerformed
        computeAgeAndBirthDate();
    }//GEN-LAST:event_identificationNumberEditActionPerformed

    private void addContractButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addContractButtonActionPerformed
        Contract contract = new ContractImpl(dbOperator,
                dbOperator.generateNewId("contract"), 
                client.getId());
        contract.getDBOperator().insert();
        saveContracts();
        updateContracts();
    }//GEN-LAST:event_addContractButtonActionPerformed

    private void removeContractButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeContractButtonActionPerformed
        if(contractTable.getSelectedColumn() >= 0) {
            int answer = JOptionPane.showOptionDialog(
                    null,
                    Common.bundle.getString("CLIENT_CONFIRM-CONTRACT-DELETE"),
                    Common.bundle.getString("CLIENT_CONFIRM-CONTRACT-TITLE"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new String[] { Common.bundle.getString("MAIN_CONFIRM-OK"), 
                        Common.bundle.getString("MAIN_CONFIRM-NO")},
                    null);
            if(answer == JOptionPane.YES_OPTION) {
                saveContracts();
                getSelectedContract().getDBOperator().delete();
                updateContracts();
            }
        }
    }//GEN-LAST:event_removeContractButtonActionPerformed

    private void clientReminderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientReminderButtonActionPerformed
        NewReminderGUI.getInstance(dbOperator,ReminderType.CLIENT,
                client.getId()).setVisible(true);
    }//GEN-LAST:event_clientReminderButtonActionPerformed

    private void contractReminderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contractReminderButtonActionPerformed
        if(contractTable.getSelectedColumn() >= 0) {
            NewReminderGUI.getInstance(dbOperator,ReminderType.CONTRACT_END,
                    getSelectedContract().getId()).setVisible(true);
        }
    }//GEN-LAST:event_contractReminderButtonActionPerformed

    private void telephoneAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telephoneAddButtonActionPerformed
        if(!telephoneEdit.getText().isEmpty()) {
            ((DefaultListModel) telephoneList.getModel()).addElement(
                    telephoneEdit.getText());
            telephoneEdit.setText("");
        }
    }//GEN-LAST:event_telephoneAddButtonActionPerformed

    private void emailAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailAddButtonActionPerformed
        if(!emailEdit.getText().isEmpty()) {
            ((DefaultListModel) emailList.getModel()).addElement(
                emailEdit.getText());
            emailEdit.setText("");
        }
    }//GEN-LAST:event_emailAddButtonActionPerformed

    private void telephoneDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telephoneDeleteButtonActionPerformed
        int selectedIndex = telephoneList.getSelectedIndex();
        if(selectedIndex >= 0) {
            ((DefaultListModel) telephoneList.getModel()).remove(selectedIndex);
        }
    }//GEN-LAST:event_telephoneDeleteButtonActionPerformed

    private void emailDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailDeleteButtonActionPerformed
        int selectedIndex = emailList.getSelectedIndex();
        if(selectedIndex >= 0) {
            ((DefaultListModel) emailList.getModel()).remove(selectedIndex);
        }
    }//GEN-LAST:event_emailDeleteButtonActionPerformed

    private void manSexRadioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manSexRadioActionPerformed
    }//GEN-LAST:event_manSexRadioActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addContractButton;
    private javax.swing.JLabel ageLabel;
    private javax.swing.JButton applyButton;
    private javax.swing.JTextField bankAccountNumberEdit;
    private javax.swing.JTextField bankCodeEdit;
    private javax.swing.JTextArea clientNote;
    private javax.swing.JPanel clientPanel;
    private javax.swing.JButton clientReminderButton;
    private javax.swing.JPanel contractPanel;
    private javax.swing.JButton contractReminderButton;
    private javax.swing.JScrollPane contractScrollPane;
    private javax.swing.JTable contractTable;
    private javax.swing.JLabel dateOfBirthLabel;
    private javax.swing.JTextField doctorEdit;
    private javax.swing.JButton emailAddButton;
    private javax.swing.JButton emailDeleteButton;
    private javax.swing.JTextField emailEdit;
    private javax.swing.JList emailList;
    private javax.swing.ButtonGroup employGroup;
    private javax.swing.JRadioButton employedRadio;
    private javax.swing.JRadioButton entrepreneurRadio;
    private javax.swing.JLabel familyLabel;
    private javax.swing.JTextField identificationNumberEdit;
    private javax.swing.JTextField identityCardBirthPlaceEdit;
    private javax.swing.JTextField identityCardIssuerEdit;
    private javax.swing.JTextField identityCardNumberEdit;
    private com.toedter.calendar.JDateChooser identityCardValidDateChooser;
    private javax.swing.JSpinner incomeSpinner;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JRadioButton manSexRadio;
    private javax.swing.JTextField nameEdit;
    private javax.swing.JRadioButton notSportingRadio;
    private javax.swing.JTextField professionEdit;
    private javax.swing.JRadioButton professionalSportRadio;
    private javax.swing.JRadioButton recreationalSportRadio;
    private javax.swing.JButton removeContractButton;
    private javax.swing.ButtonGroup sexGroup;
    private javax.swing.ButtonGroup sportGroup;
    private javax.swing.JTextField sportTypeEdit;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JButton telephoneAddButton;
    private javax.swing.JButton telephoneDeleteButton;
    private javax.swing.JTextField telephoneEdit;
    private javax.swing.JList telephoneList;
    private javax.swing.JRadioButton tradesmanRadio;
    private javax.swing.JRadioButton unemployedRadio;
    private javax.swing.JRadioButton womanSexRadio;
    // End of variables declaration//GEN-END:variables
}
