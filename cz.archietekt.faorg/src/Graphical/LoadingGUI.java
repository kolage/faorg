package Graphical;

import Faorg.Common;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * Class used for displaying loading status of some data.
 * 
 * @author kolage
 */
public class LoadingGUI extends Thread {
    
    /**
     * Frame related to loading GUI.
     */
    private JFrame frame;
    
    /**
     * State defining final loading state number.
     */
    private final int maxState;
    
    /**
     * Progress bar displaying loading status.
     */
    private JProgressBar progressBar;
    
    /**
     * Label displaying information about loaded data.
     */
    private JLabel statusLabel;
    
    /**
     * Label representing loading caption and icon.
     */
    private JLabel captionLabel;
    
    /**
     * Creates instance of loading GUI.
     */
    public LoadingGUI() {
        maxState = 100;
    }
    
    /**
     * Hide GUI.
     */
    public void hide() {
        frame.dispose();
    }
    
    /**
     * Sets information text.
     * @param statusText status text
     */
    public void setText(String statusText) {
        statusLabel.setText(statusText);
    }
    
    /**
     * Switches to greater loading state number. If it is last state, 
     * loading GUI will be disposed.
     * 
     * @param currentState number of current state of loader
     */
    public void setState(int currentState) {
        progressBar.setValue(currentState);
        if(currentState == maxState)
            frame.dispose();
    }
    
    /**
     * Auxiliary method for creating loading GUI window and setting up captions
     * and so on ...
     */
    private void createShowGUI() {
        frame = new JFrame(Common.bundle.getString("LOADING_TITLE"));
        frame.setAlwaysOnTop(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        progressBar = new JProgressBar(0,maxState);
        statusLabel = new JLabel(Common.bundle.getString("LOADING_INFORMATION"));
        captionLabel = new JLabel(Common.bundle.getString("LOADING_LOADING"));
        
        captionLabel.setFont(new Font("Tahoma", 1, 14));
        captionLabel.setHorizontalAlignment(SwingConstants.CENTER);
        captionLabel.setIcon(new javax.swing.ImageIcon(
                getClass().getResource("/pictures/loader.gif")));
        
        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(captionLabel, GroupLayout.Alignment.TRAILING, 
            GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 
                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
            .addComponent(statusLabel, GroupLayout.Alignment.TRAILING, 
            GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(captionLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 22, 
                GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 30, 
                Short.MAX_VALUE)
                .addComponent(statusLabel))
        );
        
        progressBar.setMaximum(maxState);
        frame.pack();
        
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        synchronized(this) {
            this.notify();
        }
    }
    
    @Override
    public void run() {
        createShowGUI();
    }
}
