package Graphical;

import Database.Impl.ClientDBImpl;
import Database.Impl.ContractDBImpl;
import Database.Impl.DatabaseOperator;
import Database.Impl.FamilyDBImpl;
import Entity.Client;
import Entity.Contract;
import Entity.Entity;
import Entity.Family;
import Entity.Reminder;
import Faorg.Common;
import Faorg.ProcessThread;
import Print.PrintProcessor;
import static Print.PrintProcessor.OUTPUT_DIR;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 * Class representing main application window.
 * 
 * @author kolage
 */
public class MainGUI extends javax.swing.JFrame {
    
    /**
     * Using singleton GUI.
     */
    private static MainGUI myInstance;
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;
    
    /**
     * Main window table model displaying families from DB.
     */
    private DefaultTableModel familyModel;
    
    /**
     * Families fetched from DB.
     */
    private HashMap<Integer,Family> families;
    
    /**
     * Get instance of MainGUI. If it not exists, return null.
     * @return instance of MainGUI
     */
    public static MainGUI getInstance() {
        return myInstance;
    }
    
    /**
     * Method for creating singleton MainGUI object.
     *
     * @param dbOperator reference to DatabaseOperator
     * @return instance of main GUI
     */
    public static MainGUI getInstance(DatabaseOperator dbOperator) {
        if (myInstance == null) {
            myInstance = new MainGUI(dbOperator);
        }

        return myInstance;
    }
    
    /**
     * Creates new form MainGUI.
     */
    private MainGUI(DatabaseOperator dbOperator) {
        this.dbOperator = dbOperator;
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Update family hashset and family model after making changes in DB.
     */
    public void updateFamilies() {
        fetchFamilies();
    }
    
    /**
     * Method for initializing objects after successful logging in and before 
     * graphical elements are loaded.
     */
    private void preInit() {
        familyModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if(getRowCount() > 0) {
                    return getValueAt(0, columnIndex).getClass();
                } else {
                    return super.getColumnClass(columnIndex);
                }
            }
        };
        
        familyModel.setColumnIdentifiers(new String[] 
            {   
                Common.bundle.getString("MAIN_DB-ID"), 
                Common.bundle.getString("MAIN_DB-NAME"),
                Common.bundle.getString("MAIN_DB-CITY"),
                Common.bundle.getString("MAIN_DB-STREET"),
                Common.bundle.getString("MAIN_DB-ZIPCODE")
            });
        
        fetchFamilies();
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        familyTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    showEditFamilyWindow();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        
        familyTable.setRowSorter(new TableRowSorter<>(familyModel));
        
        if(dbOperator.isReadOnly()) {
            setTitle(getTitle() + " - " + Common.bundle.getString("MAIN_READ_ONLY"));
        }
    }
    
    /**
     * Get family selected in family table.
     * @return selected family
     */
    private Family getSelectedFamily() {
        int row = familyTable.getSelectedRow();
        Integer familyId = (Integer) familyTable.getValueAt(row, 0);
        return families.get(familyId);
    }
    
    /**
     * Will show FamilyGUI in editing mode.
     */
    private void showEditFamilyWindow() {
        Family family = getSelectedFamily();
        FamilyGUI.getInstance(dbOperator,family).setVisible(true);
    }
    
    /**
     * Fetch families from DB and update table model.
     */
    private void fetchFamilies() {
        // clear family table
        familyModel.setRowCount(0);
        
        families = new HashMap<>();
        List<? extends Family> familyList = new FamilyDBImpl(dbOperator).get();
        for(Family family : familyList) {
            families.put(family.getId(), family);
            familyModel.addRow(new Object[] 
                {
                    family.getId(),
                    family.getName(),
                    family.getAddress().getCity(),
                    family.getAddress().getStreet(),
                    family.getAddress().getZipCode()
                });
        }
    }
    
    /**
     * Disconnect database connections and exit application.
     */
    private void exitApplication() {
        int answer = JOptionPane.showConfirmDialog(null, 
                Common.bundle.getString("APP_SHUTDOWN"),
                Common.bundle.getString("APP_SHUTDOWN-TITLE"),
                JOptionPane.YES_NO_OPTION);
        if(answer == JOptionPane.YES_OPTION) {
            dbOperator.closeConnections();
            this.dispose();
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        familyTable = new javax.swing.JTable();
        addFamilyButton = new javax.swing.JButton();
        editFamilyButton = new javax.swing.JButton();
        deleteFamilyButton = new javax.swing.JButton();
        reminderButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        updateItem = new javax.swing.JMenuItem();
        backupItem = new javax.swing.JMenuItem();
        generatedDataItem = new javax.swing.JMenuItem();
        exitItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        searchItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        familyListItem = new javax.swing.JMenuItem();
        clientListItem = new javax.swing.JMenuItem();
        contractListItem = new javax.swing.JMenuItem();
        reminderMenu = new javax.swing.JMenu();
        showRemindersItem = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        settingsItem = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        aboutItem = new javax.swing.JMenuItem();
        licenseItem = new javax.swing.JMenuItem();
        adviserEditItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("APP_NAME")); // NOI18N
        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        layout.rowHeights = new int[] {0, 12, 0, 12, 0};
        getContentPane().setLayout(layout);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(650, 500));

        familyTable.setModel(familyModel);
        familyTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        familyTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(familyTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jScrollPane1, gridBagConstraints);

        addFamilyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/add.png"))); // NOI18N
        addFamilyButton.setText(bundle.getString("MAIN_LABEL-ADDFAMILY")); // NOI18N
        addFamilyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addFamilyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(addFamilyButton, gridBagConstraints);

        editFamilyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/edit.png"))); // NOI18N
        editFamilyButton.setText(bundle.getString("MAIN_LABEL-EDITFAMILY")); // NOI18N
        editFamilyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editFamilyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(editFamilyButton, gridBagConstraints);

        deleteFamilyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/delete.png"))); // NOI18N
        deleteFamilyButton.setText(bundle.getString("MAIN_LABEL-DELETEFAMILY")); // NOI18N
        deleteFamilyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteFamilyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(deleteFamilyButton, gridBagConstraints);

        reminderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminder.png"))); // NOI18N
        reminderButton.setText(bundle.getString("CLIENT_BUTTON-REMINDER")); // NOI18N
        reminderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reminderButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 13, 0, 13);
        getContentPane().add(reminderButton, gridBagConstraints);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/dataSmall.png"))); // NOI18N
        jMenu1.setText(bundle.getString("MAIN_MENU-DATA")); // NOI18N

        updateItem.setText(bundle.getString("MAIN_MENU-DATA-REFRESH")); // NOI18N
        updateItem.setToolTipText(bundle.getString("MAIN_MENU-DATA-REFRESH_TOOLTIP")); // NOI18N
        updateItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateItemActionPerformed(evt);
            }
        });
        jMenu1.add(updateItem);

        backupItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        backupItem.setText(bundle.getString("MAIN_MENU-DATA-BACKUP")); // NOI18N
        backupItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backupItemActionPerformed(evt);
            }
        });
        jMenu1.add(backupItem);

        generatedDataItem.setText(bundle.getString("MAIN_MENU-DATA-SHOW-DATA-FOLDER")); // NOI18N
        generatedDataItem.setToolTipText(bundle.getString("MAIN_MENU-DATA-SHOW-DATA-FOLDER-TOOLTIP")); // NOI18N
        generatedDataItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generatedDataItemActionPerformed(evt);
            }
        });
        jMenu1.add(generatedDataItem);

        exitItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitItem.setText(bundle.getString("MAIN_MENU-DATA-EXIT")); // NOI18N
        exitItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitItemActionPerformed(evt);
            }
        });
        jMenu1.add(exitItem);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/searchSmall.png"))); // NOI18N
        jMenu2.setText(bundle.getString("MAIN_MENU-FIND")); // NOI18N

        searchItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        searchItem.setText(bundle.getString("MAIN_MENU-FIND-ITEM")); // NOI18N
        searchItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchItemActionPerformed(evt);
            }
        });
        jMenu2.add(searchItem);

        jMenuBar1.add(jMenu2);

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/printSmall.png"))); // NOI18N
        jMenu3.setText(bundle.getString("MAIN_MENU-OUTPUT")); // NOI18N

        familyListItem.setText(bundle.getString("MAIN_MENU-OUTPUT-FAMILYLIST")); // NOI18N
        familyListItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                familyListItemActionPerformed(evt);
            }
        });
        jMenu3.add(familyListItem);

        clientListItem.setText(bundle.getString("MAIN_MENU-OUTPUT-CLIENTLIST")); // NOI18N
        clientListItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientListItemActionPerformed(evt);
            }
        });
        jMenu3.add(clientListItem);

        contractListItem.setText(bundle.getString("MAIN_MENU-OUTPUT-CONTRACTLIST")); // NOI18N
        contractListItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contractListItemActionPerformed(evt);
            }
        });
        jMenu3.add(contractListItem);

        jMenuBar1.add(jMenu3);

        reminderMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminderSmall.png"))); // NOI18N
        reminderMenu.setText(bundle.getString("MAIN_MENU-REMINDERS")); // NOI18N

        showRemindersItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        showRemindersItem.setText(bundle.getString("MAIN_MENU-REMINDERS-SHOW")); // NOI18N
        showRemindersItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showRemindersItemActionPerformed(evt);
            }
        });
        reminderMenu.add(showRemindersItem);

        jMenuBar1.add(reminderMenu);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/settingsSmall.png"))); // NOI18N
        jMenu5.setText(bundle.getString("MAIN_MENU-SETTINGS")); // NOI18N

        settingsItem.setText(bundle.getString("MAIN_MENU-SETTINGS-SHOW")); // NOI18N
        settingsItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingsItemActionPerformed(evt);
            }
        });
        jMenu5.add(settingsItem);

        jMenuBar1.add(jMenu5);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/infoSmall.png"))); // NOI18N
        jMenu6.setText(bundle.getString("MAIN_MENU-INFO")); // NOI18N

        aboutItem.setText(bundle.getString("MAIN_MENU-INFO-ABOUT")); // NOI18N
        aboutItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutItemActionPerformed(evt);
            }
        });
        jMenu6.add(aboutItem);

        licenseItem.setText(bundle.getString("MAIN_MENU-INFO-LICENSE")); // NOI18N
        licenseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                licenseItemActionPerformed(evt);
            }
        });
        jMenu6.add(licenseItem);

        adviserEditItem.setText(bundle.getString("MAIN_MENU-INFO-ADVISER")); // NOI18N
        adviserEditItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adviserEditItemActionPerformed(evt);
            }
        });
        jMenu6.add(adviserEditItem);

        jMenuBar1.add(jMenu6);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void addFamilyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFamilyButtonActionPerformed
        FamilyGUI.getInstance(dbOperator,null).setVisible(true);
    }//GEN-LAST:event_addFamilyButtonActionPerformed

    private void editFamilyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editFamilyButtonActionPerformed
        if(familyTable.getSelectedRow() >= 0) {
            showEditFamilyWindow();
        }
    }//GEN-LAST:event_editFamilyButtonActionPerformed

    private void deleteFamilyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteFamilyButtonActionPerformed
        if(familyTable.getSelectedRow() >= 0) {
            int answer = JOptionPane.showOptionDialog(
                    null,
                    Common.bundle.getString("MAIN_CONFIRM-FAMILY-DELETE"),
                    Common.bundle.getString("MAIN_CONFIRM-TITLE"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new String[] { Common.bundle.getString("MAIN_CONFIRM-OK"), 
                        Common.bundle.getString("MAIN_CONFIRM-NO")},
                    null);
            if(answer == JOptionPane.YES_OPTION) {
                getSelectedFamily().getDBOperator().delete();
                updateFamilies();
            }
        }
    }//GEN-LAST:event_deleteFamilyButtonActionPerformed

    private void reminderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reminderButtonActionPerformed
        if(familyTable.getSelectedRow() >= 0) {
            Family family = getSelectedFamily();
            NewReminderGUI.getInstance(dbOperator,Reminder.ReminderType.FAMILY,family.getId()).setVisible(true);
        } else {
            NewReminderGUI.getInstance(dbOperator,Reminder.ReminderType.OTHER,null).setVisible(true);
        }
    }//GEN-LAST:event_reminderButtonActionPerformed

    private void showRemindersItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showRemindersItemActionPerformed
        EntityListGUI.getInstance(dbOperator, Reminder.class, dbOperator.getCurrentReminders(),
                false).setVisible(true);
    }//GEN-LAST:event_showRemindersItemActionPerformed

    private void familyListItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_familyListItemActionPerformed
        EntityListGUI.getInstance(dbOperator, Family.class, new FamilyDBImpl(dbOperator).get(),
                false).setVisible(true);
    }//GEN-LAST:event_familyListItemActionPerformed

    private void clientListItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientListItemActionPerformed
        EntityListGUI.getInstance(dbOperator, Client.class, new ClientDBImpl(dbOperator).get(),
                false).setVisible(true);
    }//GEN-LAST:event_clientListItemActionPerformed

    private void contractListItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contractListItemActionPerformed
        EntityListGUI.getInstance(dbOperator, Contract.class, new ContractDBImpl(dbOperator).get(),
                false).setVisible(true);
    }//GEN-LAST:event_contractListItemActionPerformed

    private void settingsItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingsItemActionPerformed
        OptionsGUI.getInstance().setVisible(true);
    }//GEN-LAST:event_settingsItemActionPerformed

    private void searchItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchItemActionPerformed
        SearchGUI.getInstance(dbOperator).setVisible(true);
    }//GEN-LAST:event_searchItemActionPerformed

    private void exitItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitItemActionPerformed
        exitApplication();
    }//GEN-LAST:event_exitItemActionPerformed

    private void aboutItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutItemActionPerformed
        AboutGUI.getInstance(dbOperator).setVisible(true);
    }//GEN-LAST:event_aboutItemActionPerformed

    private void backupItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backupItemActionPerformed
        String backupFilename = DatabaseOperator.localDatabaseLocation
                + (new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss")).
                format(new Date()) + "--" + dbOperator.getLogin();
        try {
            Files.copy(Paths.get(DatabaseOperator.localDatabaseLocation), Paths.get(backupFilename));
            JOptionPane.showMessageDialog(null, Common.bundle.getString("MAIN_BACKUP-OK"));
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }//GEN-LAST:event_backupItemActionPerformed

    private void licenseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_licenseItemActionPerformed
        LicenseKeyGUI.getInstance(dbOperator).setVisible(true);
    }//GEN-LAST:event_licenseItemActionPerformed

    private void adviserEditItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adviserEditItemActionPerformed
        AdviserInfoGUI.getInstance(dbOperator).setVisible(true);
    }//GEN-LAST:event_adviserEditItemActionPerformed

    private void generatedDataItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generatedDataItemActionPerformed
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.OPEN)) {
                try {
                    desktop.open(new File(PrintProcessor.getGeneratedDataOutputFolder()));
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            } else {
                PrintProcessor.showSavedDialog(OUTPUT_DIR, Common.bundle.getString("FOLDER_LOCATED_IN"));
            }
        } else {
            PrintProcessor.showSavedDialog(OUTPUT_DIR, Common.bundle.getString("FOLDER_LOCATED_IN"));
        }
    }//GEN-LAST:event_generatedDataItemActionPerformed

    private void updateItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateItemActionPerformed
        new ProcessThread() {
            @Override
            public void process() {
                dbOperator.updateLocalDatabaseProcess(this);
            }

            @Override
            public void done() {
                updateFamilies();
            }
        }.execute();
    }//GEN-LAST:event_updateItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutItem;
    private javax.swing.JButton addFamilyButton;
    private javax.swing.JMenuItem adviserEditItem;
    private javax.swing.JMenuItem backupItem;
    private javax.swing.JMenuItem clientListItem;
    private javax.swing.JMenuItem contractListItem;
    private javax.swing.JButton deleteFamilyButton;
    private javax.swing.JButton editFamilyButton;
    private javax.swing.JMenuItem exitItem;
    private javax.swing.JMenuItem familyListItem;
    private javax.swing.JTable familyTable;
    private javax.swing.JMenuItem generatedDataItem;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem licenseItem;
    private javax.swing.JButton reminderButton;
    private javax.swing.JMenu reminderMenu;
    private javax.swing.JMenuItem searchItem;
    private javax.swing.JMenuItem settingsItem;
    private javax.swing.JMenuItem showRemindersItem;
    private javax.swing.JMenuItem updateItem;
    // End of variables declaration//GEN-END:variables
}
