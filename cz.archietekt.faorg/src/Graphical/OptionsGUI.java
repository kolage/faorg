package Graphical;

import Faorg.Common;
import Faorg.StringPair;
import java.awt.event.ItemEvent;
import java.util.Calendar;
import javax.swing.SpinnerNumberModel;

/**
 * Class representing window for setting up various application preferences.
 * 
 * @author kolage
 */
public class OptionsGUI extends javax.swing.JFrame {

    /**
     * Using singleton GUI.
     */
    private static OptionsGUI myInstance;
    
    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        OptionsGUI.myInstance = null;
    }
    
    /**
     * Method for creating singleton OptionsGUI object.
     * 
     * @return instance of options GUI
     */
    public static OptionsGUI getInstance() {
        if (myInstance == null)
            myInstance = new OptionsGUI();
        return myInstance;
    }
    
    /**
     * Creates new form OptionsGUI.
     */
    private OptionsGUI() {
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Method for initializing objects before graphical elements are loaded.
     */
    private void preInit() {
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        birthdayUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.DAY_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-DAY")));
        birthdayUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.WEEK_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-WEEK")));
        birthdayUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-MONTH")));
        
        contractUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.DAY_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-DAY")));
        contractUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.WEEK_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-WEEK")));
        contractUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-MONTH")));
        
        contractAnniversaryUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.DAY_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-DAY")));
        contractAnniversaryUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.WEEK_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-WEEK")));
        contractAnniversaryUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-MONTH")));
        
        lastVisitUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.DAY_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-DAY")));
        lastVisitUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.WEEK_OF_MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-WEEK")));
        lastVisitUnitCombo.addItem(new StringPair(
                String.valueOf(Calendar.MONTH),
                Common.bundle.getString("NEW_REMINDER-DATE-UNIT-MONTH")));
        
        birthdayUnitSpinner.setModel(new SpinnerNumberModel(1, 0, 30, 1));
        contractUnitSpinner.setModel(new SpinnerNumberModel(1, 0, 30, 1));
        contractAnniversaryUnitSpinner.setModel(new SpinnerNumberModel(1, 0, 30, 1));
        lastVisitUnitSpinner.setModel(new SpinnerNumberModel(1, 0, 30, 1));
        
        loadPreferencesToForm();
    }
    
    /**
     * Loads stored preferences into form.
     */
    private void loadPreferencesToForm() {
        // application tab
        
        // database tab
        
        // reminder tab
        
        //// birthday
        boolean createBirthdayReminders =
                Common.getPreferences().getBoolean(
                Common.CREATE_BIRTHDAY_REMINDER_PREFERENCE,
                false);
        
        userBirthdayDefaultCheckbox.setSelected(createBirthdayReminders);
        
        if(createBirthdayReminders) {
            birthdayUnitSpinner.setValue(
                    Common.getPreferences().getInt(
                    Common.BIRTHDAY_UNIT_COUNT_PREFERENCE, 
                    1));
            int birthdayUnit =
                    Common.getPreferences().getInt(
                    Common.BIRTHDAY_UNIT_PREFERENCE,
                    Calendar.DATE);
            for (int i = 0; i < birthdayUnitCombo.getItemCount(); i++) {
                StringPair pair = (StringPair) birthdayUnitCombo.getItemAt(i);
                if (pair.getKey().equals(String.valueOf(birthdayUnit))) {
                    birthdayUnitCombo.setSelectedIndex(i);
                    break;
                }
            }
        } else {
            birthdayUnitSpinner.setEnabled(false);
            birthdayUnitCombo.setEnabled(false);
        }
        
        //// contract
        boolean createContractReminders =
                Common.getPreferences().getBoolean(
                Common.CREATE_CONTRACT_REMINDER_PREFERENCE,
                false);
        
        userContractDefaultCheckbox.setSelected(createContractReminders);
        
        if(createContractReminders) {
            contractUnitSpinner.setValue(
                    Common.getPreferences().getInt(
                    Common.CONTRACT_UNIT_COUNT_PREFERENCE,
                    1));
            int contractUnit =
                    Common.getPreferences().getInt(
                    Common.CONTRACT_UNIT_PREFERENCE,
                    Calendar.DATE);
            for (int i = 0; i < contractUnitCombo.getItemCount(); i++) {
                StringPair pair = (StringPair) contractUnitCombo.getItemAt(i);
                if (pair.getKey().equals(String.valueOf(contractUnit))) {
                    contractUnitCombo.setSelectedIndex(i);
                    break;
                }
            }
        } else {
            contractUnitSpinner.setEnabled(false);
            contractUnitCombo.setEnabled(false);
        }
        
        //// contract anniversary
        boolean createContractAnniversaryReminders =
                Common.getPreferences().getBoolean(
                Common.CREATE_CONTRACT_ANNIVERSARY_REMINDER_PREFERENCE,
                false);
        
        contractAnniversaryDefaultCheckbox.setSelected(createContractAnniversaryReminders);
        
        if(createContractAnniversaryReminders) {
            contractAnniversaryUnitSpinner.setValue(
                    Common.getPreferences().getInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_COUNT_PREFERENCE,
                    1));
            int contractAnniversaryUnit =
                    Common.getPreferences().getInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_PREFERENCE,
                    Calendar.DATE);
            for (int i = 0; i < contractAnniversaryUnitCombo.getItemCount(); i++) {
                StringPair pair = (StringPair) contractAnniversaryUnitCombo.getItemAt(i);
                if (pair.getKey().equals(String.valueOf(contractAnniversaryUnit))) {
                    contractAnniversaryUnitCombo.setSelectedIndex(i);
                    break;
                }
            }
        } else {
            contractAnniversaryUnitSpinner.setEnabled(false);
            contractAnniversaryUnitCombo.setEnabled(false);
        }
        
        //// last visit
        boolean createLastVisitReminders =
                Common.getPreferences().getBoolean(
                Common.CREATE_LASTVISIT_REMINDER_PREFERENCE,
                false);
        
        userVisitDefaultCheckbox.setSelected(createLastVisitReminders);
        
        if(createLastVisitReminders) {
            lastVisitUnitSpinner.setValue(
                    Common.getPreferences().getInt(
                    Common.LASTVISIT_UNIT_COUNT_PREFERENCE,
                    1));
            int lastVisitUnit =
                    Common.getPreferences().getInt(
                    Common.LASTVISIT_UNIT_PREFERENCE,
                    Calendar.DATE);
            for (int i = 0; i < lastVisitUnitCombo.getItemCount(); i++) {
                StringPair pair = (StringPair) lastVisitUnitCombo.getItemAt(i);
                if (pair.getKey().equals(String.valueOf(lastVisitUnit))) {
                    lastVisitUnitCombo.setSelectedIndex(i);
                    break;
                }
            }
        } else {
            lastVisitUnitSpinner.setEnabled(false);
            lastVisitUnitCombo.setEnabled(false);
        }
    }
    
    /**
     * Stores preferences from form into preferences object.
     */
    private void savePreferencesFromForm() {
        // application tab
        
        // database tab
        
        // reminder tab
        
        //// birthday
        boolean createBirthdayReminders = 
                userBirthdayDefaultCheckbox.isSelected();
        
        Common.getPreferences().putBoolean(
                Common.CREATE_BIRTHDAY_REMINDER_PREFERENCE,
                createBirthdayReminders);
        
        if(createBirthdayReminders) {
            Common.getPreferences().putInt(
                    Common.BIRTHDAY_UNIT_COUNT_PREFERENCE,
                    (Integer) birthdayUnitSpinner.getValue());
            
            Common.getPreferences().putInt(
                    Common.BIRTHDAY_UNIT_PREFERENCE,
                    Integer.parseInt(((StringPair) birthdayUnitCombo.
                    getSelectedItem()).
                    getKey()));
        }
        
        //// contract
        boolean createContractReminders = 
                userContractDefaultCheckbox.isSelected();
        
        Common.getPreferences().putBoolean(
                Common.CREATE_CONTRACT_REMINDER_PREFERENCE,
                createContractReminders);
        
        if(createContractReminders) {
            Common.getPreferences().putInt(
                    Common.CONTRACT_UNIT_COUNT_PREFERENCE,
                    (Integer) contractUnitSpinner.getValue());
            
            Common.getPreferences().putInt(
                    Common.CONTRACT_UNIT_PREFERENCE,
                    Integer.parseInt(((StringPair) contractUnitCombo.
                    getSelectedItem()).
                    getKey()));
        }
        
        //// contract anniversary
        boolean createContractAnniversaryReminders = 
                contractAnniversaryDefaultCheckbox.isSelected();
        
        Common.getPreferences().putBoolean(
                Common.CREATE_CONTRACT_ANNIVERSARY_REMINDER_PREFERENCE,
                createContractAnniversaryReminders);
        
        if(createContractAnniversaryReminders) {
            Common.getPreferences().putInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_COUNT_PREFERENCE,
                    (Integer) contractAnniversaryUnitSpinner.getValue());
            
            Common.getPreferences().putInt(
                    Common.CONTRACT_ANNIVERSARY_UNIT_PREFERENCE,
                    Integer.parseInt(((StringPair) contractAnniversaryUnitCombo.
                    getSelectedItem()).
                    getKey()));
        }
        
        //// last visit
        boolean createLastVisitReminders = 
                userVisitDefaultCheckbox.isSelected();
        
        Common.getPreferences().putBoolean(
                Common.CREATE_LASTVISIT_REMINDER_PREFERENCE,
                createLastVisitReminders);
        
        if(createLastVisitReminders) {
            Common.getPreferences().putInt(
                    Common.LASTVISIT_UNIT_COUNT_PREFERENCE,
                    (Integer) lastVisitUnitSpinner.getValue());
            
            Common.getPreferences().putInt(
                    Common.LASTVISIT_UNIT_PREFERENCE,
                    Integer.parseInt(((StringPair) lastVisitUnitCombo.
                    getSelectedItem()).
                    getKey()));
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPane1 = new javax.swing.JTabbedPane();
        reminderTab = new javax.swing.JPanel();
        birthdayReminderLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        userBirthdayDefaultCheckbox = new javax.swing.JCheckBox();
        contractReminderLabel = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        userContractDefaultCheckbox = new javax.swing.JCheckBox();
        birthdayUnitSpinner = new javax.swing.JSpinner();
        birthdayUnitCombo = new javax.swing.JComboBox();
        contractUnitCombo = new javax.swing.JComboBox();
        contractUnitSpinner = new javax.swing.JSpinner();
        lastVisitReminderLabel = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        userVisitDefaultCheckbox = new javax.swing.JCheckBox();
        lastVisitUnitSpinner = new javax.swing.JSpinner();
        lastVisitUnitCombo = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        contractAnniversaryReminderLabel = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        contractAnniversaryDefaultCheckbox = new javax.swing.JCheckBox();
        contractAnniversaryUnitSpinner = new javax.swing.JSpinner();
        contractAnniversaryUnitCombo = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        applyButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("OPTIONS_TITLE")); // NOI18N
        setResizable(false);
        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0};
        getContentPane().setLayout(layout);

        reminderTab.setPreferredSize(new java.awt.Dimension(220, 400));
        java.awt.GridBagLayout reminderTabLayout = new java.awt.GridBagLayout();
        reminderTabLayout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        reminderTabLayout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        reminderTab.setLayout(reminderTabLayout);

        birthdayReminderLabel.setText(bundle.getString("REMINDER-BIRTHDAY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(birthdayReminderLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        reminderTab.add(jSeparator1, gridBagConstraints);

        userBirthdayDefaultCheckbox.setText(bundle.getString("OPTIONS_REMINDER-BIRTHDAYUSER")); // NOI18N
        userBirthdayDefaultCheckbox.setToolTipText(bundle.getString("OPTIONS_REMINDER-BIRTHDAYUSER-TOOLTIP")); // NOI18N
        userBirthdayDefaultCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                userBirthdayDefaultCheckboxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(userBirthdayDefaultCheckbox, gridBagConstraints);

        contractReminderLabel.setText(bundle.getString("OPTIONS_REMINDER-CONTRACTS")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractReminderLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        reminderTab.add(jSeparator2, gridBagConstraints);

        userContractDefaultCheckbox.setText(bundle.getString("OPTIONS_REMINDER-CONTRACTUSER")); // NOI18N
        userContractDefaultCheckbox.setToolTipText(bundle.getString("OPTIONS_REMINDER-CONTRACTUSER-TOOLTIP")); // NOI18N
        userContractDefaultCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                userContractDefaultCheckboxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(userContractDefaultCheckbox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(birthdayUnitSpinner, gridBagConstraints);

        birthdayUnitCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(birthdayUnitCombo, gridBagConstraints);

        contractUnitCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractUnitCombo, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractUnitSpinner, gridBagConstraints);

        lastVisitReminderLabel.setText(bundle.getString("OPTIONS_REMINDER-LAST-VISIT")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 32;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(lastVisitReminderLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 34;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        reminderTab.add(jSeparator3, gridBagConstraints);

        userVisitDefaultCheckbox.setText(bundle.getString("OPTIONS_REMINDER-LASTVISITUSER")); // NOI18N
        userVisitDefaultCheckbox.setToolTipText(bundle.getString("OPTIONS_REMINDER-LASTVISITUSER-TOOLTIP")); // NOI18N
        userVisitDefaultCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                userVisitDefaultCheckboxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 36;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(userVisitDefaultCheckbox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 38;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(lastVisitUnitSpinner, gridBagConstraints);

        lastVisitUnitCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 38;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(lastVisitUnitCombo, gridBagConstraints);

        jLabel1.setText(bundle.getString("NEW_REMINDER-DATE-UNIT-BEFORE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(jLabel1, gridBagConstraints);

        jLabel2.setText(bundle.getString("NEW_REMINDER-DATE-UNIT-BEFORE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(jLabel2, gridBagConstraints);

        jLabel3.setText(bundle.getString("NEW_REMINDER-DATE-UNIT-AFTER")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 38;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 8, 0);
        reminderTab.add(filler1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 8, 0);
        reminderTab.add(filler2, gridBagConstraints);

        contractAnniversaryReminderLabel.setText(bundle.getString("OPTIONS_REMINDER-CONTRACT_ANNIVERSARY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractAnniversaryReminderLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        reminderTab.add(jSeparator4, gridBagConstraints);

        contractAnniversaryDefaultCheckbox.setText(bundle.getString("OPTIONS_REMINDER-LASTVISITUSER")); // NOI18N
        contractAnniversaryDefaultCheckbox.setToolTipText(bundle.getString("OPTIONS_REMINDER-CONTRACTANNIVERSARY-TOOLTIP")); // NOI18N
        contractAnniversaryDefaultCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                contractAnniversaryDefaultCheckboxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractAnniversaryDefaultCheckbox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractAnniversaryUnitSpinner, gridBagConstraints);

        contractAnniversaryUnitCombo.setPreferredSize(new java.awt.Dimension(75, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(contractAnniversaryUnitCombo, gridBagConstraints);

        jLabel4.setText(bundle.getString("NEW_REMINDER-DATE-UNIT-BEFORE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        reminderTab.add(jLabel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 30;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 8, 0);
        reminderTab.add(filler3, gridBagConstraints);

        jTabbedPane1.addTab(bundle.getString("OPTIONS_REMINDER-TITLE"), reminderTab); // NOI18N

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jTabbedPane1, gridBagConstraints);

        applyButton.setFont(applyButton.getFont().deriveFont(applyButton.getFont().getStyle() | java.awt.Font.BOLD));
        applyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        applyButton.setText(bundle.getString("CLIENT_BUTTON-OK")); // NOI18N
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(applyButton, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        savePreferencesFromForm();
        this.dispose();
    }//GEN-LAST:event_applyButtonActionPerformed

    private void userContractDefaultCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_userContractDefaultCheckboxItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            contractUnitCombo.setEnabled(true);
            contractUnitSpinner.setEnabled(true);
        } else {
            contractUnitCombo.setEnabled(false);
            contractUnitSpinner.setEnabled(false);
        }
    }//GEN-LAST:event_userContractDefaultCheckboxItemStateChanged

    private void userBirthdayDefaultCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_userBirthdayDefaultCheckboxItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            birthdayUnitCombo.setEnabled(true);
            birthdayUnitSpinner.setEnabled(true);
        } else {
            birthdayUnitCombo.setEnabled(false);
            birthdayUnitSpinner.setEnabled(false);
        }
    }//GEN-LAST:event_userBirthdayDefaultCheckboxItemStateChanged

    private void userVisitDefaultCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_userVisitDefaultCheckboxItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            lastVisitUnitCombo.setEnabled(true);
            lastVisitUnitSpinner.setEnabled(true);
        } else {
            lastVisitUnitCombo.setEnabled(false);
            lastVisitUnitSpinner.setEnabled(false);
        }
    }//GEN-LAST:event_userVisitDefaultCheckboxItemStateChanged

    private void contractAnniversaryDefaultCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_contractAnniversaryDefaultCheckboxItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            contractAnniversaryUnitCombo.setEnabled(true);
            contractAnniversaryUnitSpinner.setEnabled(true);
        } else {
            contractAnniversaryUnitCombo.setEnabled(false);
            contractAnniversaryUnitSpinner.setEnabled(false);
        }
    }//GEN-LAST:event_contractAnniversaryDefaultCheckboxItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private javax.swing.JLabel birthdayReminderLabel;
    private javax.swing.JComboBox birthdayUnitCombo;
    private javax.swing.JSpinner birthdayUnitSpinner;
    private javax.swing.JCheckBox contractAnniversaryDefaultCheckbox;
    private javax.swing.JLabel contractAnniversaryReminderLabel;
    private javax.swing.JComboBox contractAnniversaryUnitCombo;
    private javax.swing.JSpinner contractAnniversaryUnitSpinner;
    private javax.swing.JLabel contractReminderLabel;
    private javax.swing.JComboBox contractUnitCombo;
    private javax.swing.JSpinner contractUnitSpinner;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lastVisitReminderLabel;
    private javax.swing.JComboBox lastVisitUnitCombo;
    private javax.swing.JSpinner lastVisitUnitSpinner;
    private javax.swing.JPanel reminderTab;
    private javax.swing.JCheckBox userBirthdayDefaultCheckbox;
    private javax.swing.JCheckBox userContractDefaultCheckbox;
    private javax.swing.JCheckBox userVisitDefaultCheckbox;
    // End of variables declaration//GEN-END:variables
}
