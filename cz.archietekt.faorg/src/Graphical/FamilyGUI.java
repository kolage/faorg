package Graphical;

import Database.Impl.DatabaseOperator;
import Entity.Client;
import Entity.Family;
import Entity.Family.FamilyLevel;
import Entity.Family.FamilyProperty;
import Entity.Family.HouseType;
import Entity.Impl.FamilyImpl;
import Entity.Reminder;
import Faorg.Address;
import Faorg.Common;
import Print.PrintProcessor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * Class representing family window. It is used for creating, editing and
 * displaying info about families and creating and editing clients.
 * 
 * @author kolage
 */
public class FamilyGUI extends javax.swing.JFrame {

    /**
     * Using singleton GUI.
     */
    private static FamilyGUI myInstance;
    
    /**
     * FamilyGUI mode.
     */
    public enum EditMode {
        EDIT, NEW
    }
    
    /**
     * Reference to database operator.
     */
    private final DatabaseOperator dbOperator;
    
    /**
     * Table model displaying members of family from DB.
     */
    private DefaultTableModel clientListModel;
    
    /**
     * Family that is being operated with.
     */
    private Family family;
    
    /**
     * Editing mode.
     */
    private final EditMode mode;
    
    /**
     * Clients fetched from DB.
     */
    private HashMap<Integer,Client> clients;
    
    /**
     * Overridden dispose method for destroying instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        FamilyGUI.myInstance = null;
    }
    
    /**
     * Get instance of FamilyGUI. If it not exists, return null.
     * @return instance of FamilyGUI
     */
    public static FamilyGUI getInstance() {
        return myInstance;
    }
    
    /**
     * Method for creating singleton FamilyGUI object.
     * @param dbOperator reference to DatabaseOperator
     * @param family relevant family
     * @return instance of family GUI
     */
    public static FamilyGUI getInstance(DatabaseOperator dbOperator, 
            Family family) {
        if (myInstance == null)
            myInstance = new FamilyGUI(dbOperator, family);

        return myInstance;
    }
    
    /**
     * Creates new form FamilyGUI.
     */
    private FamilyGUI(DatabaseOperator dbOperator, Family family) {
        this.dbOperator = dbOperator;
        this.family = family;
        
        if(family == null) {
            mode = EditMode.NEW;
        } else {
            mode = EditMode.EDIT;
        }
        
        preInit();
        initComponents();
        postInit();
    }
    
    /**
     * Update clients hashset and client list model after making changes in DB.
     */
    public void updateClients() {
        fetchClients();
    }
    
    /**
     * Method for initializing objects before graphical elements are loaded.
     */
    private void preInit() {
        clientListModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if(getRowCount() > 0) {
                    return getValueAt(0, columnIndex).getClass();
                } else {
                    return super.getColumnClass(columnIndex);
                }
            }
        };
        
        clientListModel.setColumnIdentifiers(new String[] 
            {   Common.bundle.getString("FAMILY_DB-CLIENT-ID"), 
                Common.bundle.getString("FAMILY_DB-CLIENT-NAME"),
                Common.bundle.getString("FAMILY_DB-CLIENT-IN"),
                Common.bundle.getString("FAMILY_DB-CLIENT-BIRTHDATE")
            });
        
        if(mode == EditMode.NEW) {
            family = new FamilyImpl(dbOperator,dbOperator.generateNewId("family"));
        }
        
        fetchClients();
    }
    
    /**
     * Method for initializing objects after graphical elements are loaded.
     */
    private void postInit() {
        if(mode == EditMode.EDIT) {
            insertFamilyAttributesFromObject();
            setTitle(family.getName());
        } else {
            setTitle(Common.bundle.getString("FAMILY_TITLE-NEW"));
        }
        
        clientListTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    showEditClientWindow();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        
        lastVisitDateChooser.setLocale(new Locale(Common.LOCALE_STRING));
    }
    
    /**
     * Method will fetch written and selected family attributes and stores it
     * in family object.
     */
    private void loadFamilyAttributesFromForm() {
        family.setName(nameEdit.getText());
        
        family.setAddress(new Address(
                cityEdit.getText(),
                streetEdit.getText(),
                zipCodeEdit.getText()));
        
        family.setPostalAddress(new Address(
                postalCityEdit.getText(),
                postalStreetEdit.getText(),
                postalZipCodeEdit.getText()));
        
        // fetch family level selection
        FamilyLevel level = FamilyLevel.BAD;
        
        if(standardLevelRadio.isSelected()) {
            level = FamilyLevel.STANDARD;
        } else if(goodLevelRadio.isSelected()) {
            level = FamilyLevel.GOOD;
        } else if(perfectLevelRadio.isSelected()) {
            level = FamilyLevel.PERFECT;
        }
        
        family.setFamilyLevel(level);
        
        // fetch family property selection
        FamilyProperty property = FamilyProperty.NO_CAR;
        
        if(oneCarPropertyRadio.isSelected()) {
            property = FamilyProperty.ONE_CAR;
        } else if(twoCarsPropertyRadio.isSelected()) {
            property = FamilyProperty.TWO_CARS;
        } else if(moreCarsPropertyRadio.isSelected()) {
            property = FamilyProperty.MORE_CARS;
        }
        
        family.setFamilyProperty(property);
        
        // fetch family house type selection
        HouseType houseType = HouseType.FLAT_HIRE;
        
        if(flatOwnHouseRadio.isSelected()) {
            houseType = HouseType.FLAT_OWN;
        } else if(semidetachedHouseRadio.isSelected()) {
            houseType = HouseType.SEMI_DETACHED;
        } else if(familyHouseRadio.isSelected()) {
            houseType = HouseType.HOUSE;
        }
        
        family.setHouseType(houseType);
        
        family.setLastVisit(lastVisitDateChooser.getDate());
        
        family.setEnvelopeTitle((String) familyTitleCombo.getSelectedItem());
        
        family.setLetterTitle(familyLetterTitleEdit.getText());
    }
    
    /**
     * Method will load family attributes from selected object in DB and 
     * outputs it to GUI.
     */
    private void insertFamilyAttributesFromObject() {
        nameEdit.setText(family.getName());
        
        streetEdit.setText(family.getAddress().getStreet());
        cityEdit.setText(family.getAddress().getCity());
        zipCodeEdit.setText(family.getAddress().getZipCode());
        
        postalStreetEdit.setText(family.getPostalAddress().getStreet());
        postalCityEdit.setText(family.getPostalAddress().getCity());
        postalZipCodeEdit.setText(family.getPostalAddress().getZipCode());
        
        switch(family.getFamilyLevel()) {
            case BAD:
                badLevelRadio.setSelected(true);
                break;
            case STANDARD:
                standardLevelRadio.setSelected(true);
                break;
            case GOOD:
                goodLevelRadio.setSelected(true);
                break;
            case PERFECT:
                perfectLevelRadio.setSelected(true);
                break;
        }
        
        switch(family.getFamilyProperty()) {
            case NO_CAR:
                noCarPropertyRadio.setSelected(true);
                break;
            case ONE_CAR:
                oneCarPropertyRadio.setSelected(true);
                break;
            case TWO_CARS:
                twoCarsPropertyRadio.setSelected(true);
                break;
            case MORE_CARS:
                moreCarsPropertyRadio.setSelected(true);
                break;
        }
        
        switch(family.getHouseType()) {
            case FLAT_HIRE:
                flatHireHouseRadio.setSelected(true);
                break;
            case FLAT_OWN:
                flatOwnHouseRadio.setSelected(true);
                break;
            case SEMI_DETACHED:
                semidetachedHouseRadio.setSelected(true);
                break;
            case HOUSE:
                familyHouseRadio.setSelected(true);
                break;
        }
        
        lastVisitDateChooser.setDate(family.getLastVisit());
        
        familyTitleCombo.setSelectedItem(family.getEnvelopeTitle());
        
        familyLetterTitleEdit.setText(family.getLetterTitle());
    }
    
    /**
     * Get client selected in client list table.
     * @return selected client
     */
    private Client getSelectedClient() {
        int row = clientListTable.getSelectedRow();
        Integer familyId = (Integer) clientListTable.getValueAt(row, 0);
        return clients.get(familyId);
    }
    
    /**
     * Will show FamilyGUI in editing mode.
     */
    private void showEditClientWindow() {
        Client client = getSelectedClient();
        ClientGUI.getInstance(dbOperator,client,family).setVisible(true);
    }
    
    /**
     * Fetch clients from DB and update table model.
     */
    private void fetchClients() {
        // clear clients table
        clientListModel.setRowCount(0);
        
        clients = new HashMap<>();
        List<? extends Client> clientList = family.getDBOperator().getClients();
        for(Client client : clientList) {
            
            clients.put(client.getId(), client);
            clientListModel.addRow(new Object[] 
                {
                    client.getId(),
                    client.getFirstName(),
                    client.getIdentificationNumber(),
                    client.getDateOfBirth(),
                });
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        levelGroup = new javax.swing.ButtonGroup();
        propertyGroup = new javax.swing.ButtonGroup();
        houseGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        postalStreetEdit = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        postalCityEdit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        postalZipCodeEdit = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lastVisitDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        clientListTable = new javax.swing.JTable();
        addClientButton = new javax.swing.JButton();
        editClientButton = new javax.swing.JButton();
        removeClientButton = new javax.swing.JButton();
        envelopeButton = new javax.swing.JButton();
        reminderButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();
        familyHouseRadio = new javax.swing.JRadioButton();
        semidetachedHouseRadio = new javax.swing.JRadioButton();
        flatOwnHouseRadio = new javax.swing.JRadioButton();
        flatHireHouseRadio = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        badLevelRadio = new javax.swing.JRadioButton();
        standardLevelRadio = new javax.swing.JRadioButton();
        goodLevelRadio = new javax.swing.JRadioButton();
        perfectLevelRadio = new javax.swing.JRadioButton();
        noCarPropertyRadio = new javax.swing.JRadioButton();
        oneCarPropertyRadio = new javax.swing.JRadioButton();
        twoCarsPropertyRadio = new javax.swing.JRadioButton();
        moreCarsPropertyRadio = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        nameEdit = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        familyTitleCombo = new JComboBox(FamilyImpl.getTitles().toArray());
        jLabel12 = new javax.swing.JLabel();
        familyLetterTitleEdit = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jLabel13 = new javax.swing.JLabel();
        streetEdit = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        cityEdit = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        zipCodeEdit = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lang/System"); // NOI18N
        setTitle(bundle.getString("FAMILY_TITLE-NEW")); // NOI18N
        setResizable(false);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel2Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel1.setLayout(jPanel2Layout);

        jLabel4.setText(bundle.getString("FAMILY-POSTAL-STREET")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel4, gridBagConstraints);

        postalStreetEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        postalStreetEdit.setNextFocusableComponent(postalCityEdit);
        postalStreetEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(postalStreetEdit, gridBagConstraints);

        jLabel5.setText(bundle.getString("FAMILY-POSTAL-CITY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel5, gridBagConstraints);

        postalCityEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        postalCityEdit.setNextFocusableComponent(postalZipCodeEdit);
        postalCityEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(postalCityEdit, gridBagConstraints);

        jLabel6.setText(bundle.getString("FAMILY-POSTAL-ZIPCODE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel6, gridBagConstraints);

        postalZipCodeEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        postalZipCodeEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(postalZipCodeEdit, gridBagConstraints);

        jLabel2.setText(bundle.getString("FAMILY-LAST-VISIT")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(lastVisitDateChooser, gridBagConstraints);

        jLabel11.setText(bundle.getString("FAMILY-NOTES")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel11, gridBagConstraints);

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jScrollPane2, gridBagConstraints);

        jLabel10.setText(bundle.getString("FAMILY-MEMBERS")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 34;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel10, gridBagConstraints);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 100));

        clientListTable.setModel(clientListModel);
        clientListTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        clientListTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(clientListTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 36;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.gridheight = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jScrollPane1, gridBagConstraints);

        addClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/add.png"))); // NOI18N
        addClientButton.setText(bundle.getString("FAMILY_BUTTON-ADDCLIENT")); // NOI18N
        addClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addClientButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 50;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(addClientButton, gridBagConstraints);

        editClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/edit.png"))); // NOI18N
        editClientButton.setText(bundle.getString("FAMILY_BUTTON-EDITCLIENT")); // NOI18N
        editClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editClientButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 50;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(editClientButton, gridBagConstraints);

        removeClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/deleteSmall.png"))); // NOI18N
        removeClientButton.setText(bundle.getString("FAMILY_BUTTON-REMOVECLIENT")); // NOI18N
        removeClientButton.setIconTextGap(8);
        removeClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeClientButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 50;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(removeClientButton, gridBagConstraints);

        envelopeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/print.png"))); // NOI18N
        envelopeButton.setText(bundle.getString("FAMILY-ENVELOPE")); // NOI18N
        envelopeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                envelopeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 46;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel1.add(envelopeButton, gridBagConstraints);

        reminderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/reminder.png"))); // NOI18N
        reminderButton.setText(bundle.getString("CLIENT_BUTTON-REMINDER")); // NOI18N
        reminderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reminderButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 42;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel1.add(reminderButton, gridBagConstraints);

        applyButton.setFont(applyButton.getFont().deriveFont(applyButton.getFont().getStyle() | java.awt.Font.BOLD));
        applyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/apply.png"))); // NOI18N
        applyButton.setText(bundle.getString("FAMILY_BUTTON-OK")); // NOI18N
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 50;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel1.add(applyButton, gridBagConstraints);

        houseGroup.add(familyHouseRadio);
        familyHouseRadio.setFont(familyHouseRadio.getFont());
        familyHouseRadio.setText(bundle.getString("FAMILY-HOUSE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 32;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(familyHouseRadio, gridBagConstraints);

        houseGroup.add(semidetachedHouseRadio);
        semidetachedHouseRadio.setFont(semidetachedHouseRadio.getFont());
        semidetachedHouseRadio.setText(bundle.getString("FAMILY-SEMI_DETACHED")); // NOI18N
        semidetachedHouseRadio.setNextFocusableComponent(familyHouseRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 30;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(semidetachedHouseRadio, gridBagConstraints);

        houseGroup.add(flatOwnHouseRadio);
        flatOwnHouseRadio.setFont(flatOwnHouseRadio.getFont());
        flatOwnHouseRadio.setText(bundle.getString("FAMILY-FLAT_OWN")); // NOI18N
        flatOwnHouseRadio.setNextFocusableComponent(semidetachedHouseRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(flatOwnHouseRadio, gridBagConstraints);

        houseGroup.add(flatHireHouseRadio);
        flatHireHouseRadio.setFont(flatHireHouseRadio.getFont());
        flatHireHouseRadio.setSelected(true);
        flatHireHouseRadio.setText(bundle.getString("FAMILY-FLAT_HIRE")); // NOI18N
        flatHireHouseRadio.setNextFocusableComponent(flatOwnHouseRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(flatHireHouseRadio, gridBagConstraints);

        jLabel9.setText(bundle.getString("FAMILY-HOUSETYPE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel9, gridBagConstraints);

        jLabel8.setText(bundle.getString("FAMILY-PROPERTY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel8, gridBagConstraints);

        jLabel7.setText(bundle.getString("FAMILY-LEVEL")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel7, gridBagConstraints);

        levelGroup.add(badLevelRadio);
        badLevelRadio.setFont(badLevelRadio.getFont());
        badLevelRadio.setText(bundle.getString("FAMILY-BAD")); // NOI18N
        badLevelRadio.setNextFocusableComponent(standardLevelRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(badLevelRadio, gridBagConstraints);

        levelGroup.add(standardLevelRadio);
        standardLevelRadio.setFont(standardLevelRadio.getFont());
        standardLevelRadio.setSelected(true);
        standardLevelRadio.setText(bundle.getString("FAMILY-STANDARD")); // NOI18N
        standardLevelRadio.setNextFocusableComponent(goodLevelRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(standardLevelRadio, gridBagConstraints);

        levelGroup.add(goodLevelRadio);
        goodLevelRadio.setFont(goodLevelRadio.getFont());
        goodLevelRadio.setText(bundle.getString("FAMILY-GOOD")); // NOI18N
        goodLevelRadio.setNextFocusableComponent(perfectLevelRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(goodLevelRadio, gridBagConstraints);

        levelGroup.add(perfectLevelRadio);
        perfectLevelRadio.setFont(perfectLevelRadio.getFont());
        perfectLevelRadio.setText(bundle.getString("FAMILY-PERFECT")); // NOI18N
        perfectLevelRadio.setNextFocusableComponent(noCarPropertyRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(perfectLevelRadio, gridBagConstraints);

        propertyGroup.add(noCarPropertyRadio);
        noCarPropertyRadio.setFont(noCarPropertyRadio.getFont());
        noCarPropertyRadio.setSelected(true);
        noCarPropertyRadio.setText(bundle.getString("FAMILY-NO_CAR")); // NOI18N
        noCarPropertyRadio.setNextFocusableComponent(oneCarPropertyRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(noCarPropertyRadio, gridBagConstraints);

        propertyGroup.add(oneCarPropertyRadio);
        oneCarPropertyRadio.setFont(oneCarPropertyRadio.getFont());
        oneCarPropertyRadio.setText(bundle.getString("FAMILY-ONE_CAR")); // NOI18N
        oneCarPropertyRadio.setNextFocusableComponent(twoCarsPropertyRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(oneCarPropertyRadio, gridBagConstraints);

        propertyGroup.add(twoCarsPropertyRadio);
        twoCarsPropertyRadio.setFont(twoCarsPropertyRadio.getFont());
        twoCarsPropertyRadio.setText(bundle.getString("FAMILY-TWO_CARS")); // NOI18N
        twoCarsPropertyRadio.setNextFocusableComponent(moreCarsPropertyRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(twoCarsPropertyRadio, gridBagConstraints);

        propertyGroup.add(moreCarsPropertyRadio);
        moreCarsPropertyRadio.setFont(moreCarsPropertyRadio.getFont());
        moreCarsPropertyRadio.setText(bundle.getString("FAMILY-MORE_CARS")); // NOI18N
        moreCarsPropertyRadio.setNextFocusableComponent(flatHireHouseRadio);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(moreCarsPropertyRadio, gridBagConstraints);

        jLabel1.setText(bundle.getString("FAMILY-NAME")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(nameEdit, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 12, 0);
        jPanel1.add(jSeparator1, gridBagConstraints);

        jLabel3.setText(bundle.getString("FAMILY-NAME-ENVELOPE-TITLE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel3, gridBagConstraints);

        familyTitleCombo.setEditable(true);
        familyTitleCombo.setMinimumSize(new java.awt.Dimension(200, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(familyTitleCombo, gridBagConstraints);

        jLabel12.setText(bundle.getString("FAMILY-NAME-LETTER-TITLE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel12, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(familyLetterTitleEdit, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 23, 0, 23);
        jPanel1.add(filler1, gridBagConstraints);

        jLabel13.setText(bundle.getString("FAMILY-STREET")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel13, gridBagConstraints);

        streetEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        streetEdit.setNextFocusableComponent(postalCityEdit);
        streetEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(streetEdit, gridBagConstraints);

        jLabel14.setText(bundle.getString("FAMILY-CITY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel14, gridBagConstraints);

        cityEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        cityEdit.setNextFocusableComponent(postalZipCodeEdit);
        cityEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(cityEdit, gridBagConstraints);

        jLabel15.setText(bundle.getString("FAMILY-ZIPCODE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jLabel15, gridBagConstraints);

        zipCodeEdit.setMinimumSize(new java.awt.Dimension(0, 20));
        zipCodeEdit.setPreferredSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(zipCodeEdit, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        loadFamilyAttributesFromForm();
        if(mode == EditMode.NEW) {
            family.getDBOperator().insert();
        } else {
            family.getDBOperator().update();
        }
        MainGUI.getInstance().updateFamilies();
        this.dispose();
    }//GEN-LAST:event_applyButtonActionPerformed

    private void reminderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reminderButtonActionPerformed
        NewReminderGUI.getInstance(dbOperator,Reminder.ReminderType.FAMILY,
            family.getId()).setVisible(true);
    }//GEN-LAST:event_reminderButtonActionPerformed

    private void envelopeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_envelopeButtonActionPerformed
        PrintProcessor.printEnvelope(dbOperator.getAdviser(),family);
    }//GEN-LAST:event_envelopeButtonActionPerformed

    private void removeClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeClientButtonActionPerformed
        if(clientListTable.getSelectedRow() >= 0) {
            int answer = JOptionPane.showOptionDialog(
                null,
                Common.bundle.getString("FAMILY_CONFIRM-CLIENT-DELETE"),
                Common.bundle.getString("FAMILY_CONFIRM-TITLE"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[] { Common.bundle.getString("MAIN_CONFIRM-OK"),
                    Common.bundle.getString("MAIN_CONFIRM-NO")},
                null);
            if(answer == JOptionPane.YES_OPTION) {
                getSelectedClient().getDBOperator().delete();
                updateClients();
            }
        }
    }//GEN-LAST:event_removeClientButtonActionPerformed

    private void editClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editClientButtonActionPerformed
        if(clientListTable.getSelectedRow() >= 0) {
            showEditClientWindow();
        }
    }//GEN-LAST:event_editClientButtonActionPerformed

    private void addClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addClientButtonActionPerformed
        ClientGUI.getInstance(dbOperator,null,family).setVisible(true);
    }//GEN-LAST:event_addClientButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addClientButton;
    private javax.swing.JButton applyButton;
    private javax.swing.JRadioButton badLevelRadio;
    private javax.swing.JTextField cityEdit;
    private javax.swing.JTable clientListTable;
    private javax.swing.JButton editClientButton;
    private javax.swing.JButton envelopeButton;
    private javax.swing.JRadioButton familyHouseRadio;
    private javax.swing.JTextField familyLetterTitleEdit;
    private javax.swing.JComboBox familyTitleCombo;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JRadioButton flatHireHouseRadio;
    private javax.swing.JRadioButton flatOwnHouseRadio;
    private javax.swing.JRadioButton goodLevelRadio;
    private javax.swing.ButtonGroup houseGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private com.toedter.calendar.JDateChooser lastVisitDateChooser;
    private javax.swing.ButtonGroup levelGroup;
    private javax.swing.JRadioButton moreCarsPropertyRadio;
    private javax.swing.JTextField nameEdit;
    private javax.swing.JRadioButton noCarPropertyRadio;
    private javax.swing.JRadioButton oneCarPropertyRadio;
    private javax.swing.JRadioButton perfectLevelRadio;
    private javax.swing.JTextField postalCityEdit;
    private javax.swing.JTextField postalStreetEdit;
    private javax.swing.JTextField postalZipCodeEdit;
    private javax.swing.ButtonGroup propertyGroup;
    private javax.swing.JButton reminderButton;
    private javax.swing.JButton removeClientButton;
    private javax.swing.JRadioButton semidetachedHouseRadio;
    private javax.swing.JRadioButton standardLevelRadio;
    private javax.swing.JTextField streetEdit;
    private javax.swing.JRadioButton twoCarsPropertyRadio;
    private javax.swing.JTextField zipCodeEdit;
    // End of variables declaration//GEN-END:variables
}
