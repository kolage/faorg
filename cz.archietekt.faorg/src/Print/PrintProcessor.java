package Print;

import Entity.Adviser;
import Entity.Family;
import Faorg.Common;
import Faorg.ProcessThread;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import javax.swing.JOptionPane;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jopendocument.dom.ODSingleXMLDocument;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.jopendocument.dom.template.JavaScriptFileTemplate;
import org.jopendocument.dom.template.TemplateException;
import org.jopendocument.model.OpenDocument;
import org.jopendocument.print.DefaultDocumentPrinter;

/**
 * Class used for handling printing tasks.
 * 
 * @author kolage
 */
public class PrintProcessor {
    
    /**
     * Constant used for replacing newlines in string.
     */
    private static final String NEWLINE_REPLACEMENT = "xqzv";
    
    /**
     * Constant used for adding newline XML element to Open Document.
     */
    private static final String NEWLINE_XML_REPLACEMENT = "<text:line-break />";
    
    /**
     * Max envelopes count that can jOpenDocument safely handle to load.
     */
    private static final int MAX_ENVELOPES_COUNT = 50;
    
    /**
     * File name of envelope template.
     */
    private static final String ENVELOPE_TEMPLATE_FILENAME = "envelope.ods";
    
    /**
     * File name of letter template.
     */
    private static final String LETTER_TEMPLATE_FILENAME = "letter.odt";
    
    /**
     * File name of envelopes output.
     */
    private static final String ENVELOPES_FILENAME = "envelopes.ods";
    
    /**
     * File name of letters output.
     */
    private static final String LETTERS_FILENAME = "letters.odt";
    
    /**
     * Path to directory with document templates.
     */
    private static final String TEMPLATES_DIR = "data/templates";
    
    /**
     * Path to directory with output data.
     */
    public static final String OUTPUT_DIR = "data/out";
    
    /**
     * Get path to folder with generated letters and envelopes.
     * @return path
     */
    public static String getGeneratedDataOutputFolder() {
        return OUTPUT_DIR;
    }
    
    /**
     * Method for generating letters from template stored on hard disk and
     * from input attributes.
     * @param adviser adviser - sender of letter
     * @param families families to be generated
     * @param place place where letter was written
     * @param date date when letter was written
     * @param letterBody text of letter
     * @param salutationEnd closing salutation to be used in letter
     */
    public static void generateLetter(final Adviser adviser, 
            final Family[] families,
            final String place,
            final String date,
            final String letterBody, 
            final String salutationEnd) {
        
        // replace new line characters in string by some unique string
        final String letterBodyNew = substituteNewlines(letterBody);
        
        final int AFTER_PROCESSES_COUNTER = 1;
        
        new ProcessThread() {
            @Override
            public void process() {
                File templateFile = new File(TEMPLATES_DIR + "/"
                        + LETTER_TEMPLATE_FILENAME);

                int LETTERS_COUNT = families.length;
                
                setMaxState(2*LETTERS_COUNT + AFTER_PROCESSES_COUNTER);
                
                int k = 0;

                for (Family family : families) {
                    setText(Common.bundle.getString("GENERATING_FAMILY") + " " +
                            family.getName());
                    
                    File outFile = new File(OUTPUT_DIR + "/" + k++ + "-"
                            + LETTERS_FILENAME);
                    
                    JavaScriptFileTemplate template = null;
                    
                    try {
                        template = new JavaScriptFileTemplate(templateFile);
                    } catch (IOException | TemplateException | 
                            JDOMException ex) {
                        System.err.println(ex);
                    }
                    
                    // adviser info
                    template.setField("adviser_title", 
                            adviser.getTitle());
                    template.setField("adviser_firstname", 
                            adviser.getFirstName());
                    template.setField("adviser_lastname", 
                            adviser.getLastName());
                    template.setField("adviser_address", 
                            adviser.getAddress().getStreet());
                    template.setField("adviser_city", 
                            adviser.getAddress().getCity());
                    template.setField("adviser_zipcode", 
                            adviser.getAddress().getZipCode());
                    
                    template.setField("business_title", 
                            adviser.getTitle());
                    template.setField("business_firstname", 
                            adviser.getFirstName());
                    template.setField("business_lastname", 
                            adviser.getLastName());
                    template.setField("business_address", 
                            adviser.getAddress().getStreet());
                    template.setField("business_city", 
                            adviser.getAddress().getCity());
                    
                    template.setField("business_telephone",
                            mergeStrings("t: ", adviser.getTelephone()));
                    
                    template.setField("business_email", 
                            mergeStrings("e: ", adviser.getEmail()));
                    
                    template.setField("business_website", 
                            mergeStrings("w: ", adviser.getWebsite()));
                    
                    template.setField("business_ico", 
                            mergeStrings(Common.bundle.getString("ADVISER_ICO") + " ", 
                            adviser.getICO()));
                    
                    template.setField("business_position", 
                            adviser.getPosition());
                    
                    
                    template.setField("signature_firstname", 
                            adviser.getFirstName());
                    template.setField("signature_lastname", 
                            adviser.getLastName());
                    
                    String officeCity = adviser.getOfficeAddress().getCity();
                    
                    if(!officeCity.isEmpty()) {
                        template.setField("adviser_office_address", 
                                adviser.getOfficeAddress().getStreet());
                        template.setField("adviser_office_zipcode", 
                                adviser.getOfficeAddress().getZipCode());
                        template.setField("adviser_office_city", 
                                adviser.getOfficeAddress().getCity());
                        template.showSection("OfficeSection");
                    } else {
                        template.hideSection("OfficeSection");
                    }
                    
                    // family info
                    template.setField("title", family.getEnvelopeTitle());
                    template.setField("name", family.getName());
                    template.setField("address", family.getPostalAddress().getStreet());
                    template.setField("zip_code", family.getPostalAddress().getZipCode());
                    template.setField("city", family.getPostalAddress().getCity());
                    
                    template.setField("place", place);
                    template.setField("date", date);

                    template.setField("salutation", family.getLetterTitle());
                    template.setField("body", letterBodyNew);
                    template.setField("salutation_end", salutationEnd);
                    
                    // have to add new XML tags for line breaks, because
                    // setField method cant handle multiline strings
                    createCorrectNewlines(template,outFile);
                    
                    nextProgress();
                }
                
                setText(Common.bundle.getString("MERGING_LETTER") + " "
                        + families[0].getName());

                File firstOutFile = new File(OUTPUT_DIR + "/" + "0-"
                        + LETTERS_FILENAME);

                ODSingleXMLDocument firstDoc = null;
                try {
                    firstDoc = 
                            ODSingleXMLDocument.createFromPackage(firstOutFile);
                } catch (JDOMException | IOException ex) {
                    System.err.println(ex);
                }
                
                nextProgress();

                for (k = 1; k < LETTERS_COUNT; k++) {
                    setText(Common.bundle.getString("MERGING_LETTER") + " "
                        + families[k].getName());
                    
                    File letterFile = new File(OUTPUT_DIR + "/" + k + "-"
                            + LETTERS_FILENAME);

                    ODSingleXMLDocument doc = null;
                    try {
                        doc = ODSingleXMLDocument.createFromPackage(letterFile);
                    } catch (JDOMException | IOException ex) {
                        System.err.println(ex);
                    }

                    firstDoc.add(doc);
                    
                    letterFile.delete();
                    
                    nextProgress();
                }
                
                setText(Common.bundle.getString("SAVING_LETTER"));

                try {
                    firstDoc.saveToPackageAs(new File(OUTPUT_DIR + "/"
                            + LETTERS_FILENAME));
                } catch (IOException ex) {
                    System.err.println(ex);
                }
                
                firstOutFile.delete();
                
                nextProgress();
                
                if (Desktop.isDesktopSupported()) {
                    Desktop desktop = Desktop.getDesktop();
                    if (desktop.isSupported(Desktop.Action.OPEN)) {
                        try {
                            desktop.open(new File(OUTPUT_DIR));
                        } catch (IOException ex) {
                            System.err.println(ex);
                        }
                    } else {
                        showSavedDialog(OUTPUT_DIR, Common.bundle.getString("LETTERS_SAVED_IN"));
                    }
                } else {
                    showSavedDialog(OUTPUT_DIR, Common.bundle.getString("LETTERS_SAVED_IN"));
                }
            }
        }.execute();
    }
    
    /**
     * Prints envelope with family address.
     * @param adviser adviser - sender of letter
     * @param family family to be printed
     */
    public static void printEnvelope(Adviser adviser, Family family) {
        printEnvelope(adviser, new Family[] { family });
    }
    
    /**
     * Prints envelope with family addresses.
     * @param adviser adviser - sender of letter
     * @param families list of families to be printed
     */
    public static void printEnvelope(final Adviser adviser, 
            final Family[] families) {
        
        final int AFTER_PROCESSES_COUNTER = 3;
        
        new ProcessThread() {

            @Override
            public void process() {
                final File templateFile = 
                        new File(TEMPLATES_DIR + "/" + 
                        ENVELOPE_TEMPLATE_FILENAME);
                
                final File outFile = 
                        new File(OUTPUT_DIR + "/" + 
                        ENVELOPES_FILENAME);

                SpreadSheet spreadSheet = null;
                try {
                    spreadSheet = SpreadSheet.createFromFile(templateFile);
                } catch (IOException ex) {
                    System.err.println(ex);
                }

                spreadSheet.getFirstSheet().setName("TO-BE-DELETED");

                int index = 0;
                
                setMaxState(families.length + AFTER_PROCESSES_COUNTER);

                for (Family family : families) {
                    setText(Common.bundle.getString("GENERATING_FAMILY") + " " +
                            family.getName());
                    Sheet sheet = null;
                    try {
                        sheet = SpreadSheet.createFromFile(templateFile).
                                getFirstSheet();
                    } catch (IOException ex) {
                        System.err.println(ex);

                    }

                    sheet.setName(family.getId().toString());

                    // adviser info
                    sheet.getCellAt("A1").setValue(adviser.getTitle() + " "
                            + adviser.getFirstName() + " " 
                            + adviser.getLastName());
                    sheet.getCellAt("A2").setValue(
                            adviser.getAddress().getStreet());
                    sheet.getCellAt("A3").setValue(
                            adviser.getAddress().getZipCode() + " "
                            + adviser.getAddress().getCity());
                    
                    sheet.getCellAt("A4").setValue(
                                mergeStrings("t: ", adviser.getTelephone()));
                    
                    sheet.getCellAt("A5").setValue(
                                mergeStrings("e: ", adviser.getEmail()));
                    
                    sheet.getCellAt("A6").setValue(
                                mergeStrings(Common.bundle.getString("ADVISER_ICO") 
                            + " ", adviser.getICO()));
                    
                    // family info
                    sheet.getCellAt("D8").setValue(family.getEnvelopeTitle());
                    sheet.getCellAt("D9").setValue(family.getName());
                    sheet.getCellAt("D10").setValue(
                            family.getPostalAddress().getStreet());
                    sheet.getCellAt("D11").setValue(
                            family.getPostalAddress().getZipCode() + " "
                            + family.getPostalAddress().getCity());

                    Sheet tempSheet = spreadSheet.addSheet("XXX");
                    Element clonedSheet = (Element) sheet.getElement().clone();
                    tempSheet.getElement().getParentElement().
                            addContent(index++,clonedSheet);
                    tempSheet.detach();
                    
                    nextProgress();
                }

                spreadSheet.getSheet("TO-BE-DELETED").detach();
                
                setText(Common.bundle.getString("SAVING_ENVELOPES"));

                try {
                    spreadSheet.saveAs(outFile);
                } catch (IOException ex) {
                    System.err.println(ex);
                }
                
                nextProgress();
                
                if(families.length <= MAX_ENVELOPES_COUNT) {
                    setText(Common.bundle.getString("LOADING_ENVELOPES_CONTENT"));
                    
                    final OpenDocument doc = new OpenDocument();
                    doc.loadFrom(outFile);

                    nextProgress();
                    setText(Common.bundle.getString("PREPARING_PRINT"));

                    final DefaultDocumentPrinter printer =
                            new DefaultDocumentPrinter();
                    printer.print(doc);
                } else {
                    nextProgress();
                    nextProgress();
                    
                    if(Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if(desktop.isSupported(Desktop.Action.OPEN)) {
                            try {
                                desktop.open(new File(OUTPUT_DIR));
                            } catch (IOException ex) {
                                System.err.println(ex);
                            }
                        } else {
                            showSavedDialog(OUTPUT_DIR,
                                    Common.bundle.getString("ENVELOPE_SAVED_IN"));
                        }
                    } else {
                        showSavedDialog(OUTPUT_DIR,
                                Common.bundle.getString("ENVELOPE_SAVED_IN"));
                    }
                }

                nextProgress();
            }
            
        }.execute();
    }
    
    private static String mergeStrings(String prefix, String string) {
        if(string == null || string.isEmpty()) {
            return "";
        }
        
        return prefix + string;
    }
    
    /**
     * Auxiliary method for replacing newlines in input string for unique
     * strings. It is done because of Sheet.setField method in jOpenDocument
     * that replaces newlines from string for spaces.
     * 
     * @param inputString input string
     * @return modified string
     */
    private static String substituteNewlines(String inputString) {
        return inputString.replaceAll("\n", NEWLINE_REPLACEMENT);
    }
    
    /**
     * Auxiliary method for creating template with correctly setup newlines.
     * 
     * @param templateWSubstitutedNewlines input template with specific patterns
     * on places where should be regular newlines
     * @param outputFile file where should be saved modified template
     */
    private static void createCorrectNewlines(
            JavaScriptFileTemplate templateWSubstitutedNewlines,
            File outputFile) {
        
        File tempInFile = new File(OUTPUT_DIR + "/" +
                            "tempIn.fodt");
                    
        File tempOutFile = new File(OUTPUT_DIR + "/" +
                            "tempOut");
        
        try {
            templateWSubstitutedNewlines.saveAs(tempInFile);
            String content = new String(Files.readAllBytes(
                    tempInFile.toPath()), StandardCharsets.UTF_8);
            content = content.replaceAll(NEWLINE_REPLACEMENT,
                    NEWLINE_XML_REPLACEMENT);
            Files.write(tempOutFile.toPath(),
                    content.getBytes());
            ODSingleXMLDocument tempDoc =
                    ODSingleXMLDocument.createFromFile(tempOutFile);
            tempDoc.saveToPackageAs(outputFile);

            tempInFile.delete();
            tempOutFile.delete();
        } catch (JDOMException | IOException |
                TemplateException ex) {
            System.err.println(ex);
        }
    }
    
    /**
     * Auxiliary method for showing information dialog about generated 
     * envelopes or letter file.
     * 
     * @param prefixMessageText text of message to be shown before string with
     * final file path
     */
    public static void showSavedDialog(String filePath, String prefixMessageText) {
        JOptionPane.showMessageDialog(null,
                prefixMessageText + " "
                + new File(filePath).getAbsolutePath(),
                Common.bundle.getString("SAVED_IN_TITLE"),
                JOptionPane.INFORMATION_MESSAGE);
    }
}