package Faorg;

/**
 * Class used to represent some object's id and object's value.
 *
 * @author kolage
 */
public final class StringPair {

    /**
     * Object key - id.
     */
    private final String key;
    
    /**
     * Object value.
     */
    private final String value;

    /**
     * Creates new string pair object.
     *
     * @param key object key
     * @param value object value
     */
    public StringPair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Get object key - identificator.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Get object value.
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * Overridden toString method for easier use of StringPair in various renderers.
     *
     * @return string pair value
     */
    @Override
    public String toString() {
        return getValue();
    }
}
