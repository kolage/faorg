package Faorg;

/**
 * Class for representing addresses.
 * 
 * @author kolage
 */
public final class Address {
    
    /**
     * City name.
     */
    private String city;
    
    /**
     * Street name.
     */
    private String street;
    
    /**
     * Zip code value.
     */
    private String zipCode;
    
    /**
     * Address constructor.
     * @param city city
     * @param street street
     * @param zipCode zip code
     */
    public Address(String city, String street, String zipCode) {
        this.city = city;
        this.street = street;
        this.zipCode = zipCode;
    }
    
    /**
     * Check if address is valid - every field is filled.
     * 
     * @return True if address is valid, false otherwise.
     */
    public boolean isValid() {
        return !getCity().isEmpty() && !getStreet().isEmpty() && !getZipCode().isEmpty();
    }
    
    /**
     * Get city.
     * @return city
     */
    public String getCity() {
        if(city != null) {
            return city;
        } else {
            return "";
        }
    }
    
    /**
     * Get street.
     * @return street
     */
    public String getStreet() {
        if(street != null) {
            return street;
        } else {
            return "";
        }
    }
    
    /**
     * Get zip code.
     * @return zip code
     */
    public String getZipCode() {
        if(zipCode != null) {
            return zipCode;
        } else {
            return "";
        }
    }
    
    /**
     * Sets city.
     * @param city city 
     */
    public void setCity(String city) {
        this.city = city;
    }
    
    /**
     * Sets street.
     * @param street street
     */
    public void setStreet(String street) {
        this.street = street;
    }
    
    /**
     * Sets zip code.
     * @param zipCode zip code 
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
