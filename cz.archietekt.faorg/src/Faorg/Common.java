package Faorg;

import java.awt.Color;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class for representing objects and methods that have many classes in common.
 * 
 * @author kolage
 */
public final class Common {
    
    /**
     * Instance of class used for loading language captions.
     */
    public final static ResourceBundle bundle = ResourceBundle.getBundle("lang/System");
    
    /**
     * Locale string to be used.
     */
    public final static String LOCALE_STRING = "cs";
    
    /**
     * Maximum number of days to keep reminder that is reminding after 
     * target date.
     */
    public static final int MAX_DAYS_TO_KEEP_REMINDER = 7;
    
    /**
     * Name of preference used for automatic creating of client birthday 
     * reminder.
     */
    public final static String CREATE_BIRTHDAY_REMINDER_PREFERENCE =
            "CREATE-BIRTHDAY-REMINDER";
    
    /**
     * Name of preference used for automatic creating of client contract 
     * reminder.
     */
    public final static String CREATE_CONTRACT_REMINDER_PREFERENCE =
            "CREATE-CONTRACT-REMINDER";
    
    /**
     * Name of preference used for automatic creating of client contract 
     * anniversary reminder.
     */
    public final static String CREATE_CONTRACT_ANNIVERSARY_REMINDER_PREFERENCE =
            "CREATE-CONTRACT-ANNIVERSARY-REMINDER";
    
    /**
     * Name of preference used for automatic creating of last visit
     * reminder.
     */
    public final static String CREATE_LASTVISIT_REMINDER_PREFERENCE =
            "CREATE-LASTVISIT-REMINDER";
    
    /**
     * Name of preference used for setting number of units related to 
     * client birthday reminder.
     */
    public final static String BIRTHDAY_UNIT_COUNT_PREFERENCE = 
            "BIRTHDAY-UNIT-COUNT";
    
    /**
     * Name of preference used for setting type of units related to 
     * client birthday reminder.
     */
    public final static String BIRTHDAY_UNIT_PREFERENCE =
            "BIRTHDAY-UNIT";
    
    /**
     * Name of preference used for setting number of units related to 
     * client contract reminder.
     */
    public final static String CONTRACT_UNIT_COUNT_PREFERENCE = 
            "CONTRACT-UNIT-COUNT";
    
    /**
     * Name of preference used for setting type of units related to 
     * client contract reminder.
     */
    public final static String CONTRACT_UNIT_PREFERENCE =
            "CONTRACT-UNIT";
    
    /**
     * Name of preference used for setting number of units related to 
     * client contract anniversary reminder.
     */
    public final static String CONTRACT_ANNIVERSARY_UNIT_COUNT_PREFERENCE = 
            "CONTRACT-ANNIVERSARY-UNIT-COUNT";
    
    /**
     * Name of preference used for setting type of units related to 
     * client contract anniversary reminder.
     */
    public final static String CONTRACT_ANNIVERSARY_UNIT_PREFERENCE =
            "CONTRACT-ANNIVERSARY-UNIT";
    
    /**
     * Name of preference used for setting number of units related to 
     * client last visit reminder.
     */
    public final static String LASTVISIT_UNIT_COUNT_PREFERENCE = 
            "LASTVISIT-UNIT-COUNT";
    
    /**
     * Name of preference used for setting type of units related to 
     * client last visit reminder.
     */
    public final static String LASTVISIT_UNIT_PREFERENCE =
            "LASTVISIT-UNIT";
    
    /**
     * Name of preference used for storing Faorg application code.
     */
    public final static String FAORG_CODE_PREFERENCE =
            "FAORG-CODE";
    
    /**
     * Name of preference used for checking that adviser DB file wasn't
     * unauthorized modified.
     */
    public final static String FAORG_LICENSE_CHECK_PREFERENCE =
            "LICENSE-CHECK";
    
    /**
     * Color used for highlighting table cells.
     */
    public final static Color AQUAMARINE_COLOR = new Color(0x7f,0xff,0xd4);
    
    public final static int FAKE_BIRTH_YEAR = 1900;
    
    // initialization -- setting up today's date
    static {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DATE),
                0, 0, 0);
        todayDate = cal.getTime();
        
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DATE),
                23, 59, 59);
        todayDateAtEnd = cal.getTime();
    }
    
    /**
     * Preferences for Faorg project.
     */
    private static final Preferences prefs = Preferences.userNodeForPackage(Common.class);
    
    /**
     * Today date.
     */
    private static final Date todayDate;
    
    /**
     * Today date at end.
     */
    private static final Date todayDateAtEnd;
    
    /**
     * Date format string used in program.
     */
    private static final String dateFormatString = "dd.MM.yyyy";
    
    /**
     * Date format used in program.
     */
    private static final DateFormat dateFormat = 
            new SimpleDateFormat(dateFormatString);
    
    /**
     * Passphrase used for AES encryption/decryption.
     */
    private static final String aesPassphrase = "zyX5cF4nI11jnvr";
    
    /**
     * Separator used in Faorg program.
     */
    private static final String separator = ";";
    
    /**
     * Get today's date.
     * @return today's date
     */
    public static Date getTodayDate() {
        return todayDate;
    }
    
    /**
     * Get today's date with max hours, minutes and seconds.
     * @return today's date at end
     */
    public static Date getTodayDateAtEnd() {
        return todayDateAtEnd;
    }
    
    /**
     * Get date format used in application.
     * @return date format
     */
    public static DateFormat getDateFormat() {
        return dateFormat;
    }
    
    /**
     * Get string of date format used in application.
     * @return date format string
     */
    public static String getDateFormatString() {
        return dateFormatString;
    }
    
    /**
     * Get object storing application preferences.
     * @return application preferences object
     */
    public static Preferences getPreferences() {
        return prefs;
    }
    
    /**
     * Get separator used in Faorg program.
     * @return separator
     */
    public static String getSeparator() {
        return separator;
    }
    
    public static String getCurrentTimeString() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }
    
    public static Date getDateWithoutYear(Date date) {
        Calendar sourceCalendar = Calendar.getInstance();
        sourceCalendar.setTime(date);
        if(sourceCalendar.get(Calendar.YEAR) != FAKE_BIRTH_YEAR) {
            Calendar targetCalendar = Calendar.getInstance();
            targetCalendar.setTime(date);
            targetCalendar.set(Calendar.YEAR, 1970);

            return targetCalendar.getTime();
        } else {
            return date;
        }
    }
    
    /**
     * Computes reminder date based on unit input data.
     * 
     * @param targetDate target date
     * @param unitCountBefore number of date units before the target date
     * @param unit date unit
     * @param direction -1 if computing date before event, 1 if computing date 
     * after
     * 
     * @return reminder date
     */
    public static Date computeShiftedDate(Date targetDate, int unitCountBefore, 
            int unit, int direction) {
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(targetDate);
        targetCalendar.add(unit, direction*unitCountBefore);
        
        return targetCalendar.getTime();
    }
    
    /**
     * Get date shifted by 100 years (to be able to store various dates 
     * in unix format into DB).
     * @param date input date
     * @return shifted date
     */
    public static Long getShiftedTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 100);

        return calendar.getTimeInMillis();
    }
    
    /**
     * Get date unshifted by 100 years (to be able to store various dates 
     * in unix format into DB).
     * @param time input date
     * @return unshifted date
     */
    public static Date unshiftTime(Long time) {
        if(time != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            calendar.add(Calendar.YEAR, -100);

            return calendar.getTime();
        } else {
            return null;
        }
    }
    
    /**
     * Input date shifted by one year.
     * @param date input date
     * @return shifted date
     */
    public static Date addYearToDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 1);
        
        return calendar.getTime();
    }
    
    /**
     * Checks if two dates are same, but ignoring time.
     * @param date1 first input date
     * @param date2 second input date
     * @return true if the dates are same, false otherwise
     */
    public static boolean sameDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        
        return calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                calendar1.get(Calendar.DATE) ==  calendar2.get(Calendar.DATE) &&
                calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
    }
    
    /**
     * Checks if two dates are same, but ignoring years.
     * @param date1 first input date
     * @param date2 second input date
     * @return true if the dates are same, false otherwise
     */
    public static boolean sameDatesIgnoringYear(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        
        return calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                calendar1.get(Calendar.DATE) ==  calendar2.get(Calendar.DATE);
    }
    
    /**
     * Encrypts string.
     * @param clearText clear text
     * @return encrypted text
     */
    public static String encryptString(String clearText) {
        String cipherText = null;
        
        try {
            Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
            aes.init(Cipher.ENCRYPT_MODE, getAESKey());
            byte[] encryptedByteArray = aes.doFinal(clearText.getBytes());
            StringBuilder sb = new StringBuilder();
            for(byte encByte : encryptedByteArray) {
                sb.append(String.format("%02X",encByte));
            }
            cipherText = sb.toString();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | 
                InvalidKeyException | IllegalBlockSizeException | 
                BadPaddingException ex) {
            System.err.println(ex);
        }
        
        return cipherText;
    }
    
    /**
     * Decrypts string.
     * @param encryptedText encrypted text
     * @return clear text
     */
    public static String decryptString(String encryptedText) {
        String clearText = null;
        
        try {
            String[] encArray = encryptedText.split("(?<=\\G.{2})");
            byte[] encByteArray = new byte[encArray.length];
            for(int i = 0; i < encArray.length; i++) {
                encByteArray[i] = (byte) Integer.parseInt(encArray[i], 16);
            }
            Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
            aes.init(Cipher.DECRYPT_MODE, getAESKey());
            clearText = new String(aes.doFinal(encByteArray));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | 
                InvalidKeyException | IllegalBlockSizeException |
                BadPaddingException | NumberFormatException ex) {
            System.err.println(ex);
        }
        
        return clearText;
    }
    
    /**
     * Get AES key used in Faorg program.
     * @return AES key
     */
    private static SecretKeySpec getAESKey() {
        SecretKeySpec key = null;
        
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            digest.update(aesPassphrase.getBytes());
            key = new SecretKeySpec(digest.digest(), 0, 16, "AES");
        } catch (NoSuchAlgorithmException ex) {
            System.err.println(ex);
            System.exit(1);
        }
        
        return key;
    }
}
