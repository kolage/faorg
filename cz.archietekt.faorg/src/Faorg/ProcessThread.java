package Faorg;

import Graphical.LoadingGUI;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingWorker;

/**
 * Class for better handling with time-consuming operations including window
 * with progress info.
 * 
 * @author kolage
 */
public abstract class ProcessThread extends SwingWorker<Object, Object> implements Runnable {

    /**
     * Reference to loading GUI that implements window with progress bar.
     */
    private final LoadingGUI lg;
    
    /**
     * Actual progress value.
     */
    private int actualProgress;
    
    /**
     * Maximum progress value.
     */
    private int maxProgress;
    
    /**
     * Actual progress value scaled to number from interval [0,100].
     */
    private int actualScaledProgress;
    
    /**
     * Creates new instance of process thread class.
     */
    public ProcessThread() {
        this.lg = new LoadingGUI();
        this.actualProgress = 0;
        
        // when property progress changes - change progress bar state 
        // on loading GUI
        addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("progress".equals(evt.getPropertyName())) {
                    lg.setState(actualScaledProgress);
                }
            }
        });
    }
    
    public void destroyGUI() {
        lg.hide();
    }
    
    /**
     * Set maximum progress state.
     * @param maxState maximum state
     */
    public void setMaxState(int maxState) {
        if(maxState > 0) { 
            maxProgress = maxState;
        } else {
            maxProgress = 1;
        }
    }
    
    /**
     * Increment actual progress value.
     */
    public void nextProgress() {
        ++actualProgress;
        actualScaledProgress = (int) Math.floor(((double) actualProgress / (double) maxProgress) * 100);
        if(actualScaledProgress > 100) {
            actualScaledProgress = 100;
        }
        setProgress(actualScaledProgress);
    }
    
    /**
     * Sets information text on progress bar window.
     * @param text 
     */
    public void setText(String text) {
        lg.setText(text);
    }
    
    @Override
    protected final Object doInBackground() {
        lg.start();

        synchronized (lg) {
            try {
                lg.wait();
            } catch (InterruptedException ex) {
                System.err.println(ex);
            }
        }
        
        process();
        
        return null;
    }
    
    /**
     * This method must user implement. It should contain some time-consuming
     * process. In this method user should set maximum state of loading 
     * progress, information text on progress bar window and should switch
     * to next state when each part of whole task is done.
     */
    abstract public void process();
}
