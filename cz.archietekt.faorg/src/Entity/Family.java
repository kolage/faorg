package Entity;

import Database.FamilyDB;
import Faorg.Address;
import java.util.Date;

/**
 * Interface describing family - the group of clients with same surname.
 * 
 * @author kolage
 */
public interface Family extends Entity {
    
    /**
     * Enum representing family respectability level based on subjective feeling 
     * of adviser.
     */
    public enum FamilyLevel {
        BAD, STANDARD, GOOD, PERFECT
    }
    
    /**
     * Enum representing family property.
     */
    public enum FamilyProperty {
        NO_CAR, ONE_CAR, TWO_CARS, MORE_CARS
    }
    
    /**
     * Enum representing family house type.
     */
    public enum HouseType {
        FLAT_HIRE, FLAT_OWN, SEMI_DETACHED, HOUSE
    }
    
    /**
     * Get family name.
     * @return family name
     */
    public String getName();
    
    /**
     * Get family address.
     * @return family address
     */
    public Address getAddress();
    
    /**
     * Get family postal address.
     * @return family postal address
     */
    public Address getPostalAddress();
    
    /**
     * Get family respectability level.
     * @return family level
     */
    public FamilyLevel getFamilyLevel();
    
    /**
     * Get family property level.
     * @return family property
     */
    public FamilyProperty getFamilyProperty();
    
    /**
     * Get family house type.
     * @return house type
     */
    public HouseType getHouseType();
    
    /**
     * Get last visit of family by adviser.
     * @return date of last visit
     */
    public Date getLastVisit();
    
    /**
     * Get family envelope title (used for printing envelopes).
     * @return family envelope title
     */
    public String getEnvelopeTitle();
    
    /**
     * Get family letter title (used for generating letter templates).
     * @return family letter title
     */
    public String getLetterTitle();
    
    /**
     * Sets family name.
     * @param name family name
     */
    public void setName(String name);
    
    /**
     * Sets family address.
     * @param address family address
     */
    public void setAddress(Address address);
    
    /**
     * Sets family postal address.
     * @param address family postal address
     */
    public void setPostalAddress(Address address);
    
    /**
     * Sets family respectability level.
     * @param familyLevel family level
     */
    public void setFamilyLevel(FamilyLevel familyLevel);
    
    /**
     * Sets family respectability level.
     * @param familyLevel family level
     */
    public void setFamilyLevel(String familyLevel);
    
    /**
     * Sets family property level.
     * @param familyProperty family property
     */
    public void setFamilyProperty(FamilyProperty familyProperty);

    /**
     * Sets family property level.
     * @param familyProperty family property
     */
    public void setFamilyProperty(String familyProperty);
    
    /**
     * Sets family house type.
     * @param houseType house type
     */
    public void setHouseType(HouseType houseType);
    
    /**
     * Sets family house type.
     * @param houseType house type
     */
    public void setHouseType(String houseType);
    
    /**
     * Sets last visit of family by adviser.
     * @param lastVisit date of last visit
     */
    public void setLastVisit(Date lastVisit);
    
    /**
     * Sets family envelope title (used for printing envelopes).
     * @param title family envelope title
     */
    public void setEnvelopeTitle(String title);
    
    /**
     * Sets family letter title (used for generating letter templates).
     * @param title family letter title
     */
    public void setLetterTitle(String title);
    
    /**
     * Get family database operator.
     * @return family DB operator
     */
    @Override
    public FamilyDB getDBOperator();
}
