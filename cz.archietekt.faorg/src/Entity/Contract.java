package Entity;

import Database.ContractDB;
import java.util.Date;

/**
 * Interface describing client contracts.
 * 
 * @author kolage
 */
public interface Contract extends Entity {
    
    /**
     * Enum representing types of contracts.
     */
    public enum ContractType {
        ACCIDENT_INSURANCE, BRIDGING_LOAN, BUILDING_INSURANCE, BUILDING_SAVINGS, 
        CAR_INSURANCE, CREDIT, HOME_INSURANCE, INVESTMENT_INSURANCE, 
        LIFE_INSURANCE, MORTGAGE, PENSION_FUND, TERM_DEPOSIT, WRECKING_INSURANCE
    }
    
    /**
     * Enum representing frequency of contract payment.
     */
    public enum ContractFrequency {
        MONTHLY, QUARTERLY, BIANNUAL, ANNUAL, ONE_OFF
    }
    
    /**
     * Get contract type.
     * @return contract type
     */
    public ContractType getType();
    
    /**
     * Get id of client - contract owner.
     * @return client id
     */
    public Integer getClientId();
    
    /**
     * Get contract identification value.
     * @return contract identification value
     */
    public String getIdentificationValue();
    
    /**
     * Get date of the beginning of contract validity.
     * @return start date
     */
    public Date getStartDate();
    
    /**
     * Get date of the end of contract validity.
     * @return end date
     */
    public Date getEndDate();
    
    /**
     * Get contract frequency of payment.
     * @return frequency
     */
    public ContractFrequency getFrequency();
    
    /**
     * Get size of each contract payment.
     * @return payment
     */
    public Integer getPayment();
    
    /**
     * Get contract target amount.
     * @return target amount
     */
    public Long getTargetAmount();
    
    /**
     * Get name of institution where contract was made.
     * @return name of institution.
     */
    public String getInstitutionName();
    
    /**
     * Get contract note.
     * @return note
     */
    public String getNote();
    
    /**
     * Sets contract type.
     * @param type contract type
     */
    public void setType(ContractType type);
    
     /**
     * Sets contract type.
     * @param type contract type
     */
    public void setType(String type);
    
    /**
     * Sets contract identification value.
     * @param identificationValue identification value
     */
    public void setIdentificationValue(String identificationValue);
    
    /**
     * Sets date of the beginning of contract validity.
     * @param startDate start date
     */
    public void setStartDate(Date startDate);
    
     /**
     * Sets date of the end of contract validity.
     * @param endDate end date
     */
    public void setEndDate(Date endDate);
    
    /**
     * Sets contract frequency of payment.
     * @param frequency payment frequency
     */
    public void setFrequency(ContractFrequency frequency);
    
    /**
     * Sets contract frequency of payment.
     * @param frequency payment frequency
     */
    public void setFrequency(String frequency);
    
    /**
     * Sets size of each contract payment.
     * @param payment contract payment
     */
    public void setPayment(int payment);
    
    /**
     * Sets contract target amount.
     * @param targetAmount target amount
     */
    public void setTargetAmount(Long targetAmount);
    
    /**
     * Sets name of institution where contract is made.
     * @param institutionName institution name
     */
    public void setInstitutionName(String institutionName);
    
    /**
     * Sets adviser note.
     * @param note note
     */
    public void setNote(String note);
    
    /**
     * Get contract database operator.
     * @return contract DB operator
     */
    @Override
    public ContractDB getDBOperator();
}
