package Entity;

import Database.ClientDB;
import java.util.Date;
import java.util.List;

/**
 * Interface describing client with whom the adviser operates. Advisers have
 * arranged contracts and meetings with client. Every client belongs to exactly 
 * one family.
 * 
 * @author kolage
 */
public interface Client extends Entity {
    
    /**
     * Enum representing client working condition.
     */
    public enum WorkingCondition {
        UNEMPLOYED, EMPLOYED, TRADESMAN, ENTREPRENEUR
    }
    
    /**
     * Enum representing client sport level.
     */
    public enum SportLevel {
        NO_SPORT, RECREATIONAL, PROFESSIONAL
    }
    
    /**
     * Enum representing gender of client.
     */
    public enum Gender {
        MAN, WOMAN
    }
    
    /**
     * Get id of parent family.
     * @return family id
     */
    public Integer getFamilyId();
    
    /**
     * Get client IN.
     * @return client identification number
     */
    public String getIdentificationNumber();
    
    /**
     * Get client date of birth.
     * @return date of birth
     */
    public Date getDateOfBirth();
    
    /**
     * Get client age.
     * @return client age
     */
    public Integer getAge();
    
    /**
     * Get client first name.
     * @return client first name
     */
    public String getFirstName();
    
    /**
     * Get client income.
     * @return client income
     */
    public Long getIncome();
    
    /**
     * Get client working condition.
     * @return client working condition
     */
    public WorkingCondition getWorkingCondition();
    
    /**
     * Get client gender.
     * @return client gender
     */
    public Gender getGender();
    
    /**
     * Get client bank account number.
     * @return client bank account number
     */
    public String getBankAccountNumber();
    
    /**
     * Get client bank code.
     * @return bank code
     */
    public String getBankCode();
    
    /**
     * Get client telephones list.
     * @return telephones
     */
    public List<String> getTelephones();
    
    /**
     * Get client emails list.
     * @return emails
     */
    public List<String> getEmails();
    
    /**
     * Get client note.
     * @return note
     */
    public String getNote();
    
    /**
     * Get client's doctor.
     * @return doctor
     */
    public String getDoctor();
    
    /**
     * Get client's profession.
     * @return profession
     */
    public String getProfession();
    
    /**
     * Get identity card number.
     * @return identity card number
     */
    public String getIdentityCardNumber();
    
    
    /**
     * Get expire date of identity card.
     * @return identity card expire date
     */
    public Date getIdentityCardValidity();
    
    /**
     * Get issuer of identity card.
     * @return identity card issuer
     */
    public String getIdentityCardIssuer();
    
    /**
     * Get client place of birth.
     * @return place of birth
     */
    public String getBirthPlace();
    
    /**
     * Get client sport level.
     * @return sport level
     */
    public SportLevel getSportLevel();
    
    /**
     * Get client type of sport.
     * @return type of sport
     */
    public String getSportType();
    
    /**
     * Sets client IN and gender.
     * @param identificationNumber client IN
     * @param gender client gender
     */
    public void setIdentificationNumberAndGender(String identificationNumber, 
            Gender gender);
    
    /**
     * Sets client IN and gender.
     * @param identificationNumber client IN
     * @param gender client gender
     */
    public void setIdentificationNumberAndGender(String identificationNumber, 
            String gender);
    
    /**
     * Sets client first name.
     * @param firstName client first name
     */
    public void setFirstName(String firstName);
    
    /**
     * Sets client income.
     * @param income client income
     */
    public void setIncome(Long income);
    
    /**
     * Sets client working condition.
     * @param workingCondition client working condition
     */
    public void setWorkingCondition(WorkingCondition workingCondition);
    
    /**
     * Sets client working condition.
     * @param workingCondition client working condition
     */
    public void setWorkingCondition(String workingCondition);
    
     /**
     * Sets client bank account.
     * @param accountNumber client bank account number
     * @param bankCode bank code
     */
    public void setBankAccount(String accountNumber, String bankCode);
    
    /**
     * Sets client note.
     * @param note note
     */
    public void setNote(String note);
    
    /**
     * Set client's doctor.
     * @param doctor client doctor
     */
    public void setDoctor(String doctor);
    
    /**
     * Set client's profession.
     * @param profession client profession
     */
    public void setProfession(String profession);
    
    /**
     * Set identity card number.
     * @param number identity card number 
     */
    public void setIdentityCardNumber(String number);
    
    /**
     * Set expire date of identity card.
     * @param validity expire date of identity card
     */
    public void setIdentityCardValidity(Date validity);
    
    /**
     * Set Issuer of identity card.
     * @param issuer identity card issuer
     */
    public void setIdentityCardIssuer(String issuer);
    
    /**
     * Set client place of birth.
     * @param birthPlace place of birth
     */
    public void setBirthPlace(String birthPlace);
    
    /**
     * Set client sport level.
     * @param sportLevel sport level
     */
    public void setSportLevel(SportLevel sportLevel);
    
    /**
     * Set client sport level.
     * @param sportLevel sport level
     */
    public void setSportLevel(String sportLevel);
    
    /**
     * Set client type of sport.
     * @param type type of sport
     */
    public void setSportType(String type);
    
    /**
     * Adds client telephone to the telephone list.
     * @param telephone client telephone number
     */
    public void addTelephoneNumber(String telephone);
    
    /**
     * Removes client telephone from list.
     * @param telephone telephone number to be deleted
     */
    public void removeTelephoneNumber(String telephone);
    
    /**
     * Removes all client telephones.
     */
    public void clearTelephones();
    
    /**
     * Adds client email to the email list.
     * @param email client email address
     */
    public void addEmail(String email);
    
    /**
     * Removes client email from list.
     * @param email email address to be deleted
     */
    public void removeEmail(String email);
    
    /**
     * Removes all client emails.
     */
    public void clearEmails();
    
    /**
     * Computes birthday for this year.
     * @return birthday
     */
    public Date computeBirthday();
    
    /**
     * Get client database operator.
     * @return client DB operator
     */
    @Override
    public ClientDB getDBOperator();
}
