package Entity;

import Database.ReminderDB;
import java.util.Date;

/**
 * Interface describing reminder object - startup popup notification that 
 * reminds adviser about some important events.
 * 
 * @author kolage
 */
public interface Reminder extends Entity {
    
    /**
     * Enum representing type of reminder.
     */
    public enum ReminderType {
        BIRTHDAY, CONTRACT, CONTRACT_END, CONTRACT_ANNIVERSARY, CLIENT, FAMILY, LAST_VISIT, MEETING, OTHER
    }
    
    /**
     * Get id of object to be reminded.
     * @return id
     */
    public Integer getRemindedObjectId();
    
    /**
     * Get reminder type.
     * @return reminder type
     */
    public ReminderType getType();
    
    /**
     * Get reminder text.
     * @return reminder text
     */
    public String getText();
    
    /**
     * Get reminder date.
     * @return reminder date
     */
    public Date getDate();
    
    /**
     * Get reminder target date.
     * @return reminder target date
     */
    public Date getTargetDate();
    
    /**
     * Sets id of object to be reminded.
     * @param remindedObjectId id
     */
    public void setRemindedObjectId(Integer remindedObjectId);
    
    /**
     * Sets reminder type.
     * @param type reminder type
     */
    public void setType(ReminderType type);
    
    /**
     * Sets reminder type.
     * @param type reminder type
     */
    public void setType(String type);
    
     /**
     * Sets reminder text.
     * @param text reminder text
     */
    public void setText(String text);
    
    /**
     * Sets reminder date.
     * @param date reminder date
     */
    public void setDate(Date date);
    
    /**
     * Sets reminder target date.
     * @param date reminder target date
     */
    public void setTargetDate(Date date);
    
    /**
     * Get reminder database operator.
     * @return reminder DB operator
     */
    @Override
    public ReminderDB getDBOperator();
}
