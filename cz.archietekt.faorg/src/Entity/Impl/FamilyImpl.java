package Entity.Impl;

import Database.Impl.DatabaseOperator;
import Database.Impl.FamilyDBImpl;
import Entity.Family;
import Faorg.Address;
import Faorg.Common;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation of FamilyDB interface.
 * 
 * @author kolage
 */
public final class FamilyImpl extends EntityImpl implements Family {
    
    /**
     * Name of family.
     */
    private String name;
    
    /**
     * Family address.
     */
    private Address address;
    
    /**
     * Family postal address.
     */
    private Address postalAddress;
    
    /**
     * Family behavior level.
     */
    private FamilyLevel familyLevel;
    
    /**
     * Family property level.
     */
    private FamilyProperty familyProperty;
    
    /**
     * Family house type.
     */
    private HouseType houseType;
    
    /**
     * Date of last visit of family by adviser.
     */
    private Date lastVisit;
    
    /**
     * Family envelope title (used for printing envelopes).
     */
    private String envelopeTitle;
    
    /**
     * Family letter title (used for generating letter templates).
     */
    private String letterTitle;
    
    /**
     * Family database operator.
     */
    private final FamilyDBImpl familyDB;
    
    /**
     * Creates new family object.
     * @param operator reference to database operator
     * @param id family id
     */
    public FamilyImpl(DatabaseOperator operator, Integer id) {
        super(operator,id);
        this.familyDB = new FamilyDBImpl(this, operator);
    }
    
    @Override
    public String getName() {
        if(name != null) {
            return name;
        } else {
            return "";
        }
    }
    
    @Override
    public Address getAddress() {
        if(address != null) {
            return address;
        } else {
            return new Address("", "", "");
        }
    }
    
    @Override
    public Address getPostalAddress() {
        if(postalAddress.isValid()) {
            return postalAddress;
        } else {
            return getAddress();
        }
    }
    
    @Override
    public FamilyLevel getFamilyLevel() {
        if(familyLevel != null) {
            return familyLevel;
        } else {
            return FamilyLevel.STANDARD;
        }
    }
    
    @Override
    public FamilyProperty getFamilyProperty() {
        if(familyProperty != null) {
            return familyProperty;
        } else {
            return FamilyProperty.NO_CAR;
        }
    }
    
    @Override
    public HouseType getHouseType() {
        if(houseType != null) {
            return houseType;
        } else {
            return HouseType.FLAT_HIRE;
        }
    }
    
    @Override
    public Date getLastVisit() {
        if(lastVisit != null) {
            return lastVisit;
        } else {
            return new Date(0);
        }
    }
    
    @Override
    public String getEnvelopeTitle() {
        if(envelopeTitle != null) {
            return envelopeTitle;
        } else {
            return getTitles().get(0);
        }
    }
    
    @Override
    public String getLetterTitle() {
        if(letterTitle != null) {
            return letterTitle;
        } else {
            return "Vážení " + getName();
        }
    }
    
    @Override
    public FamilyDBImpl getDBOperator() {
        return familyDB;
    }
    
    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public void setAddress(Address address) {
        this.address = address;
    }
    
    @Override
    public void setPostalAddress(Address postalAddress) {
        this.postalAddress = postalAddress;
    }
    
    @Override
    public void setFamilyLevel(FamilyLevel familyLevel) {
        this.familyLevel = familyLevel;
    }
    
    @Override
    public void setFamilyLevel(String familyLevel) {
        for(FamilyLevel level : FamilyLevel.values()) {
            if(familyLevel.equals(level.toString())) {
                this.familyLevel = level;
            }
        }
    }
    
    @Override
    public void setFamilyProperty(FamilyProperty familyProperty) {
        this.familyProperty = familyProperty;
    }
    
    @Override
    public void setFamilyProperty(String familyProperty) {
        for(FamilyProperty property : FamilyProperty.values()) {
            if(familyProperty.equals(property.toString())) {
                this.familyProperty = property;
            }
        }
    }
    
    @Override
    public void setHouseType(HouseType houseType) {
        this.houseType = houseType;
    }
    
    @Override
    public void setHouseType(String houseType) {
        for(HouseType type : HouseType.values()) {
            if(houseType.equals(type.toString())) {
                this.houseType = type;
            }
        }
    }
    
    @Override
    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }
    
    @Override
    public void setEnvelopeTitle(String envelopeTitle) {
        this.envelopeTitle = envelopeTitle;
    }
    
    @Override
    public void setLetterTitle(String letterTitle) {
        this.letterTitle = letterTitle;
    }
    
    @Override
    public String toString() {
        return "FAMILY";
    }
    
    /**
     * Get name titles used for family salutation.
     * @return list of titles
     */
    public static List<String> getTitles() {
        List titles = new ArrayList();
        
        titles.add("");
        titles.add("Vážený pan");
        titles.add("Vážená paní");
        titles.add("Vážená rodina");
        titles.add("Vážení manželé");
        
        return titles;
    }

    @Override
    public String exportToCSV() {
        return String.join(";",
                getId().toString(),
                getName(),
                getAddress().getStreet(),
                getAddress().getCity(),
                getAddress().getZipCode(),
                getPostalAddress().getStreet(),
                getPostalAddress().getCity(),
                getPostalAddress().getZipCode(),
                Common.bundle.getString(this.toString() + "-" + getFamilyLevel().toString()),
                Common.bundle.getString(this.toString() + "-" + getFamilyProperty().toString()),
                Common.bundle.getString(this.toString() + "-" + getHouseType().toString()),
                Common.getDateFormat().format(getLastVisit()));
    }

    @Override
    public String headerCSV() {
        return "id;name;street;city;zip;postal_street;postal_city;postal_zip;family_level;family_property;house_type;"
                + "last_visit";
    }
    
}