package Entity.Impl;

import Database.Impl.DatabaseOperator;
import Database.Impl.EntityDBImpl;
import Entity.Entity;

/**
 * Base abstract class implementing Entity interface.
 * 
 * @author kolage
 */
public abstract class EntityImpl implements Entity {
    
    /**
     * Identification value.
     */
    private final Integer id;
    
    /**
     * Entity database operator.
     */
    private EntityDBImpl entityDB;
    
    /**
     * Creates new entity object.
     * @param operator Reference to database operator
     * @param id entity id
     */
    public EntityImpl(DatabaseOperator operator, Integer id) {
        this.id = id;
    }
    
    @Override
    public Integer getId() {
        if(id != null) {
            return id;
        } else {
            return 0;
        }
    }
    
    @Override
    public EntityDBImpl getDBOperator() {
        return entityDB;
    }
    
    @Override
    public String toString() {
        return "ENTITY";
    }
}
