package Entity.Impl;

import Database.Impl.DatabaseOperator;
import Database.Impl.ClientDBImpl;
import Entity.Client;
import Entity.Family;
import Faorg.Common;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Implementation of Client interface.
 * 
 * @author kolage
 */
public final class ClientImpl extends EntityImpl implements Client {
    
    /**
     * Minimal length of identification number string.
     */
    private final int IN_LENGTH = 6;
    
    /**
     * Constant used within IN in woman month field.
     */
    private final int IN_WOMAN_MONTH_CONSTANT = 50;
    
    /**
     * Constant used for distinguishing people born in year 1900-1925 and 
     * 2000-2025. We will only work with people born after 1925 and before 2025.
     */
    private final int IN_YEAR_LOWER_BOUND = 25;
    
    /**
     * Constant used for computing birth year of people born in 2000-2025.
     */
    private final int IN_ADDITIVE_CONST = 100;
    
    /**
     * Client identification value of its family in database.
     */
    private final Integer familyId;
    
    /**
     * Client IN.
     */
    private String identificationNumber;
    
    /**
     * Client date of birth.
     */
    private Date dateOfBirth;
    
    /**
     * Client age.
     */
    private Integer age;
    
    /**
     * Client first name.
     */
    private String firstName;
    
    /**
     * Client monthly income.
     */
    private Long income;

    /**
     * Client working condition.
     */
    private WorkingCondition workingCondition;
    
    /**
     * Client gender.
     */
    private Gender gender;
    
    /**
     * Client bank account number.
     */
    private String bankAccountNumber;
    
    /**
     * Bank code of client bank account.
     */
    private String bankCode;
    
    /**
     * Client note.
     */
    private String note;
    
    /**
     * Client's doctor.
     */
    private String doctor;
    
    /**
     * Client's profession.
     */
    private String profession;
    
    /**
     * Identity card number.
     */
    private String identityCardNumber;
    
    /**
     * Expire date of identity card.
     */
    private Date identityCardValidity;
    
    /**
     * Issuer of identity card.
     */
    private String identityCardIssuer;
    
    /**
     * Place of birth.
     */
    private String birthPlace;
    
    /**
     * Sport level.
     */
    private SportLevel sportLevel;
    
    /**
     * Type of sport.
     */
    private String sportType;
    
    /**
     * List of client telephone numbers.
     */
    private final List<String> telephones;
    
    /**
     * List of client email addresses.
     */
    private final List<String> emails;
    
    /**
     * Client database operator.
     */
    private final ClientDBImpl clientDB;
    
    /**
     * Creates new client object.
     * @param operator reference to database operator
     * @param id client id
     * @param familyId id of parent family
     */
    public ClientImpl(DatabaseOperator operator, Integer id, Integer familyId) {
        super(operator,id);
        this.familyId = familyId;
        clientDB = new ClientDBImpl(this, operator);
        telephones = new ArrayList<>();
        emails = new ArrayList<>();
    }
    
    @Override
    public Integer getFamilyId() {
        return familyId;
    }
    
    @Override
    public String getIdentificationNumber() {
        if(identificationNumber != null) {
            return identificationNumber;
        } else {
            return "";
        }
    }
    
    @Override
    public Date getDateOfBirth() {
        if(dateOfBirth != null) {
            return dateOfBirth;
        } else {
            return new Date();
        }
    }
    
    @Override
    public Integer getAge() {
        if(age != null) {
            return age;
        } else {
            return 0;
        }
    }
    
    @Override
    public String getFirstName() {
        if(firstName != null) {
            return firstName;
        } else {
            return "";
        }
    }
    
    @Override
    public Long getIncome() {
        if(income != null) {
            return income;
        } else {
            return 0L;
        }
    }
    
    @Override
    public WorkingCondition getWorkingCondition() {
        if(workingCondition != null) {
            return workingCondition;
        } else {
            return WorkingCondition.UNEMPLOYED;
        }
    }
    
    @Override
    public Gender getGender() {
        if(gender != null) {
            return gender;
        } else {
            return Gender.MAN;
        }
    }
    
    @Override
    public String getBankAccountNumber() {
        if(bankAccountNumber != null) {
            return bankAccountNumber;
        } else {
            return "";
        }
    }
    
    @Override
    public String getBankCode() {
        if(bankCode != null) {
            return bankCode;
        } else {
            return "";
        }
    }
    
    @Override
    public String getNote() {
        if(note != null) {
            return note;
        } else {
            return "";
        }
    }
    
    @Override
    public List<String> getTelephones() {
        return telephones;
    }
    
    @Override
    public List<String> getEmails() {
        return emails;
    }
    
    @Override
    public ClientDBImpl getDBOperator() {
        return clientDB;
    }
    
    @Override
    public void setIdentificationNumberAndGender(String identificationNumber, 
            Gender gender) {
        setGender(gender);
        this.identificationNumber = identificationNumber;
        this.dateOfBirth = computeDateOfBirth(identificationNumber);
        this.age = computeAge(dateOfBirth);
    }
    
    @Override
    public void setIdentificationNumberAndGender(String identificationNumber, 
            String gender) {
        setGender(gender);
        this.identificationNumber = identificationNumber;
        this.dateOfBirth = computeDateOfBirth(identificationNumber);
        this.age = computeAge(dateOfBirth);
    }
    
    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    @Override
    public void setIncome(Long income) {
        this.income = income;
    }
    
    @Override
    public void setWorkingCondition(WorkingCondition workingCondition) {
        this.workingCondition = workingCondition;
    }
    
    @Override
    public void setWorkingCondition(String workingCondition) {
        for(WorkingCondition condition : WorkingCondition.values()) {
            if(workingCondition.equals(condition.toString())) {
                this.workingCondition = condition;
                break;
            }
        }
    }
    
    @Override
    public void setBankAccount(String accountNumber, String bankCode) {
        this.bankAccountNumber = accountNumber;
        this.bankCode = bankCode;
    }
    
    @Override
    public void setNote(String note) {
        this.note = note;
    }
    
    @Override
    public void addTelephoneNumber(String telephone) {
        telephones.add(telephone);
    }
    
    @Override
    public void removeTelephoneNumber(String telephone) {
        for(String telephoneIt : telephones) {
            if(telephoneIt.equals(telephone)) {
                telephones.remove(telephoneIt);
                break;
            }
        }
    }
    
    @Override
    public void clearTelephones() {
        telephones.clear();
    }
    
    @Override
    public void addEmail(String email) {
        emails.add(email);
    }
    
    @Override
    public void removeEmail(String email) {
        for(String emailIt : emails) {
            if(emailIt.equals(email)) {
                emails.remove(emailIt);
                break;
            }
        }
    }
    
    @Override
    public void clearEmails() {
        emails.clear();
    }
    
    @Override
    public String toString() {
        return "CLIENT";
    }
    
    @Override
    public Date computeBirthday() {
        Calendar calendar = Calendar.getInstance();
        int actualYear = calendar.get(Calendar.YEAR);
        calendar.setTime(getDateOfBirth());
        calendar.set(Calendar.YEAR, actualYear);

        return calendar.getTime();
    }
    
    /**
     * Sets client gender.
     * @param gender client gender
     */
    private void setGender(Gender gender) {
        this.gender = gender;
    }
    
    /**
     * Sets client gender.
     * @param gender client gender
     */
    private void setGender(String gender) {
        for(Gender genderIt : Gender.values()) {
            if(gender.equals(genderIt.toString())) {
                this.gender = genderIt;
                break;
            }
        }
    }
    
    /**
     * Computes date of birth from client IN.
     * @param identificationNumber client IN
     * @return date of birth
     */
    private Date computeDateOfBirth(String identificationNumber) {
        if(identificationNumber.length() >= IN_LENGTH) {
            // get first two characters representing year
            // if one was born in 2000-2025, we will add constant 100 to
            // correctly compute the birth date
            int year = Integer.parseInt(identificationNumber.substring(0, 2));
            if(year < IN_YEAR_LOWER_BOUND) {
                year += IN_ADDITIVE_CONST;
            }
            
            // get third and forth character representing month
            // if she is girl, subtract IN constant
            int month = Integer.parseInt(identificationNumber.substring(2, 4));
            if(getGender() == Gender.WOMAN) {
                month -= IN_WOMAN_MONTH_CONSTANT;
            }
            
            // get fifth and sixth character representing day
            int day = Integer.parseInt(identificationNumber.substring(4, 6));
            
            Calendar cal = GregorianCalendar.getInstance();
            cal.set(1900 + year, month-1, day, 0, 0, 0);
            cal.set(Calendar.MILLISECOND, 0);
            
            return cal.getTime();
        } else {
            return new GregorianCalendar(Common.FAKE_BIRTH_YEAR, 1, 1).getTime();
        }
    }
    
    /**
     * Compute client age from date of birth.
     * @param dateOfBirth date of birth
     * @return client age
     */
    private int computeAge(Date dateOfBirth) {
        
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar today = Calendar.getInstance();
        
        int computedAge = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        
        if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
            computedAge--;
        } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                && today.get(Calendar.DAY_OF_MONTH) < 
                dob.get(Calendar.DAY_OF_MONTH)) {
            computedAge--;
        }
        
        return computedAge;
    }

    @Override
    public String getDoctor() {
        if(doctor != null) {
            return doctor;
        } else {
            return "";
        }
    }

    @Override
    public String getProfession() {
        if(profession != null) {
            return profession;
        } else {
            return "";
        }
    }

    @Override
    public String getIdentityCardNumber() {
        if(identityCardNumber != null) {
            return identityCardNumber;
        } else {
            return "";
        }
    }

    @Override
    public Date getIdentityCardValidity() {
        if(identityCardValidity != null) {
            return identityCardValidity;
        } else {
            return new Date(0);
        }
    }

    @Override
    public String getIdentityCardIssuer() {
        if(identityCardIssuer != null) {
            return identityCardIssuer;
        } else {
            return "";
        }
    }

    @Override
    public String getBirthPlace() {
        if(birthPlace != null) {
            return birthPlace;
        } else {
            return "";
        }
    }

    @Override
    public SportLevel getSportLevel() {
        if(sportLevel != null) {
            return sportLevel;
        } else {
            return SportLevel.NO_SPORT;
        }
    }

    @Override
    public String getSportType() {
        if(sportType != null) {
            return sportType;
        } else {
            return "";
        }
    }

    @Override
    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Override
    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Override
    public void setIdentityCardNumber(String number) {
        this.identityCardNumber = number;
    }

    @Override
    public void setIdentityCardValidity(Date validity) {
        this.identityCardValidity = validity;
    }

    @Override
    public void setIdentityCardIssuer(String issuer) {
        this.identityCardIssuer = issuer;
    }

    @Override
    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Override
    public void setSportLevel(String sportLevel) {
        for(SportLevel level : SportLevel.values()) {
            /*
             * WARNING: Some very weird error !!!
             * If I flip equals, parent process thread hangs somehow.
             */
            if(level.toString().equals(sportLevel)) {
                this.sportLevel = level;
            }
        }
    }
    
    @Override
    public void setSportLevel(SportLevel sportLevel) {
        this.sportLevel = sportLevel;
    }

    @Override
    public void setSportType(String type) {
        this.sportType = type;
    }

    @Override
    public String exportToCSV() {
        Family family = clientDB.getFamily();
        
        return String.join(";", 
                getId().toString(),
                getFamilyId().toString(),
                getFirstName() + " " + family.getName(), 
                Common.bundle.getString(this.toString() + "-" + getGender().toString()), 
                getAge().toString(), 
                Common.getDateFormat().format(getDateOfBirth()),
                String.join(",", getEmails()),
                String.join(",", getTelephones()),
                getBankAccountNumber() + "/" + getBankCode(),
                getBirthPlace(),
                getProfession(),
                getSportType(),
                Common.bundle.getString(this.toString() + "-" + getSportLevel().toString()),
                getDoctor(),
                getIdentificationNumber(),
                getIdentityCardIssuer(),
                getIdentityCardNumber(),
                Common.getDateFormat().format(getIdentityCardValidity()),
                getIncome().toString(),
                Common.bundle.getString(this.toString() + "-" + getWorkingCondition().toString()),
                getNote());
    }

    @Override
    public String headerCSV() {
        return "id;family_id;name;gender;age;birth;emails;phones;bank_account;birth_place;profession;sport;sport_level;"
                + "doctor;in;ic_issuer;ic_number;ic_validity;income;work;note";
    }
}
