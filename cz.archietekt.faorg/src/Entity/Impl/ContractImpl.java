package Entity.Impl;

import Database.Impl.DatabaseOperator;
import Database.Impl.ContractDBImpl;
import Entity.Contract;
import Faorg.Common;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation of Contract interface.
 * 
 * @author kolage
 */
public final class ContractImpl extends EntityImpl implements Contract {
    
    /**
     * Type of contract.
     */
    private ContractType type;
    
    /**
     * Identification number of client - owner of this contract.
     */
    private final Integer clientId;
    
    /**
     * Contract identification string.
     */
    private String identificationValue;
    
    /**
     * Beginning of contract validity.
     */
    private Date startDate;
    
    /**
     * End of contract validity.
     */
    private Date endDate;
    
    /**
     * Frequency of contract payment.
     */
    private ContractFrequency frequency;
    
    /**
     * Size of each payment in contract.
     */
    private Integer payment;
    
    /**
     * Contract target amount.
     */
    private Long targetAmount;
    
    /**
     * Name of institution where contract is made.
     */
    private String institutionName;
    
    /**
     * Adviser note.
     */
    private String note;
    
    /**
     * Contract database operator.
     */
    private final ContractDBImpl contractDB;
    
    /**
     * Creates new contract object.
     * @param operator reference to database operator
     * @param id contract id
     * @param clientId id of client - the contract owner
     */
    public ContractImpl(DatabaseOperator operator, Integer id, Integer clientId) {
        super(operator,id);
        this.clientId = clientId;
        contractDB = new ContractDBImpl(this, operator);
    }
    
    @Override
    public ContractType getType() {
        if(type != null) {
            return type;
        } else {
            return ContractType.CREDIT;
        }
    }
    
    @Override
    public Integer getClientId() {
        return clientId;
    }
    
    @Override
    public String getIdentificationValue() {
        if(identificationValue != null) {
            return identificationValue;
        } else {
            return "";
        }
    }
    
    @Override
    public Date getStartDate() {
        if(startDate != null) {
            return startDate;
        } else {
            return new Date();
        }
    }
    
    @Override
    public Date getEndDate() {
        if(endDate != null) {
            return endDate;
        } else {
            return new Date();
        }
    }
    
    @Override
    public ContractFrequency getFrequency() {
        if(frequency != null) {
            return frequency;
        } else {
            return ContractFrequency.ANNUAL;
        }
    }
    
    @Override
    public Integer getPayment() {
        if(payment != null) {
            return payment;
        } else {
            return 0;
        }
    }
    
    @Override
    public Long getTargetAmount() {
        if(targetAmount != null) {
            return targetAmount;
        } else {
            return 0L;
        }
    }
    
    @Override
    public String getInstitutionName() {
        if(institutionName != null) {
            return institutionName;
        } else {
            return "";
        }
    }
    
    @Override
    public String getNote() {
        if(note != null) {
            return note;
        } else {
            return "";
        }
    }
    
    @Override
    public ContractDBImpl getDBOperator() {
        return contractDB;
    }
    
    @Override
    public void setType(ContractType type) {
        this.type = type;
    }
    
    @Override
    public void setType(String type) {
        for(ContractType typeIt : ContractType.values()) {
            if(type.equals(typeIt.toString())) {
                this.type = typeIt;
                break;
            }
        }
    }
    
    @Override
    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }
    
    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    @Override
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    @Override
    public void setFrequency(ContractFrequency frequency) {
        this.frequency = frequency;
    }
    
    @Override
    public void setFrequency(String frequency) {
        for(ContractFrequency frequencyIt : ContractFrequency.values()) {
            if(frequency.equals(frequencyIt.toString())) {
                this.frequency = frequencyIt;
                break;
            }
        }
    }
    
    @Override
    public void setPayment(int payment) {
        this.payment = payment;
    }
    
    @Override
    public void setTargetAmount(Long targetAmount) {
        this.targetAmount = targetAmount;
    }
    
    @Override
    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }
    
    @Override
    public void setNote(String note) {
        this.note = note;
    }
    
    @Override
    public String toString() {
        return "CONTRACT";
    }

    /**
     * Get institution names used for contracts in DB.
     * @return list of institution names
     */
    public static List<String> getInstitutions() {
        List institutions = new ArrayList();
        
        institutions.add("");
        
        institutions.add("AXA fondy");
        institutions.add("CONSEQ");
        institutions.add("CYRRUS");
        institutions.add("ČPI");
        institutions.add("ING banka");
        institutions.add("PIONEER");
        
        institutions.add("Allianz penz.");
        institutions.add("ČS penz.");
        institutions.add("ČP penz.");
        institutions.add("Raiffeisen penz.");
        
        institutions.add("Aegon poj.");
        institutions.add("Allianz poj.");
        institutions.add("AXA poj.");
        institutions.add("ČPP");
        institutions.add("ČS poj.");
        institutions.add("Česká poj.");
        institutions.add("ČSOB poj.");
        institutions.add("Generali poj.");
        institutions.add("ING poj.");
        institutions.add("Kooperativa poj.");
        institutions.add("Metlife poj.");
        institutions.add("Slavia poj.");
        institutions.add("Uniqa poj.");
        institutions.add("Wustenrot poj.");
        
        institutions.add("ČMSS");
        institutions.add("MPSS");
        institutions.add("RSTS");
        institutions.add("SSČS");
        institutions.add("WSS");
        
        institutions.add("Akcenta");
        institutions.add("mBank");
        institutions.add("ČS");
        institutions.add("ČSOB");
        institutions.add("EQUA");
        institutions.add("GE Money");
        institutions.add("Hypoteční banka");
        institutions.add("KB");
        institutions.add("LBBW");
        institutions.add("Oberbank");
        institutions.add("RB");
        institutions.add("Sberbank");
        institutions.add("UCB");
        institutions.add("WHB");
        institutions.add("WR");
        
        institutions.add("Datart");
        institutions.add("Deutscher Ring");
        institutions.add("IPB");
        
        return institutions;
    }

    @Override
    public String exportToCSV() {
        return String.join(";", 
                getId().toString(),
                getClientId().toString(),
                Common.bundle.getString(this.toString() + "-" + getType().toString()),
                Common.getDateFormat().format(getStartDate()),
                Common.getDateFormat().format(getEndDate()),
                getInstitutionName(),
                getIdentificationValue(),
                Common.bundle.getString(this.toString() + "-" + getFrequency().toString()),
                getPayment().toString(),
                getTargetAmount().toString(),
                getNote());
    }

    @Override
    public String headerCSV() {
        return "id;client_id;type;start;end;institution;id_number;freq;payment;target;note";
    }
}
