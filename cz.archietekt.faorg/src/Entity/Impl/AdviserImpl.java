package Entity.Impl;

import Database.Impl.AdviserDBImpl;
import Database.Impl.DatabaseOperator;
import Entity.Adviser;
import Faorg.Address;

/**
 * Implementation of Adviser interface.
 * 
 * @author kolage
 */
public final class AdviserImpl extends EntityImpl implements Adviser {
    
    /**
     * Adviser first name.
     */
    private String firstName;
    
    /**
     * Adviser last name.
     */
    private String lastName;
    
    /**
     * Adviser login to database.
     */
    private final String login;
    
    /**
     * Adviser password to database.
     */
    private String password;
    
    /**
     * Hashed password loaded from DB.
     */
    private String hashedPassword;
    
    /**
     * Adviser telephone number.
     */
    private String telephone;
    
    /**
     * Adviser email address.
     */
    private String email;
    
    /**
     * Adviser homepage.
     */
    private String website;
    
    /**
     * Adviser title.
     */
    private String title;
    
    /**
     * Adviser address.
     */
    private Address address;
    
    /**
     * Adviser office address.
     */
    private Address officeAddress;
    
    /**
     * Adviser ICO.
     */
    private String ico;
    
    /**
     * Adviser position.
     */
    private String position;
    
    /**
     * Adviser database operator.
     */
    private final AdviserDBImpl adviserDB;
    
    /**
     * Creates new adviser object.
     * @param operator reference to database operator
     * @param login adviser login
     */
    public AdviserImpl(DatabaseOperator operator, String login) {
        super(operator,null);
        this.login = login;
        adviserDB = new AdviserDBImpl(this, operator);
    }
    
    @Override
    public String getFirstName() {
        if(firstName != null) {
            return firstName;
        } else {
            return "";
        }
    }
    
    @Override
    public String getLastName() {
        if(lastName != null) {
            return lastName;
        } else {
            return "";
        }
    }
    
    @Override
    public String getLogin() {
        if(login != null) {
            return login;
        } else {
            return "";
        }
    }

    @Override
    public String getPassword() {
        if(password != null) {
            return password;
        } else {
            return "";
        }
    }
    
    @Override
    public String getTelephone() {
        if(telephone != null) {
            return telephone;
        } else {
            return "";
        }
    }
    
    @Override
    public String getEmail() {
        if(email != null) {
            return email;
        } else {
            return "";
        }
    }
    
    @Override
    public String getWebsite() {
        if(website != null) {
            return website;
        } else {
            return "";
        }
    }
    
    @Override
    public String getTitle() {
        if(title != null) {
            return title;
        } else {
            return "";
        }
    }
    
    @Override
    public Address getAddress() {
        if(address != null) {
            return address;
        } else {
            return new Address("", "", "");
        }
    }
    
    @Override
    public Address getOfficeAddress() {
        if(officeAddress != null) {
            return officeAddress;
        } else {
            return new Address("", "", "");
        }
    }
    
    @Override
    public String getICO() {
        if(ico != null) {
            return ico;
        } else {
            return "";
        }
    }
    
    @Override
    public String getPosition() {
        if(position != null) {
            return position;
        } else {
            return "";
        }
    }
    
    public String getHashedPassword() {
        return hashedPassword;
    }
    
    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setWebsite(String website) {
        this.website = website;
    }
    
    @Override
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public void setAddress(Address address) {
        this.address = address;
    }
    
    @Override
    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }
    
    @Override
    public void setICO(String ico) {
        this.ico = ico;
    }
    
    @Override
    public void setPosition(String position) {
        this.position = position;
    }
    
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
    
    @Override
    public String toString() {
        return "ADVISER";
    }

    @Override
    public AdviserDBImpl getDBOperator() {
        return adviserDB;
    }

    @Override
    public Integer getId() {
        throw new UnsupportedOperationException("Adviser doesn't have id, but login.");
    }

    @Override
    public String exportToCSV() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String headerCSV() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
