package Entity.Impl;

import Database.Impl.DatabaseOperator;
import Database.Impl.ReminderDBImpl;
import Entity.Reminder;
import Faorg.Common;
import java.util.Calendar;
import java.util.Date;

/**
 * Implementation of ReminderDB interface.
 * 
 * @author kolage
 */
public final class ReminderImpl extends EntityImpl implements Reminder {
    
    /**
     * Id of object to be reminded.
     */
    private Integer remindedObjectId;

    /**
     * Type of reminder.
     */
    private ReminderType type;
    
    /**
     * Reminder text.
     */
    private String text;
    
    /**
     * Reminder date.
     */
    private Date date;
    
    /**
     * Reminder target date.
     */
    private Date targetDate;
    
    /**
     * Reminder database operator.
     */
    private final ReminderDBImpl reminderDB;
    
    /**
     * Creates new reminder object.
     * @param operator reference to database connection
     * @param id reminder id
     */
    public ReminderImpl(DatabaseOperator operator, Integer id) {
        super(operator,id);
        this.reminderDB = new ReminderDBImpl(this, operator);
    }
    
    @Override
    public Integer getRemindedObjectId() {
        return remindedObjectId;
    }

    @Override
    public ReminderType getType() {
        if(type != null) {
            return type;
        } else {
            return ReminderType.OTHER;
        }
    }
    
    @Override
    public String getText() {
        if(text != null) {
            return text;
        } else {
            return "";
        }
    }
    
    @Override
    public Date getDate() {
        if(date != null) {
            return date;
        } else {
            return new Date();
        }
    }
    
    @Override
    public Date getTargetDate() {
        if(targetDate != null) {
            return targetDate;
        } else {
            return new Date();
        }
    }
    
    @Override
    public ReminderDBImpl getDBOperator() {
        return reminderDB;
    }
    
    @Override
    public void setRemindedObjectId(Integer remindedObjectId) {
        this.remindedObjectId = remindedObjectId;
    }
    
    @Override
    public void setType(ReminderType type) {
        this.type = type;
    }
    
    @Override
    public void setType(String type) {
        for(ReminderType typeIt : ReminderType.values()) {
            if(type.equals(typeIt.toString())) {
                this.type = typeIt;
                break;
            }
        }
    }
    
    @Override
    public void setText(String text) {
        this.text = text;
    }
    
    @Override
    public void setDate(Date date) {
        this.date = date;
    }
    
    @Override
    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }
    
    @Override
    public String toString() {
        return "REMINDER";
    }
    
    public boolean isExpired() {
        Date todayDate = Common.getTodayDateAtEnd();
        
        // reminder target date must be expired
        if(getTargetDate().compareTo(todayDate) <= 0) {
            // if reminder is reminding before target date, it should be removed
            if(getTargetDate().compareTo(getDate()) >= 0) {
                return true;
            // else if reminder is reminding after target date, it should be
            // kept only if it is not older than week
            } else {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                start.setTime(getDate());
                end.setTime(todayDate);
                long diffTime = end.getTime().getTime() - 
                        start.getTime().getTime();
                long diffDays = diffTime / (1000 * 60 * 60 * 24);
                if(diffDays > Common.MAX_DAYS_TO_KEEP_REMINDER) {
                    return true;
                }
            }
        }
        
        return false;
    }

    @Override
    public String exportToCSV() {
        return String.join(";",
                getId().toString(),
                getRemindedObjectId().toString(),
                Common.getDateFormat().format(getDate()),
                Common.getDateFormat().format(getTargetDate()),
                getText(),
                getType().toString());
    }

    @Override
    public String headerCSV() {
        return "id;object_id;date;target_date;text;type";
    }
}
