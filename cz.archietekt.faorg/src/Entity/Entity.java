package Entity;

import Database.EntityDB;

/**
 * Base interface describing representing entity object.
 * 
 * @author kolage
 */
public interface Entity {
    
    /**
     * Get entity id.
     * @return id
     */
    public Integer getId();
    
    /**
     * Get entity database operator.
     * @return entity DB operator
     */
    public EntityDB getDBOperator();
    
    /**
     * Method for exporting object information into CSV.
     * @return CSV row with object info.
     */
    public String exportToCSV();
    
    /**
     * CSV header for this object.
     */
    public String headerCSV();
}
