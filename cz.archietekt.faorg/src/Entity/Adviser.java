package Entity;

import Database.AdviserDB;
import Faorg.Address;

/**
 * Interface describing the adviser that works with client and contract database.
 * 
 * @author kolage
 */
public interface Adviser extends Entity {
    
    /**
     * Get adviser first name.
     * @return adviser first name
     */
    public String getFirstName();
    
    /**
     * Get adviser last name.
     * @return adviser last name
     */
    public String getLastName();
    
    /**
     * Get adviser login.
     * @return adviser login
     */
    public String getLogin();
    
    /**
     * Get adviser password.
     * @return adviser password
     */
    public String getPassword();
    
    /**
     * Get adviser telephone.
     * @return adviser telephone
     */
    public String getTelephone();
    
    /**
     * Get adviser email.
     * @return adviser email
     */
    public String getEmail();
    
    /**
     * Get adviser website.
     * @return adviser website
     */
    public String getWebsite();
    
    /**
     * Get adviser title. It is used in letter templates.
     * @return adviser title
     */
    public String getTitle();
    
    /**
     * Get adviser address.
     * @return adviser address
     */
    public Address getAddress();
    
    /**
     * Get adviser office address.
     * @return adviser office address
     */
    public Address getOfficeAddress();
    
    /**
     * Get adviser ICO.
     * @return adviser ICO
     */
    public String getICO();
    
    /**
     * Get adviser position in company.
     * @return adviser position
     */
    public String getPosition();
    
    /**
     * Sets adviser first name.
     * @param firstName first name
     */
    public void setFirstName(String firstName);
    
    /**
     * Sets adviser last name.
     * @param lastName last name
     */
    public void setLastName(String lastName);
    
    /**
     * Sets adviser password to database.
     * @param password password
     */
    public void setPassword(String password);
    
    /**
     * Sets adviser telephone.
     * @param telephone adviser telephone
     */
    public void setTelephone(String telephone);
    
    /**
     * Sets adviser email.
     * @param email adviser email
     */
    public void setEmail(String email);
    
    /**
     * Sets adviser website.
     * @param website adviser website
     */
    public void setWebsite(String website);
    
    /**
     * Sets adviser title.
     * @param title title
     */
    public void setTitle(String title);
    
    /**
     * Sets adviser address.
     * @param address address
     */
    public void setAddress(Address address);
    
    /**
     * Sets adviser office address.
     * @param officeAddress office address
     */
    public void setOfficeAddress(Address officeAddress);
    
    /**
     * Sets adviser ICO.
     * @param ico adviser ICO
     */
    public void setICO(String ico);
    
    /**
     * Sets adviser position in company.
     * @param position adviser position
     */
    public void setPosition(String position);
    
    /**
     * Get adviser database operator.
     * @return client DB operator
     */
    @Override
    public AdviserDB getDBOperator();
}
